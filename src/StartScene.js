//create a new Layer
var StartLayer = cc.Layer.extend({
 	ctor:function () {
 		this._super();

 		var size = cc.winSize;

     	var helloLabel = new cc.LabelTTF("Hello World", "", 38);
     	helloLabel.x = size.width / 2;
     	helloLabel.y = size.height / 2;
     	this.addChild(helloLabel);

     	//console.log(rsrc);

		// add bg
		this.bgSprite = new cc.Sprite(g_ressources[52]);
		this.bgSprite.attr({
			x: size.width / 2,
			y: size.height / 2,
		});
		this.addChild(this.bgSprite, 0);


		//add start menu
		var startItem = new cc.MenuItemImage(
				g_ressources[53],
				g_ressources[54],
				function () {
					cc.log("Menu is clicked!");
					cc.director.runScene(new cc.TransitionPageTurn(1, new PlayScene(), false) );
				}, this);
		startItem.attr({
			x: size.width/2,
			y: size.height/2,
			anchorX: 0.5,
			anchorY: 0.5
		});

		var menu = new cc.Menu(startItem);
		menu.x = 0;
		menu.y = 0;
		this.addChild(menu, 1);


 		return true;
 	}
 });

 var StartScene = cc.Scene.extend({
 	onEnter:function () {
 		this._super();
 		var layer = new StartLayer();
 		this.addChild(layer);
 	}
 });