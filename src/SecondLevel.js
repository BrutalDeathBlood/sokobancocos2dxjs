/*var level2 = [
[1,1,1,1,1,1,1],
[1,1,0,0,0,0,1],
[1,1,3,0,2,0,1],
[1,0,0,4,0,0,1],
[1,0,3,1,2,0,1],
[1,0,0,1,1,1,1],
[1,1,1,1,1,1,1]
];*/ //7*7
//9*9 Matrix
//Primer Nivel
/*var level2 = [
[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
[1,1,0,0,0,0,1,1,1,1,1,1,1,1,1],
[1,1,3,0,2,0,4,0,1,1,1,1,1,1,1],
[1,0,0,4,0,0,0,0,1,1,1,1,1,1,1],
[1,0,3,1,2,0,0,0,1,1,1,1,1,1,1],
[1,0,0,1,1,1,1,0,1,1,1,1,1,1,1],
[1,1,0,1,1,1,1,0,1,1,1,1,1,1,1],
[1,0,0,0,0,0,0,8,1,1,1,1,1,1,1],
[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
[0,0,0,1,1,1,1,1,1,0,0,0,0,0,0]
];*/
//Segundo Nivel
var level2 = [
[0,0,0,0,0,0,0,1,1,1,1,1,0,0,0],
[0,1,1,1,1,1,1,1,0,0,0,1,1,0,0],
[1,1,2,1,0,4,1,1,0,3,3,0,1,0,0],
[1,2,0,0,0,3,0,0,0,0,0,0,1,0,0],
[1,2,0,3,0,0,1,1,1,0,0,4,1,0,0],
[1,1,1,10,1,1,1,1,1,3,1,1,1,0,0],
[1,0,3,0,0,1,1,1,0,0,2,1,1,0,0],
[1,0,3,0,3,0,3,0,0,0,2,1,1,0,0],
[1,2,0,0,0,1,1,1,0,0,2,1,1,0,0],
[1,2,0,0,0,1,0,1,0,0,2,1,1,0,0],
[1,0,0,1,1,1,0,1,1,1,1,1,1,0,0],
[1,1,1,1,0,0,0,0,0,0,0,0,0,0,0]
];
//Max 16 cols
//Max 12 rows

var cratesArrayLv2 = [];
var playerPositionLv2;
var playerSpriteLv2;
// 2 downLv2
// 4 leftLv2 
// 5 stand
// 6 rightLv2
// 8 upLv2
// 45 stand - leftLv2
// 56 stand - rightLv2
var playerDirectionLv2 = 5;

var playerTwoPositionLv2;
var playerSpriteLv2Two;

var startTouchLv2;
var endTouchLv2;
var swipeToleranceLv2 = 10;
var leftLv2 = null;
var rightLv2 = null;
var upLv2 = null;
var downLv2 = null;
//Chrono Trigger
var sleftLv2 = null;
var srightLv2 = null;
var supLv2 = null;
var sdownLv2 = null;
var stand = null;
var stand_upLv2 = null;
var stand_leftLv2 = null;
var chrono_winLv2 = null;
//Chrono Trigger
var animateCrateLv2 = null;
var animateCrateLv22 = null;
var counterCratesLv2 = 0;
var maxCratesLv2 = 0;
var animWinLv2 = null;
var animWinLv2Two = null;
var counterPlayersLv2 = 0;
var animateCoinsLv2 = null;
var takedCoinLv2 = null;

//Rock downLv2 Mario RPG
var animateRockdownLv2 = null;

//Mario over
var animmarioOverLv2Lv2 = null;
var animMOverupLv2 = null;
var marioOverLv2 = 0;

//Chrono Over
var animchronoOverLv2Lv2 = null;
var chronoOverLv2 = 0;

var layerTwo;

var Secondlevel2Layer = cc.Layer.extend({
 	bgSprite: null,
 	SushiSprites: null,
 	timeout : 60,
 	timeoutLabel: 60,
	scoreLv2LabelLv2: null,
	movesLv2LabelLv2: null,
	scoreLv2: 0,
	movesLv2: 2,
 	ctor:function () {
 		this._super();
 		//this.SushiSprites = [];

		cc.eventManager.addlistener(listenerLv2, this);

 		var size = cc.winSize;
 		//Agregamos plist a spriteFrameCache
 		cc.spriteFrameCache.addSpriteFrames(g_ressources[0]);
 		cc.spriteFrameCache.addSpriteFrames(g_ressources[9]);
 		cc.spriteFrameCache.addSpriteFrames(g_ressources[14]);
 		cc.spriteFrameCache.addSpriteFrames(g_ressources[18]);
 		cc.spriteFrameCache.addSpriteFrames(g_ressources[20]);
 		cc.spriteFrameCache.addSpriteFrames(g_ressources[22]);
 		cc.spriteFrameCache.addSpriteFrames(g_ressources[24]);
 		cc.spriteFrameCache.addSpriteFrames(g_ressources[26]);
 		cc.spriteFrameCache.addSpriteFrames(g_ressources[29]);
 		cc.spriteFrameCache.addSpriteFrames(g_ressources[31]);
 		cc.spriteFrameCache.addSpriteFrames(g_ressources[33]);
 		cc.spriteFrameCache.addSpriteFrames(g_ressources[35]);
 		cc.spriteFrameCache.addSpriteFrames(g_ressources[37]);
 		cc.spriteFrameCache.addSpriteFrames(g_ressources[39]);
 		cc.spriteFrameCache.addSpriteFrames(g_ressources[41]);


 		//Agregamos los Frames a un Array
 		//Anim Move leftLv2
		var animFramesleftLv2 = [];
		var str = "";
		for(var i = 7; i < 10; i+=2){
			str = "mario-walk-" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFramesleftLv2.push(frame);
		}
		//this.setFlippedX(true);
		//Animación 
		var animMoveleftLv2 = cc.Animation.create(animFramesleftLv2, 0.1);
		leftLv2 = cc.RepeatForever.create(cc.Animate.create(animMoveleftLv2));

		//Chrono leftLv2
		//Anim Move leftLv2
		var animFramesleftLv2Chrono = [];
		var str = "";
		for(var i = 1; i < 7; i++){
			str = "chrono-leftLv2-" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFramesleftLv2Chrono.push(frame);
		}
		//this.setFlippedX(true);
		//Animación 
		var animMoveleftLv2chrono = cc.Animation.create(animFramesleftLv2Chrono, 0.1);
		sleftLv2 = cc.RepeatForever.create(cc.Animate.create(animMoveleftLv2chrono));


		//Anim Move upLv2
		var animFramesupLv2 = [];
		var str = "";
		for(var i = 4; i < 7; i+=2){
			str = "mario-walk-" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFramesupLv2.push(frame);
		}
		//this.setFlippedX(true);
		//Animación 
		var animMoveupLv2 = cc.Animation.create(animFramesupLv2, 0.1);
		upLv2 = cc.RepeatForever.create(cc.Animate.create(animMoveupLv2));

		//Chrono upLv2
		//Anim Move upLv2
		var animFramesupLv2Chrono = [];
		var str = "";
		for(var i = 1; i < 7; i++){
			str = "chrono-upLv2-" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFramesupLv2Chrono.push(frame);
		}
		//this.setFlippedX(true);
		//Animación 
		var animMoveupLv2Chrono = cc.Animation.create(animFramesupLv2Chrono, 0.1);
		supLv2 = cc.RepeatForever.create(cc.Animate.create(animMoveupLv2Chrono));

		//Anim Move downLv2
		var animFramesdownLv2 = [];
		var str = "";
		for(var i = 1; i < 4; i+=2){
			str = "mario-walk-" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFramesdownLv2.push(frame);
		}
		//this.setFlippedX(true);
		//Animación 
		var animMovedownLv2 = cc.Animation.create(animFramesdownLv2, 0.1);
		downLv2 = cc.RepeatForever.create(cc.Animate.create(animMovedownLv2));

		//Anim Move downLv2 Chrono
		var animFramesdownLv2Chrono = [];
		var str = "";
		for(var i = 1; i < 7; i++){
			str = "chrono-downLv2-" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFramesdownLv2Chrono.push(frame);
		}
		//this.setFlippedX(true);
		//Animación 
		var animMovedownLv2Chrono = cc.Animation.create(animFramesdownLv2Chrono, 0.1);
		sdownLv2 = cc.RepeatForever.create(cc.Animate.create(animMovedownLv2Chrono));

		//Anim Chrono Stand
		var animFramesStand = [];
		var str = "";
		for(var i = 1; i < 4; i++){
			str = "chrono-stand-" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFramesStand.push(frame);
		}
		for(var i = 3; i > 0; i--){
			str = "chrono-stand-" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFramesStand.push(frame);
		}
		//this.setFlippedX(true);
		//Animación 
		var animmovesLv2tand = cc.Animation.create(animFramesStand, 0.1);
		stand = cc.Animate.create(animmovesLv2tand);

		//Anim Chrono Stand leftLv2
		var animFramesStandleftLv2 = [];
		var str = "";
		for(var i = 1; i < 4; i++){
			str = "chrono-stand-leftLv2-" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFramesStandleftLv2.push(frame);
		}
		for(var i = 3; i > 0; i--){
			str = "chrono-stand-leftLv2-" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFramesStandleftLv2.push(frame);
		}
		//this.setFlippedX(true);
		//Animación 
		var animmovesLv2tandleftLv2 = cc.Animation.create(animFramesStandleftLv2, 0.1);
		stand_leftLv2 = cc.Animate.create(animmovesLv2tandleftLv2);

		//Animation star crate
		var animFramesCrate = [];
		var str = "";
		for(var i = 1; i < 5; i++){
			str = "star_crate_" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFramesCrate.push(frame);
		}
		//this.setFlippedX(true);
		//Animación 
		var animCrate = cc.Animation.create(animFramesCrate, 0.2);
		animateCrateLv2 = cc.RepeatForever.create(cc.Animate.create(animCrate));
		animateCrateLv22 = cc.RepeatForever.create(cc.Animate.create(animCrate));

		//Animation win
		var animFramesWin = [];
		var str = "";
		for(var i = 1; i < 5; i++){
			str = "mario-win-" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFramesWin.push(frame);
		}
		//this.setFlippedX(true);
		//Animación 
		var animWinLv2Mario = cc.Animation.create(animFramesWin, 0.1);
		animWinLv2 = cc.Animate.create(animWinLv2Mario);

		//Animation win Chrono
		var animFramesWinXChrono = [];
		var str = "";
		for(var i = 1; i < 3; i++){
			str = "chrono-win-" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFramesWinXChrono.push(frame);
		}
		//this.setFlippedX(true);
		//Animación 
		var animWinLv2Chrono = cc.Animation.create(animFramesWinXChrono, 0.1);
		animWinLv2Two = cc.Animate.create(animWinLv2Chrono);

		//Animation coins
		var animFramesCoins = [];
		var str = "";
		for(var i = 1; i < 9; i++){
			str = "coin-" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFramesCoins.push(frame);
		}
		//this.setFlippedX(true);
		//Animación 
		var animCoins = cc.Animation.create(animFramesCoins, 0.1);
		animateCoinsLv2 = cc.RepeatForever.create(cc.Animate.create(animCoins));


		//Animation coins taked
		var animFramesCoinsTaked = [];
		var str = "";
		for(var i = 1; i < 15; i++){
			str = "coins-taked-" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFramesCoinsTaked.push(frame);
		}
		//this.setFlippedX(true);
		//Animación 
		var animCoinsTaked = cc.Animation.create(animFramesCoinsTaked, 0.1);
		takedCoinLv2 = cc.Animate.create(animCoinsTaked);

		//Animation Rock downLv2
		var animFramesRock = [];
		var str = "";
		for(var i = 3; i > 0; i--){
			str = "rock-downLv2-" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFramesRock.push(frame);
		}
		//this.setFlippedX(true);
		//Animación 
		var animRock = cc.Animation.create(animFramesRock, 0.2);
		animateRockdownLv2 = cc.Animate.create(animRock);

		//Animation Mario Over
		var animFramesmarioOverLv2 = [];
		var str = "";
		for(var i = 1; i < 3; i++){
			str = "mario-over-" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFramesmarioOverLv2.push(frame);
		}
		//this.setFlippedX(true);
		//Animación 
		var animMarioOv = cc.Animation.create(animFramesmarioOverLv2, 0.2);
		animmarioOverLv2Lv2 = cc.Animate.create(animMarioOv);

		//Animation Mario Over upLv2
		var animFramesmarioOverLv2upLv2 = [];
		var str = "";
		for(var i = 1; i < 3; i++){
			str = "mario-over-upLv2-" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFramesmarioOverLv2upLv2.push(frame);
		}
		//this.setFlippedX(true);
		//Animación 
		var animMOvupLv2 = cc.Animation.create(animFramesmarioOverLv2upLv2, 0.2);
		animMOverupLv2 = cc.Animate.create(animMOvupLv2);

		//Animation Chrono Over 
		var animFrameschronoOverLv2 = [];
		var str = "";
		for(var i = 1; i < 4; i++){
			str = "chrono-over-" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFrameschronoOverLv2.push(frame);
		}
		//this.setFlippedX(true);
		//Animación 
		var animChronoOv = cc.Animation.create(animFrameschronoOverLv2, 0.2);
		animchronoOverLv2Lv2 = cc.Animate.create(animChronoOv);

 		// add bg
 		/*this.bgSprite = new cc.Sprite(g_ressources[0]);
 		this.bgSprite.attr({
 			x: size.width / 2,
 			y: size.height / 2,
 			//scale: 0.5,
 			rotation: 180
 		});
 		this.addChild(this.bgSprite, 0);

 		cc.spriteFrameCache.addSpriteFrames(g_ressources[4]);

 		
 		//this.addSushi();
 		this.schedule(this.upLv2date,1,16*1024,1);

 		this.scoreLv2LabelLv2 = new cc.LabelTTF("scoreLv2:0", "Arial", 20);
		this.scoreLv2LabelLv2.attr({
			x:size.width / 2 + 100,
			y:size.height - 20
		});
		this.addChild(this.scoreLv2LabelLv2, 5);*/

		// timeout 60
		/*this.timeoutLabel = cc.LabelTTF.create("" + this.timeout, "Arial", 30);
		this.timeoutLabel.x = 20;
		this.timeoutLabel.y = size.height - 20;
		this.addChild(this.timeoutLabel, 5);*/
		
		//this.schedule(this.timer,1,this.timeout,1);
		
		//scoreLv2 Label
		this.scoreLv2LabelLv2 = new cc.LabelTTF("scoreLv2: " + this.scoreLv2, "Arial", 20);
		this.scoreLv2LabelLv2.attr({
			x:size.width / 2 + 150,
			y:20
		});
		this.addChild(this.scoreLv2LabelLv2, 5);

		//movesLv2 Label
		this.movesLv2LabelLv2 = new cc.LabelTTF("movesLv2: " + this.movesLv2, "Arial", 20);
		this.movesLv2LabelLv2.attr({
			x:size.width / 2 + 300,
			y:20
		});
		this.addChild(this.movesLv2LabelLv2, 5);
		
		//this.addChild(level2Sprite);

		//Inicializar Nivel
		var countLv2 = 0;
		for(i=0;i<12;i++){
			cratesArrayLv2[i]=[];
				for(j=0;j<15;j++){
					switch(level2[i][j]){
						case 1:
							var wall = new cc.Sprite.create(g_ressources[7]);
							//wall.setPosition(165+25*j,186-25*i);
							wall.setScale(1.5);
							wall.setPosition(50+50*j,600-50*i);
							this.addChild(wall);
							cratesArrayLv2[i][j]=null;
							break;
						case 2:
							var placeCrate = new cc.Sprite.create(g_ressources[16]);
							//placeCrate.setPosition(165+25*j,186-25*i);
							placeCrate.setPosition(50+50*j,600-50*i);
							placeCrate.setScale(1.5);
							cratesArrayLv2[i][j]=placeCrate;
							this.addChild(placeCrate,0);
							maxCratesLv2++;
							break;
						case 4:
						case 6:
							counterPlayersLv2++;
							switch(counterPlayersLv2){
								case 1: 
									playerSpriteLv2 = new cc.Sprite.create(cc.spriteFrameCache.getSpriteFrame('mario-walk-2.png'));
									//playerSpriteLv2.setPosition(165+25*j,185-25*i);
									playerSpriteLv2.setPosition(50+50*j,600-50*i);
									playerSpriteLv2.setScale(1.5);
									playerSpriteLv2.setCascadeOpacityEnabled(true);
									playerSpriteLv2.setOpacity(255);
									this.addChild(playerSpriteLv2,1);
									playerPositionLv2 = {x:j,y:i};
									cratesArrayLv2[i][j]=null;
									var fadeInM = cc.FadeIn.create(1.0);
									playerSpriteLv2.runAction(fadeInM);
									break;
								case 2: 
									playerSpriteLv2Two = new cc.Sprite.create(cc.spriteFrameCache.getSpriteFrame('chrono-stand-1.png'));
									//playerSpriteLv2.setPosition(165+25*j,185-25*i);
									playerSpriteLv2Two.setPosition(50+50*j,600-50*i);
									playerSpriteLv2Two.setScale(1.5);
									playerSpriteLv2Two.setCascadeOpacityEnabled(true);
									playerSpriteLv2Two.setOpacity(255);
									playerSpriteLv2Two.runAction(stand);
									this.addChild(playerSpriteLv2Two,1);
									var fadeInM2 = cc.FadeIn.create(0.9);
									playerSpriteLv2Two.runAction(fadeInM2);
									playerTwoPositionLv2 = {x:j,y:i};
									cratesArrayLv2[i][j]=null;
									break;
							}
							break;
						case 3:
						case 5:
							var crateSprite = new cc.Sprite.create();
							//crateSprite.setPosition(165+25*j,185-25*i);
							crateSprite.setPosition(50+50*j,600-50*i);
							crateSprite.setScale(1.5);
							switch(countLv2){
								case 0:
									crateSprite.runAction(animateCrateLv2);
									break;
								case 1:
									crateSprite.runAction(animateCrateLv22);
									break;
								default:
									crateSprite.setTexture(g_ressources[17]);
								break;
							}
							countLv2++;
							this.addChild(crateSprite,1);
							cratesArrayLv2[i][j]=crateSprite;
							break;
						case 8:
							var coinSprite = cc.Sprite.create();
							coinSprite.setPosition(50+50*j,600-50*i);
							coinSprite.setScale(1.5);
							coinSprite.runAction(animateCoinsLv2);
							this.addChild(coinSprite,1);
							cratesArrayLv2[i][j]=coinSprite;
							break;
						case 10:
							var rockSprite = cc.Sprite.create(cc.spriteFrameCache.getSpriteFrame('rock-downLv2-1.png'));
							rockSprite.setPosition(50+50*j,600-50*i);
							rockSprite.setScale(1.5);
							var actionMove = cc.MoveTo.create(0.5, cc.p(rockSprite.getPosition().x, rockSprite.getPosition().y + 50));
							var actionMovedownLv2 = cc.MoveTo.create(0.5, cc.p(rockSprite.getPosition().x, rockSprite.getPosition().y ));
							//Calback
							var funcT=new cc.CallFunc(function(){
								//console.log("Termina Anim Rock");
								//playerSpriteLv2.stopAction(actionMove);
								//playerSpriteLv2.stopAllActions();
								//playerSpriteLv2.setTexture(g_ressources[11]);
								//console.log('Mario',playerSpriteLv2.getPosition());
								//console.log('Rock',rockSprite.getPosition());
								if(playerSpriteLv2 !== null && playerSpriteLv2.getPosition().x.toFixed(2) === rockSprite.getPosition().x.toFixed(2)
									&& playerSpriteLv2.getPosition().y.toFixed(2) === rockSprite.getPosition().y.toFixed(2)){
									//Fue aplastado
									marioOverLv2 = 1;
									console.log("Aplastado!");
									playerSpriteLv2.stopAllActions();
									var fadeOutMove = cc.FadeOut.create(1.0);
									var funcOverMario = new cc.CallFunc(function(){
										console.log("Se quita Mario del tablero ");
										//Cambiamos a Piedra de nuevo
										//Not over crates drop
										//Regresamos el estado que tenia rock downLv2
										level2[playerPositionLv2.y][playerPositionLv2.x] = 10;
										playerSpriteLv2.setPosition(0,0);
										layerTwo.removeChild(playerSpriteLv2);
										playerSpriteLv2 = null;
										marioOverLv2 = 2;
									},playerSpriteLv2);
									switch(playerDirectionLv2){
										case 8:
											playerSpriteLv2.runAction(new cc.sequence(animMOverupLv2,fadeOutMove,funcOverMario));
											break;
										default:
											playerSpriteLv2.runAction(new cc.sequence(animmarioOverLv2Lv2,fadeOutMove,funcOverMario));
											break;
									}
								}

								if(playerSpriteLv2Two !== null && playerSpriteLv2Two.getPosition().x.toFixed(2) === rockSprite.getPosition().x.toFixed(2)
									&& playerSpriteLv2Two.getPosition().y.toFixed(2) === rockSprite.getPosition().y.toFixed(2)){
									//Fue aplastado
									chronoOverLv2 = 1;
									console.log("Aplastado Chrono!");
									playerSpriteLv2Two.stopAllActions();
									var fadeChrono = cc.FadeOut.create(0.9);
									var funcChr = new cc.CallFunc(function(){
										console.log("Se quita Chrono del tablero ");
										//Regresamos el estado que tenia rock downLv2
										level2[playerTwoPositionLv2.y][playerTwoPositionLv2.x] = 10;
										playerSpriteLv2Two.setPosition(0,0);
										layerTwo.removeChild(playerSpriteLv2Two);
										playerSpriteLv2Two = null;
										chronoOverLv2 = 2;
									},playerSpriteLv2Two);
					
									playerSpriteLv2Two.runAction(new cc.sequence(animchronoOverLv2Lv2,fadeChrono,funcChr));
									
								}
							},rockSprite);

					    	var anim = new cc.RepeatForever(new cc.sequence(actionMove,animateRockdownLv2,actionMovedownLv2,funcT));
					    	//rockSprite.runAction(actionMove);
					    	//rockSprite.runAction(cc.sequence(animateRockdownLv2,actionMovedownLv2));
					    	rockSprite.runAction(anim);
							this.addChild(rockSprite,1);
							cratesArrayLv2[i][j]=rockSprite;
							break;
						default:
							cratesArrayLv2[i][j]=null;
					}
				}
		}

		this.schedule(this.upLv2date,1,16*1024,1);

 		return true;
 	},
 	addSushi : function() {
		
		//var sushi = new cc.Sprite(g_ressources[3]);
		var sushi = new SushiSprite(g_ressources[3]);

		var size = cc.winSize;

		var x = sushi.width/2+size.width/2*cc.random0To1();
		sushi.attr({
			x: x,
			y:size.height - 30
		});
		
		this.addChild(sushi,5);

		var dorpAction = cc.MoveTo.create(4, cc.p(sushi.x,-30));
		sushi.runAction(dorpAction);

		this.SushiSprites.push(sushi);

	},
	upLv2date : function() {
		//this.addSushi();
		//this.removesLv2ushi();
		console.log('upLv2date!');
		console.log('level2',level2);
		if((chronoOverLv2 === 2 && marioOverLv2 === 2) ||
			this.movesLv2 === 0){
			this.unschedule(this.upLv2date);
			playerSpriteLv2.stopAllActions();
			playerSpriteLv2Two.stopAllActions();
			var transition= new cc.TransitionFade(1, new GameOverScene(),cc.color(255,255,255,255));
			cc.director.runScene(transition);
		}
	},
	removesLv2ushi : function() {
		//remove sushi at teh bottom of the screen 
		for (var i = 0; i < this.SushiSprites.length; i++) {
			cc.log("removesLv2ushi.........");
			console.log(this.SushiSprites[i].y);
			if(0 > this.SushiSprites[i].y) {
				cc.log("==============remove:"+i);
				this.SushiSprites[i].removeFromParent();
				//this.removeChild(this.SushiSprites[i],true);
				this.SushiSprites[i] = undefined;
				this.SushiSprites.splice(i,1);
				i = i-1;
			}
		}
	},
	addscoreLv2:function(points){
		this.scoreLv2 += points;
		this.scoreLv2LabelLv2.setString("scoreLv2:" + this.scoreLv2);
	},
	SubmovesLv2:function(){
		this.movesLv2 -= 1;
		this.movesLv2LabelLv2.setString("movesLv2: " + this.movesLv2);
	},
	timer : function() {
		if (this.timeout == 0) {
			//cc.log('游戏结束');
			var gameOver = new cc.LayerColor(cc.color(225,225,225,100));
			var size = cc.winSize;
			var titleLabel = new cc.LabelTTF("Game Over", "Arial", 38);
			titleLabel.attr({
				x:size.width / 2 ,
				y:size.height / 2
			});
			gameOver.addChild(titleLabel, 5);
			var TryAgainItem = new cc.MenuItemFont(
					"Try Again",
					function () {
						cc.log("Menu is clicked!");
						var transition= cc.TransitionFade(1, new PlayScene(),cc.color(255,255,255,255));
						cc.director.runScene(transition);
					}, this);
			TryAgainItem.attr({
				x: size.width/2,
				y: size.height / 2 - 60,
				anchorX: 0.5,
				anchorY: 0.5
			});

			var menu = new cc.Menu(TryAgainItem);
			menu.x = 0;
			menu.y = 0;
			gameOver.addChild(menu, 1);
			this.getParent().addChild(gameOver);
			
			this.unschedule(this.upLv2date);
			this.unschedule(this.timer);
			return;
		}

		this.timeout -=1;
		this.timeoutLabel.setString("" + this.timeout);

	}
 });

var listenerLv2 = cc.EventListener.create({
	event: cc.EventListener.TOUCH_ONE_BY_ONE,
	swallowTouches: true,
	onTouchBegan:function (touch,event) {
		startTouchLv2 = touch.getLocation();
		return true;
	},
	onTouchEnded:function(touch, event){
		endTouchLv2 = touch.getLocation();
		if(playerSpriteLv2 !== null || playerSpriteLv2Two !== null){
			swipeDirection();
		}
	}
});

/*function swipeDirection(){
	var distX = startTouchLv2.x - endTouchLv2.x;
	var distY = startTouchLv2.y - endTouchLv2.y;
	if(Math.abs(distX)+Math.abs(distY)>swipeToleranceLv2){
		if(Math.abs(distX)>Math.abs(distY)){
			if(distX>0){
				playerSpriteLv2.setPosition(playerSpriteLv2.getPosition().
				x-25,playerSpriteLv2.getPosition().y);
				//move(-1,0);
			}else{
				playerSpriteLv2.setPosition(playerSpriteLv2.getPosition().
				x+25,playerSpriteLv2.getPosition().y);
				//move(1,0);
			}
		}else{
			if(distY>0){
				playerSpriteLv2.setPosition(playerSpriteLv2.getPosition().
				x,playerSpriteLv2.getPosition().y-25);
				//move(0,1);
			}else{
				playerSpriteLv2.setPosition(playerSpriteLv2.getPosition().
				x,playerSpriteLv2.getPosition().y+25);
				//move(0,-1);
			}
		}
	}
}*/
function swipeDirection(){
	var distX = startTouchLv2.x - endTouchLv2.x;
	var distY = startTouchLv2.y - endTouchLv2.y;
	if(Math.abs(distX)+Math.abs(distY)>swipeToleranceLv2){
		if(Math.abs(distX)>Math.abs(distY)){
			if(distX>0){
				//Move leftLv2
				playerDirectionLv2 = 4;
				move(-1,0);
				movesLv2econdPlayer(-1,0);
			}else{
				//Move rightLv2
				playerDirectionLv2 = 6;
				move(1,0);
				movesLv2econdPlayer(1,0);
			}
		}else{
			if(distY>0){
				//Move downLv2
				playerDirectionLv2 = 2;
				move(0,1);
				movesLv2econdPlayer(0,1);
			}else{
				//Move upLv2
				playerDirectionLv2 = 8;
				move(0,-1);
				movesLv2econdPlayer(0,-1);
			}
		}
	}
	//Quitamos un movimiento
	layerTwo.SubmovesLv2();
}

function move(deltaX,deltaY){
	if(playerSpriteLv2 !== null && marioOverLv2 === 0){
		switch(level2[playerPositionLv2.y+deltaY][playerPositionLv2.x+deltaX]){
		case 0:
		case 2:
			level2[playerPositionLv2.y][playerPositionLv2.x]-=4;
			playerPositionLv2.x+=deltaX;
			playerPositionLv2.y+=deltaY;
			level2[playerPositionLv2.y][playerPositionLv2.x]+=4;
			/*playerSpriteLv2.setPosition(165+25*playerPositionLv2.x,185-
			25*playerPositionLv2.y);*/
			switch(playerDirectionLv2){
				//downLv2 
				case 2:
					playerSpriteLv2.setFlippedX(false);
					// Move the sprite
				    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerPositionLv2.x, 
				    	600-50*playerPositionLv2.y));
				    var func=new cc.CallFunc(function(){
						console.log("Termina Movimiento abajo");
						//playerSpriteLv2.stopAction(actionMove);
						playerSpriteLv2.stopAction(downLv2);
						playerSpriteLv2.setTexture(g_ressources[11]);
						//Verificamos si ha ganado
					    if(maxCratesLv2 === counterCratesLv2){
					    	console.log("Win");
					    	playerSpriteLv2.stopAllActions();
					    	var funcT=new cc.CallFunc(function(){
								console.log("Termina Win Anim");
								//playerSpriteLv2.stopAction(actionMove);
								//playerSpriteLv2.stopAllActions();
								//playerSpriteLv2.setTexture(g_ressources[11]);
							},playerSpriteLv2);
					    	playerSpriteLv2.runAction(cc.sequence(animWinLv2,funcT));
					    }	
					    console.log('level2',level2);
						console.log('crates',cratesArrayLv2);
						console.log(level2[playerPositionLv2.y][playerPositionLv2.x]);
					    //Player over crate drop
					    if(level2[playerPositionLv2.y][playerPositionLv2.x] === 6){
					    	console.log("Crates Changed");
							var placeCrateSuccess = cratesArrayLv2[playerPositionLv2.y][playerPositionLv2.x];
							console.log('Crates',placeCrateSuccess);
					    	placeCrateSuccess.setTexture(g_ressources[17]);
					    }else{
						    //Not over crates drop
							for(i=0;i<11;i++){
								for(j=0;j<15;j++){
									switch(level2[i][j]){
										case 2:
											var placeCrate = cratesArrayLv2[i][j];
											placeCrate.setTexture(g_ressources[16]);
											//placeCrate.setPosition(165+25*j,186-25*i);
											break;
									}
								}
							}
					    }
					},playerSpriteLv2);
					playerSpriteLv2.runAction(downLv2);
					playerSpriteLv2.runAction(cc.sequence(actionMove,func)); 
					break;
				//leftLv2
				case 4:
					// Move the sprite
				    playerSpriteLv2.setFlippedX(false);
				    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerPositionLv2.x, 
				    	600-50*playerPositionLv2.y));
				    var func=new cc.CallFunc(function(){
						console.log("Termina Movimiento izquierda");
						//playerSpriteLv2.stopAction(actionMove);
						playerSpriteLv2.stopAction(leftLv2);
						playerSpriteLv2.setTexture(g_ressources[12]);
						//Verificamos si ha ganado
					    if(maxCratesLv2 === counterCratesLv2){
					    	console.log("Win");
					    	var funcT=new cc.CallFunc(function(){
								console.log("Termina Win Anim");
								//playerSpriteLv2.stopAction(actionMove);
								//playerSpriteLv2.stopAllActions();
								//playerSpriteLv2.setTexture(g_ressources[11]);
							},playerSpriteLv2);
					    	playerSpriteLv2.runAction(cc.sequence(animWinLv2,funcT));
					    }

					    console.log('level2',level2);
						console.log('crates',cratesArrayLv2);
						console.log(level2[playerPositionLv2.y][playerPositionLv2.x]);
					    //Player over crate drop
					    if(level2[playerPositionLv2.y][playerPositionLv2.x] === 6){
					    	console.log("Crates Changed");
							var placeCrateSuccess = cratesArrayLv2[playerPositionLv2.y][playerPositionLv2.x];
							console.log('Crates',placeCrateSuccess);
					    	placeCrateSuccess.setTexture(g_ressources[17]);
					    }else{
						    //Not over crates drop
							for(i=0;i<11;i++){
								for(j=0;j<15;j++){
									switch(level2[i][j]){
										case 2:
											var placeCrate = cratesArrayLv2[i][j];
											placeCrate.setTexture(g_ressources[16]);
											//placeCrate.setPosition(165+25*j,186-25*i);
											break;
									}
								}
							}
					    }
					},playerSpriteLv2);
					playerSpriteLv2.runAction(leftLv2);
					playerSpriteLv2.runAction(cc.sequence(actionMove,func)); 
					break;
				//rightLv2
				case 6:
					playerSpriteLv2.setFlippedX(true);
					// Move the sprite
				    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerPositionLv2.x, 
				    	600-50*playerPositionLv2.y));
				    var func=new cc.CallFunc(function(){
						console.log("Termina Movimiento derecha");
						//playerSpriteLv2.stopAction(actionMove);
						playerSpriteLv2.stopAction(leftLv2);
						playerSpriteLv2.setTexture(g_ressources[12]);
						//Verificamos si ha ganado
					    if(maxCratesLv2 === counterCratesLv2){
					    	console.log("Win");
					    	var funcT=new cc.CallFunc(function(){
								console.log("Termina Win Anim");
								//playerSpriteLv2.stopAction(actionMove);
								//playerSpriteLv2.stopAction(animWinLv2);
								//playerSpriteLv2.setTexture(g_ressources[11]);
							},playerSpriteLv2);
					    	playerSpriteLv2.runAction(cc.sequence(animWinLv2,funcT));
					    }

					    console.log('level2',level2);
						console.log('crates',cratesArrayLv2);
						console.log(level2[playerPositionLv2.y][playerPositionLv2.x]);
					    //Player over crate drop
					    if(level2[playerPositionLv2.y][playerPositionLv2.x] === 6){
					    	console.log("Crates Changed");
							var placeCrateSuccess = cratesArrayLv2[playerPositionLv2.y][playerPositionLv2.x];
							console.log('Crates',placeCrateSuccess);
					    	placeCrateSuccess.setTexture(g_ressources[17]);
					    }else{
						    //Not over crates drop
							for(i=0;i<11;i++){
								for(j=0;j<15;j++){
									switch(level2[i][j]){
										case 2:
											var placeCrate = cratesArrayLv2[i][j];
											placeCrate.setTexture(g_ressources[16]);
											//placeCrate.setPosition(165+25*j,186-25*i);
											break;
									}
								}
							}
					    }
					},playerSpriteLv2);
					playerSpriteLv2.runAction(leftLv2);
					playerSpriteLv2.runAction(cc.sequence(actionMove,func)); 
					break;
				//upLv2
				case 8:
					playerSpriteLv2.setFlippedX(false);
					// Move the sprite
				    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerPositionLv2.x, 
				    	600-50*playerPositionLv2.y));
				    var func=new cc.CallFunc(function(){
						console.log("Termina Movimiento arriba");
						//playerSpriteLv2.stopAction(actionMove);
						playerSpriteLv2.stopAction(upLv2);
						playerSpriteLv2.setTexture(g_ressources[13]);
						//Verificamos si ha ganado
					    if(maxCratesLv2 === counterCratesLv2){
					    	console.log("Win");
					    	var funcT=new cc.CallFunc(function(){
								console.log("Termina Win Anim");
								//playerSpriteLv2.stopAction(actionMove);
								//playerSpriteLv2.stopAction(animWinLv2);
								//playerSpriteLv2.setTexture(g_ressources[11]);
							},playerSpriteLv2);
					    	playerSpriteLv2.runAction(cc.sequence(animWinLv2,funcT));
					    }

					    console.log('level2',level2);
						console.log('crates',cratesArrayLv2);
						console.log(level2[playerPositionLv2.y][playerPositionLv2.x]);
					    //Player over crate drop
					    if(level2[playerPositionLv2.y][playerPositionLv2.x] === 6){
					    	console.log("Crates Changed");
							var placeCrateSuccess = cratesArrayLv2[playerPositionLv2.y][playerPositionLv2.x];
							console.log('Crates',placeCrateSuccess);
					    	placeCrateSuccess.setTexture(g_ressources[17]);
					    }else{
						    //Not over crates drop
							for(i=0;i<11;i++){
								for(j=0;j<15;j++){
									switch(level2[i][j]){
										case 2:
											var placeCrate = cratesArrayLv2[i][j];
											placeCrate.setTexture(g_ressources[16]);
											//placeCrate.setPosition(165+25*j,186-25*i);
											break;
									}
								}
							}
					    }
					},playerSpriteLv2);
					playerSpriteLv2.runAction(upLv2);
					playerSpriteLv2.runAction(cc.sequence(actionMove,func)); 
					break;
				
			}
		    
			/*playerSpriteLv2.setPosition(50+50*playerPositionLv2.x,600-
			50*playerPositionLv2.y);*/
			break;
		case 3:
		case 5:
			if(level2[playerPositionLv2.y+deltaY*2][playerPositionLv2.x+deltaX*2]==0
			|| level2[playerPositionLv2.y+deltaY*2][playerPositionLv2.x+deltaX*2]==2){
				level2[playerPositionLv2.y][playerPositionLv2.x]-=4;
				playerPositionLv2.x+=deltaX;
				playerPositionLv2.y+=deltaY;
				level2[playerPositionLv2.y][playerPositionLv2.x]+=1;
				/*playerSpriteLv2.setPosition(165+25*playerPositionLv2.x,185-
				25*playerPositionLv2.y);*/
				// Move the sprite
			    /*var actionMove = cc.MoveTo.create(1, cc.p(50+50*playerPositionLv2.x, 
			    	600-50*playerPositionLv2.y));
			    var func=new cc.CallFunc(function(){
					console.log("Termina Movimiento");
					playerSpriteLv2.stopAction(actionMove);
				},playerSpriteLv2);
			    playerSpriteLv2.runAction(cc.sequence(actionMove,func));*/
			    switch(playerDirectionLv2){
					//downLv2 
					case 2:
						playerSpriteLv2.setFlippedX(false);
						// Move the sprite
					    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerPositionLv2.x, 
					    	600-50*playerPositionLv2.y));
					    var func=new cc.CallFunc(function(){
							console.log("Termina Movimiento abajo");
							//playerSpriteLv2.stopAction(actionMove);
							playerSpriteLv2.stopAction(downLv2);
							playerSpriteLv2.setTexture(g_ressources[11]);
							//Verificamos si ha ganado
						    if(maxCratesLv2 === counterCratesLv2){
						    	console.log("Win downLv2");
						    	playerSpriteLv2.stopAllActions();
						    	var funcT=new cc.CallFunc(function(){
									console.log("Termina Win Anim");
									//playerSpriteLv2.stopAction(actionMove);
								},playerSpriteLv2);
						    	playerSpriteLv2.runAction(cc.sequence(animWinLv2,funcT));
						    	//playerSpriteLv2.runAction(animWinLv2);
						    }
						},playerSpriteLv2);
						playerSpriteLv2.runAction(downLv2);
						playerSpriteLv2.runAction(cc.sequence(actionMove,func)); 
						break;
					//leftLv2
					case 4:
						// Move the sprite
					    playerSpriteLv2.setFlippedX(false);
					    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerPositionLv2.x, 
					    	600-50*playerPositionLv2.y));
					    var func=new cc.CallFunc(function(){
							console.log("Termina Movimiento izquierda");
							//playerSpriteLv2.stopAction(actionMove);
							playerSpriteLv2.stopAction(leftLv2);
							playerSpriteLv2.setTexture(g_ressources[12]);
							//Verificamos si ha ganado
						    if(maxCratesLv2 === counterCratesLv2){
						    	console.log("Win");
						    	var funcT=new cc.CallFunc(function(){
									console.log("Termina Win Anim");
									//playerSpriteLv2.stopAction(actionMove);
									//playerSpriteLv2.stopAction(animWinLv2);
									//playerSpriteLv2.setTexture(g_ressources[11]);
								},playerSpriteLv2);
						    	playerSpriteLv2.runAction(cc.sequence(animWinLv2,funcT));
						    }
						},playerSpriteLv2);
						playerSpriteLv2.runAction(leftLv2);
						playerSpriteLv2.runAction(cc.sequence(actionMove,func)); 
						break;
					//rightLv2
					case 6:
						playerSpriteLv2.setFlippedX(true);
						// Move the sprite
					    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerPositionLv2.x, 
					    	600-50*playerPositionLv2.y));
					    var func=new cc.CallFunc(function(){
							console.log("Termina Movimiento derecha");
							//playerSpriteLv2.stopAction(actionMove);
							playerSpriteLv2.stopAction(leftLv2);
							playerSpriteLv2.setTexture(g_ressources[12]);
							//Verificamos si ha ganado
						    if(maxCratesLv2 === counterCratesLv2){
						    	console.log("Win");
						    	var funcT=new cc.CallFunc(function(){
									console.log("Termina Win Anim");
									//playerSpriteLv2.stopAction(actionMove);
									//playerSpriteLv2.stopAction(animWinLv2);
									//playerSpriteLv2.setTexture(g_ressources[11]);
								},playerSpriteLv2);
						    	playerSpriteLv2.runAction(cc.sequence(animWinLv2,funcT));
						    }
						},playerSpriteLv2);
						playerSpriteLv2.runAction(leftLv2);
						playerSpriteLv2.runAction(cc.sequence(actionMove,func)); 
						break;
					//upLv2
					case 8:
						playerSpriteLv2.setFlippedX(false);
						// Move the sprite
					    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerPositionLv2.x, 
					    	600-50*playerPositionLv2.y));
					    var func=new cc.CallFunc(function(){
							console.log("Termina Movimiento arriba");
							//playerSpriteLv2.stopAction(actionMove);
							playerSpriteLv2.stopAction(upLv2);
							playerSpriteLv2.setTexture(g_ressources[13]);
							//Verificamos si ha ganado
						    if(maxCratesLv2 === counterCratesLv2){
						    	console.log("Win");
						    	var funcT=new cc.CallFunc(function(){
									console.log("Termina Win Anim");
									//playerSpriteLv2.stopAction(actionMove);
									//playerSpriteLv2.stopAction(animWinLv2);
									//playerSpriteLv2.setTexture(g_ressources[11]);
								},playerSpriteLv2);
						    	playerSpriteLv2.runAction(cc.sequence(animWinLv2,funcT));
						    }
						},playerSpriteLv2);
						playerSpriteLv2.runAction(upLv2);
						playerSpriteLv2.runAction(cc.sequence(actionMove,func)); 
						break;
					
				}
				/*playerSpriteLv2.setPosition(50+50*playerPositionLv2.x,600-
				50*playerPositionLv2.y);*/
				level2[playerPositionLv2.y+deltaY][playerPositionLv2.x+deltaX]+=3;
				var movingCrate = cratesArrayLv2[playerPositionLv2.y]
				[playerPositionLv2.x];
				/*movingCrate.setPosition(movingCrate.getPosition().
				x+25*deltaX,movingCrate.getPosition().y-25*deltaY);*/
				
			    var actionMoveCrate = cc.MoveTo.create(0.5, cc.p(movingCrate.getPosition().x+50*deltaX
			    	,movingCrate.getPosition().y-50*deltaY));
			    var funcCrate=new cc.CallFunc(function(){
					console.log("Termina Movimiento");
					playerSpriteLv2.stopAction(actionMoveCrate);
				},movingCrate);
			    movingCrate.runAction(cc.sequence(actionMoveCrate,funcCrate));
			    //Crate over crate Drop
			    if(level2[playerPositionLv2.y+deltaY][playerPositionLv2.x+deltaX] === 5 ){
			    	//Ha colocado en el lugar correcto el objeto
			    	var placeCrateSuccess = cratesArrayLv2[playerPositionLv2.y+deltaY][playerPositionLv2.x+deltaX];
			    	placeCrateSuccess.setTexture(g_ressources[17]);
			    	counterCratesLv2++;
			    	layerTwo.addscoreLv2(250*counterCratesLv2);
			    }/*else{
			    	var placeCrateSuccess = level2[playerPositionLv2.y+deltaY][playerPositionLv2.x+deltaX];
			    	placeCrateSuccess.setTexture(g_ressources[16]);
			    }*/

				/*movingCrate.setPosition(movingCrate.getPosition().
				x+50*deltaX,movingCrate.getPosition().y-50*deltaY);*/

				cratesArrayLv2[playerPositionLv2.y+deltaY][playerPositionLv2.
				x+deltaX]=movingCrate;
				cratesArrayLv2[playerPositionLv2.y][playerPositionLv2.x]=null;
			}
		break;
		case 8:
			level2[playerPositionLv2.y][playerPositionLv2.x]-=4;
			playerPositionLv2.x+=deltaX;
			playerPositionLv2.y+=deltaY;
			level2[playerPositionLv2.y][playerPositionLv2.x]+=4;
			console.log('level2 Player',level2[playerPositionLv2.y][playerPositionLv2.x]);
			switch(playerDirectionLv2){
				//downLv2 
				case 2:
					playerSpriteLv2.setFlippedX(false);
					// Move the sprite
				    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerPositionLv2.x, 
				    	600-50*playerPositionLv2.y));
				    var func=new cc.CallFunc(function(){
						console.log("Termina Movimiento abajo");
						//playerSpriteLv2.stopAction(actionMove);
						playerSpriteLv2.stopAction(downLv2);
						playerSpriteLv2.setTexture(g_ressources[11]);
						//Verificamos si ha ganado
					    if(maxCratesLv2 === counterCratesLv2){
					    	console.log("Win");
					    	playerSpriteLv2.stopAllActions();
					    	var funcT=new cc.CallFunc(function(){
								console.log("Termina Win Anim");
								//playerSpriteLv2.stopAction(actionMove);
								//playerSpriteLv2.stopAction(animWinLv2);
								//playerSpriteLv2.setTexture(g_ressources[11]);
							},playerSpriteLv2);
					    	playerSpriteLv2.runAction(cc.sequence(animWinLv2,funcT));
					    }	
					    console.log('level2',level2);
						console.log('crates',cratesArrayLv2);
						console.log(level2[playerPositionLv2.y][playerPositionLv2.x]);
					    //Player over crate drop
					    if(level2[playerPositionLv2.y][playerPositionLv2.x] === 6){
					    	console.log("Crates Changed");
							var placeCrateSuccess = cratesArrayLv2[playerPositionLv2.y][playerPositionLv2.x];
							console.log('Crates',placeCrateSuccess);
					    	placeCrateSuccess.setTexture(g_ressources[17]);
					    }else{
						    //Not over crates drop
							for(i=0;i<11;i++){
								for(j=0;j<15;j++){
									switch(level2[i][j]){
										case 2:
											var placeCrate = cratesArrayLv2[i][j];
											placeCrate.setTexture(g_ressources[16]);
											//placeCrate.setPosition(165+25*j,186-25*i);
											break;
									}
								}
							}
					    }
					},playerSpriteLv2);
					playerSpriteLv2.runAction(downLv2);
					playerSpriteLv2.runAction(cc.sequence(actionMove,func)); 
					break;
				//leftLv2
				case 4:
					// Move the sprite
				    playerSpriteLv2.setFlippedX(false);
				    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerPositionLv2.x, 
				    	600-50*playerPositionLv2.y));
				    var func=new cc.CallFunc(function(){
						console.log("Termina Movimiento izquierda");
						//playerSpriteLv2.stopAction(actionMove);
						playerSpriteLv2.stopAction(leftLv2);
						playerSpriteLv2.setTexture(g_ressources[12]);
						//Verificamos si ha ganado
					    if(maxCratesLv2 === counterCratesLv2){
					    	console.log("Win");
					    	var funcT=new cc.CallFunc(function(){
								console.log("Termina Win Anim");
								//playerSpriteLv2.stopAction(actionMove);
								//playerSpriteLv2.stopAction(animWinLv2);
								//playerSpriteLv2.setTexture(g_ressources[11]);
							},playerSpriteLv2);
					    	playerSpriteLv2.runAction(cc.sequence(animWinLv2,funcT));
					    }

					    console.log('level2',level2);
						console.log('crates',cratesArrayLv2);
						console.log(level2[playerPositionLv2.y][playerPositionLv2.x]);
					    //Player over crate drop
					    if(level2[playerPositionLv2.y][playerPositionLv2.x] === 6){
					    	console.log("Crates Changed");
							var placeCrateSuccess = cratesArrayLv2[playerPositionLv2.y][playerPositionLv2.x];
							console.log('Crates',placeCrateSuccess);
					    	placeCrateSuccess.setTexture(g_ressources[17]);
					    }else{
						    //Not over crates drop
							for(i=0;i<11;i++){
								for(j=0;j<15;j++){
									switch(level2[i][j]){
										case 2:
											var placeCrate = cratesArrayLv2[i][j];
											placeCrate.setTexture(g_ressources[16]);
											//placeCrate.setPosition(165+25*j,186-25*i);
											break;
									}
								}
							}
					    }
					},playerSpriteLv2);
					playerSpriteLv2.runAction(leftLv2);
					playerSpriteLv2.runAction(cc.sequence(actionMove,func)); 
					break;
				//rightLv2
				case 6:
					playerSpriteLv2.setFlippedX(true);
					// Move the sprite
				    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerPositionLv2.x, 
				    	600-50*playerPositionLv2.y));
				    var func=new cc.CallFunc(function(){
						console.log("Termina Movimiento derecha");
						//playerSpriteLv2.stopAction(actionMove);
						playerSpriteLv2.stopAction(leftLv2);
						playerSpriteLv2.setTexture(g_ressources[12]);
						//Verificamos si ha ganado
					    if(maxCratesLv2 === counterCratesLv2){
					    	console.log("Win");
					    	var funcT=new cc.CallFunc(function(){
								console.log("Termina Win Anim");
								//playerSpriteLv2.stopAction(actionMove);
								//playerSpriteLv2.stopAction(animWinLv2);
								//playerSpriteLv2.setTexture(g_ressources[11]);
							},playerSpriteLv2);
					    	playerSpriteLv2.runAction(cc.sequence(animWinLv2,funcT));
					    }

					    console.log('level2',level2);
						console.log('crates',cratesArrayLv2);
						console.log(level2[playerPositionLv2.y][playerPositionLv2.x]);
					    //Player over crate drop
					    if(level2[playerPositionLv2.y][playerPositionLv2.x] === 6){
					    	console.log("Crates Changed");
							var placeCrateSuccess = cratesArrayLv2[playerPositionLv2.y][playerPositionLv2.x];
							console.log('Crates',placeCrateSuccess);
					    	placeCrateSuccess.setTexture(g_ressources[17]);
					    }else{
						    //Not over crates drop
							for(i=0;i<11;i++){
								for(j=0;j<15;j++){
									switch(level2[i][j]){
										case 2:
											var placeCrate = cratesArrayLv2[i][j];
											placeCrate.setTexture(g_ressources[16]);
											//placeCrate.setPosition(165+25*j,186-25*i);
											break;
									}
								}
							}
					    }
					},playerSpriteLv2);
					playerSpriteLv2.runAction(leftLv2);
					playerSpriteLv2.runAction(cc.sequence(actionMove,func)); 
					break;
				//upLv2
				case 8:
					playerSpriteLv2.setFlippedX(false);
					// Move the sprite
				    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerPositionLv2.x, 
				    	600-50*playerPositionLv2.y));
				    var func=new cc.CallFunc(function(){
						console.log("Termina Movimiento arriba");
						//playerSpriteLv2.stopAction(actionMove);
						playerSpriteLv2.stopAction(upLv2);
						playerSpriteLv2.setTexture(g_ressources[13]);
						//Verificamos si ha ganado
					    if(maxCratesLv2 === counterCratesLv2){
					    	console.log("Win");
					    	var funcT=new cc.CallFunc(function(){
								console.log("Termina Win Anim");
								//playerSpriteLv2.stopAction(actionMove);
								//playerSpriteLv2.stopAction(animWinLv2);
								//playerSpriteLv2.setTexture(g_ressources[11]);
							},playerSpriteLv2);
					    	playerSpriteLv2.runAction(cc.sequence(animWinLv2,funcT));
					    }

					    console.log('level2',level2);
						console.log('crates',cratesArrayLv2);
						console.log(level2[playerPositionLv2.y][playerPositionLv2.x]);
					    //Player over crate drop
					    if(level2[playerPositionLv2.y][playerPositionLv2.x] === 6){
					    	console.log("Crates Changed");
							var placeCrateSuccess = cratesArrayLv2[playerPositionLv2.y][playerPositionLv2.x];
							console.log('Crates',placeCrateSuccess);
					    	placeCrateSuccess.setTexture(g_ressources[17]);
					    }else{
						    //Not over crates drop
							for(i=0;i<11;i++){
								for(j=0;j<15;j++){
									switch(level2[i][j]){
										case 2:
											var placeCrate = cratesArrayLv2[i][j];
											placeCrate.setTexture(g_ressources[16]);
											//placeCrate.setPosition(165+25*j,186-25*i);
											break;
									}
								}
							}
					    }
					},playerSpriteLv2);
					playerSpriteLv2.runAction(upLv2);
					playerSpriteLv2.runAction(cc.sequence(actionMove,func)); 
					break;
				
			}

			if(level2[playerPositionLv2.y][playerPositionLv2.x] === 12 ){
		    	//Quitar Sprites de crates
		    	var coin = cratesArrayLv2[playerPositionLv2.y][playerPositionLv2.x];
		    	coin.stopAllActions();
		    	// Move the sprite
			    var actionMove = cc.MoveTo.create(0.5, cc.p(coin.x, coin.y + 50));
			    var funcT=new cc.CallFunc(function(){
					console.log("Termina Coin Anim");
					//playerSpriteLv2.stopAction(actionMove);
					coin.stopAllActions();
			    	layerTwo.removeChild(coin);
			    	cratesArrayLv2[playerPositionLv2.y][playerPositionLv2.x] = null;
			    	console.log("Mas dos");
					layerTwo.addscoreLv2(100);
			    	level2[playerPositionLv2.y][playerPositionLv2.x] = 4;
			    	console.log("level2 Changed",level2);
			    	console.log("Crates Coin",cratesArrayLv2);
				},coin);
		    	coin.runAction(actionMove);
		    	coin.runAction(cc.sequence(takedCoinLv2,funcT));
		    }
			break;
		case 10:
			level2[playerPositionLv2.y][playerPositionLv2.x]-=4;
			playerPositionLv2.x+=deltaX;
			playerPositionLv2.y+=deltaY;
			level2[playerPositionLv2.y][playerPositionLv2.x]+=4;
			switch(playerDirectionLv2){
				//downLv2 
				case 2:
					playerSpriteLv2.setFlippedX(false);
					// Move the sprite
				    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerPositionLv2.x, 
				    	600-50*playerPositionLv2.y));
				    var func=new cc.CallFunc(function(){
						console.log("Termina Movimiento abajo");
						//playerSpriteLv2.stopAction(actionMove);
						playerSpriteLv2.stopAction(downLv2);
						playerSpriteLv2.setTexture(g_ressources[11]);
						//Verificamos si ha ganado
					    if(maxCratesLv2 === counterCratesLv2){
					    	console.log("Win");
					    	playerSpriteLv2.stopAllActions();
					    	var funcT=new cc.CallFunc(function(){
								console.log("Termina Win Anim");
								//playerSpriteLv2.stopAction(actionMove);
								//playerSpriteLv2.stopAction(animWinLv2);
								//playerSpriteLv2.setTexture(g_ressources[11]);
							},playerSpriteLv2);
					    	playerSpriteLv2.runAction(cc.sequence(animWinLv2,funcT));
					    }	
					    console.log('level2',level2);
						console.log('crates',cratesArrayLv2);
						console.log(level2[playerPositionLv2.y][playerPositionLv2.x]);
					    //Player over crate drop
					    if(level2[playerPositionLv2.y][playerPositionLv2.x] === 6){
					    	console.log("Crates Changed");
							var placeCrateSuccess = cratesArrayLv2[playerPositionLv2.y][playerPositionLv2.x];
							console.log('Crates',placeCrateSuccess);
					    	placeCrateSuccess.setTexture(g_ressources[17]);
					    }else{
						    //Not over crates drop
							for(i=0;i<11;i++){
								for(j=0;j<15;j++){
									switch(level2[i][j]){
										case 2:
											var placeCrate = cratesArrayLv2[i][j];
											placeCrate.setTexture(g_ressources[16]);
											//placeCrate.setPosition(165+25*j,186-25*i);
											break;
									}
								}
							}
					    }
					},playerSpriteLv2);
					playerSpriteLv2.runAction(downLv2);
					playerSpriteLv2.runAction(cc.sequence(actionMove,func)); 
					break;
				//leftLv2
				case 4:
					// Move the sprite
				    playerSpriteLv2.setFlippedX(false);
				    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerPositionLv2.x, 
				    	600-50*playerPositionLv2.y));
				    var func=new cc.CallFunc(function(){
						console.log("Termina Movimiento izquierda");
						//playerSpriteLv2.stopAction(actionMove);
						playerSpriteLv2.stopAction(leftLv2);
						playerSpriteLv2.setTexture(g_ressources[12]);
						//Verificamos si ha ganado
					    if(maxCratesLv2 === counterCratesLv2){
					    	console.log("Win");
					    	var funcT=new cc.CallFunc(function(){
								console.log("Termina Win Anim");
								//playerSpriteLv2.stopAction(actionMove);
								//playerSpriteLv2.stopAction(animWinLv2);
								//playerSpriteLv2.setTexture(g_ressources[11]);
							},playerSpriteLv2);
					    	playerSpriteLv2.runAction(cc.sequence(animWinLv2,funcT));
					    }

					    console.log('level2',level2);
						console.log('crates',cratesArrayLv2);
						console.log(level2[playerPositionLv2.y][playerPositionLv2.x]);
					    //Player over crate drop
					    if(level2[playerPositionLv2.y][playerPositionLv2.x] === 6){
					    	console.log("Crates Changed");
							var placeCrateSuccess = cratesArrayLv2[playerPositionLv2.y][playerPositionLv2.x];
							console.log('Crates',placeCrateSuccess);
					    	placeCrateSuccess.setTexture(g_ressources[17]);
					    }else{
						    //Not over crates drop
							for(i=0;i<11;i++){
								for(j=0;j<15;j++){
									switch(level2[i][j]){
										case 2:
											var placeCrate = cratesArrayLv2[i][j];
											placeCrate.setTexture(g_ressources[16]);
											//placeCrate.setPosition(165+25*j,186-25*i);
											break;
									}
								}
							}
					    }
					},playerSpriteLv2);
					playerSpriteLv2.runAction(leftLv2);
					playerSpriteLv2.runAction(cc.sequence(actionMove,func)); 
					break;
				//rightLv2
				case 6:
					playerSpriteLv2.setFlippedX(true);
					// Move the sprite
				    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerPositionLv2.x, 
				    	600-50*playerPositionLv2.y));
				    var func=new cc.CallFunc(function(){
						console.log("Termina Movimiento derecha");
						//playerSpriteLv2.stopAction(actionMove);
						playerSpriteLv2.stopAction(leftLv2);
						playerSpriteLv2.setTexture(g_ressources[12]);
						//Verificamos si ha ganado
					    if(maxCratesLv2 === counterCratesLv2){
					    	console.log("Win");
					    	var funcT=new cc.CallFunc(function(){
								console.log("Termina Win Anim");
								//playerSpriteLv2.stopAction(actionMove);
								//playerSpriteLv2.stopAction(animWinLv2);
								//playerSpriteLv2.setTexture(g_ressources[11]);
							},playerSpriteLv2);
					    	playerSpriteLv2.runAction(cc.sequence(animWinLv2,funcT));
					    }

					    console.log('level2',level2);
						console.log('crates',cratesArrayLv2);
						console.log(level2[playerPositionLv2.y][playerPositionLv2.x]);
					    //Player over crate drop
					    if(level2[playerPositionLv2.y][playerPositionLv2.x] === 6){
					    	console.log("Crates Changed");
							var placeCrateSuccess = cratesArrayLv2[playerPositionLv2.y][playerPositionLv2.x];
							console.log('Crates',placeCrateSuccess);
					    	placeCrateSuccess.setTexture(g_ressources[17]);
					    }else{
						    //Not over crates drop
							for(i=0;i<11;i++){
								for(j=0;j<15;j++){
									switch(level2[i][j]){
										case 2:
											var placeCrate = cratesArrayLv2[i][j];
											placeCrate.setTexture(g_ressources[16]);
											//placeCrate.setPosition(165+25*j,186-25*i);
											break;
									}
								}
							}
					    }
					},playerSpriteLv2);
					playerSpriteLv2.runAction(leftLv2);
					playerSpriteLv2.runAction(cc.sequence(actionMove,func)); 
					break;
				//upLv2
				case 8:
					playerSpriteLv2.setFlippedX(false);
					// Move the sprite
				    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerPositionLv2.x, 
				    	600-50*playerPositionLv2.y));
				    var func=new cc.CallFunc(function(){
						console.log("Termina Movimiento arriba");
						//playerSpriteLv2.stopAction(actionMove);
						playerSpriteLv2.stopAction(upLv2);
						playerSpriteLv2.setTexture(g_ressources[13]);
						//Verificamos si ha ganado
					    if(maxCratesLv2 === counterCratesLv2){
					    	console.log("Win");
					    	var funcT=new cc.CallFunc(function(){
								console.log("Termina Win Anim");
								//playerSpriteLv2.stopAction(actionMove);
								//playerSpriteLv2.stopAction(animWinLv2);
								//playerSpriteLv2.setTexture(g_ressources[11]);
							},playerSpriteLv2);
					    	playerSpriteLv2.runAction(cc.sequence(animWinLv2,funcT));
					    }

					    console.log('level2',level2);
						console.log('crates',cratesArrayLv2);
						console.log(level2[playerPositionLv2.y][playerPositionLv2.x]);
					    //Player over crate drop
					    if(level2[playerPositionLv2.y][playerPositionLv2.x] === 6){
					    	console.log("Crates Changed");
							var placeCrateSuccess = cratesArrayLv2[playerPositionLv2.y][playerPositionLv2.x];
							console.log('Crates',placeCrateSuccess);
					    	placeCrateSuccess.setTexture(g_ressources[17]);
					    }else{
						    //Not over crates drop
							for(i=0;i<11;i++){
								for(j=0;j<15;j++){
									switch(level2[i][j]){
										case 2:
											var placeCrate = cratesArrayLv2[i][j];
											placeCrate.setTexture(g_ressources[16]);
											//placeCrate.setPosition(165+25*j,186-25*i);
											break;
									}
								}
							}
					    }
					},playerSpriteLv2);
					playerSpriteLv2.runAction(upLv2);
					playerSpriteLv2.runAction(cc.sequence(actionMove,func)); 
					break;
				
			}
			break;
	}
	}
}

function movesLv2econdPlayer(deltaX,deltaY){
	if(playerSpriteLv2Two !== null && chronoOverLv2 === 0){
		switch(level2[playerTwoPositionLv2.y+deltaY][playerTwoPositionLv2.x+deltaX]){
		case 0:
		case 2:
			level2[playerTwoPositionLv2.y][playerTwoPositionLv2.x]-=4;
			playerTwoPositionLv2.x+=deltaX;
			playerTwoPositionLv2.y+=deltaY;
			level2[playerTwoPositionLv2.y][playerTwoPositionLv2.x]+=4;
			/*playerSpriteLv2.setPosition(165+25*playerPositionLv2.x,185-
			25*playerPositionLv2.y);*/
			switch(playerDirectionLv2){
				//downLv2 
				case 2:
					playerSpriteLv2Two.setFlippedX(false);
					playerSpriteLv2Two.stopAllActions();
					// Move the sprite
				    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerTwoPositionLv2.x, 
				    	600-50*playerTwoPositionLv2.y));
				    var func=new cc.CallFunc(function(){
						console.log("Termina Movimiento abajo");
						//playerSpriteLv2.stopAction(actionMove);
						playerSpriteLv2Two.stopAllActions();
						playerSpriteLv2Two.runAction(stand);
						//Verificamos si ha ganado
					    if(maxCratesLv2 === counterCratesLv2){
					    	console.log("Win");
					    	playerSpriteLv2Two.stopAllActions();
					    	var funcT=new cc.CallFunc(function(){
								console.log("Termina Win Anim");
								//playerSpriteLv2.stopAction(actionMove);
								//playerSpriteLv2Two.stopAllActions();
								//playerSpriteLv2Two.runAction(Stand);
							},playerSpriteLv2Two);
					    	playerSpriteLv2Two.runAction(cc.sequence(animWinLv2Two,funcT));
					    }	
					    console.log('level2',level2);
						console.log('crates',cratesArrayLv2);
						console.log(level2[playerTwoPositionLv2.y][playerTwoPositionLv2.x]);
					    //Player over crate drop
					    if(level2[playerTwoPositionLv2.y][playerTwoPositionLv2.x] === 6){
					    	console.log("Crates Changed");
							var placeCrateSuccess = cratesArrayLv2[playerTwoPositionLv2.y][playerTwoPositionLv2.x];
							console.log('Crates',placeCrateSuccess);
					    	placeCrateSuccess.setTexture(g_ressources[17]);
					    }else{
						    //Not over crates drop
							for(i=0;i<11;i++){
								for(j=0;j<15;j++){
									switch(level2[i][j]){
										case 2:
											var placeCrate = cratesArrayLv2[i][j];
											placeCrate.setTexture(g_ressources[16]);
											//placeCrate.setPosition(165+25*j,186-25*i);
											break;
									}
								}
							}
					    }
					},playerSpriteLv2Two);
					playerSpriteLv2Two.runAction(sdownLv2);
					playerSpriteLv2Two.runAction(cc.sequence(actionMove,func)); 
					break;
				//leftLv2
				case 4:
					// Move the sprite
				    playerSpriteLv2Two.setFlippedX(true);
				    playerSpriteLv2Two.stopAllActions();
				    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerTwoPositionLv2.x, 
				    	600-50*playerTwoPositionLv2.y));
				    var func=new cc.CallFunc(function(){
						console.log("Termina Movimiento izquierda");
						//playerSpriteLv2Two.stopAction(actionMove);
						playerSpriteLv2Two.stopAllActions();
						playerSpriteLv2Two.runAction(stand_leftLv2);
						//Verificamos si ha ganado
					    if(maxCratesLv2 === counterCratesLv2){
					    	console.log("Win");
					    	playerSpriteLv2Two.stopAllActions();
					    	var funcT=new cc.CallFunc(function(){
								console.log("Termina Win Anim");
								//playerSpriteLv2Two.stopAction(actionMove);
								//playerSpriteLv2Two.stopAllActions();
								//playerSpriteLv2Two.runAction(stand);
							},playerSpriteLv2Two);
					    	playerSpriteLv2Two.runAction(cc.sequence(animWinLv2Two,funcT));
					    }

					    console.log('level2',level2);
						console.log('crates',cratesArrayLv2);
						console.log(level2[playerTwoPositionLv2.y][playerTwoPositionLv2.x]);
					    //Player over crate drop
					    if(level2[playerTwoPositionLv2.y][playerTwoPositionLv2.x] === 6){
					    	console.log("Crates Changed");
							var placeCrateSuccess = cratesArrayLv2[playerTwoPositionLv2.y][playerTwoPositionLv2.x];
							console.log('Crates',placeCrateSuccess);
					    	placeCrateSuccess.setTexture(g_ressources[17]);
					    }else{
						    //Not over crates drop
							for(i=0;i<11;i++){
								for(j=0;j<15;j++){
									switch(level2[i][j]){
										case 2:
											var placeCrate = cratesArrayLv2[i][j];
											placeCrate.setTexture(g_ressources[16]);
											//placeCrate.setPosition(165+25*j,186-25*i);
											break;
									}
								}
							}
					    }
					},playerSpriteLv2Two);
					playerSpriteLv2Two.runAction(sleftLv2);
					playerSpriteLv2Two.runAction(cc.sequence(actionMove,func)); 
					break;
				//rightLv2
				case 6:
					playerSpriteLv2Two.setFlippedX(false);
					playerSpriteLv2Two.stopAllActions();
					// Move the sprite
				    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerTwoPositionLv2.x, 
				    	600-50*playerTwoPositionLv2.y));
				    var func=new cc.CallFunc(function(){
						console.log("Termina Movimiento derecha");
						//playerSpriteLv2Two.stopAction(actionMove);
						playerSpriteLv2Two.stopAllActions();
						playerSpriteLv2Two.runAction(stand_leftLv2);
						//Verificamos si ha ganado
					    if(maxCratesLv2 === counterCratesLv2){
					    	console.log("Win");
					    	playerSpriteLv2Two.stopAllActions();
					    	var funcT=new cc.CallFunc(function(){
								console.log("Termina Win Anim");
								//playerSpriteLv2Two.stopAction(actionMove);
								//playerSpriteLv2Two.stopAllActions();
								//playerSpriteLv2Two.runAction(stand);
							},playerSpriteLv2Two);
					    	playerSpriteLv2Two.runAction(cc.sequence(animWinLv2Two,funcT));
					    }

					    console.log('level2',level2);
						console.log('crates',cratesArrayLv2);
						console.log(level2[playerTwoPositionLv2.y][playerTwoPositionLv2.x]);
					    //Player over crate drop
					    if(level2[playerTwoPositionLv2.y][playerTwoPositionLv2.x] === 6){
					    	console.log("Crates Changed");
							var placeCrateSuccess = cratesArrayLv2[playerTwoPositionLv2.y][playerTwoPositionLv2.x];
							console.log('Crates',placeCrateSuccess);
					    	placeCrateSuccess.setTexture(g_ressources[17]);
					    }else{
						    //Not over crates drop
							for(i=0;i<11;i++){
								for(j=0;j<15;j++){
									switch(level2[i][j]){
										case 2:
											var placeCrate = cratesArrayLv2[i][j];
											placeCrate.setTexture(g_ressources[16]);
											//placeCrate.setPosition(165+25*j,186-25*i);
											break;
									}
								}
							}
					    }
					},playerSpriteLv2Two);
					playerSpriteLv2Two.runAction(sleftLv2);
					playerSpriteLv2Two.runAction(cc.sequence(actionMove,func)); 
					break;
				//upLv2
				case 8:
					playerSpriteLv2Two.setFlippedX(false);
					playerSpriteLv2Two.stopAllActions();
					// Move the sprite
				    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerTwoPositionLv2.x, 
				    	600-50*playerTwoPositionLv2.y));
				    var func=new cc.CallFunc(function(){
						console.log("Termina Movimiento arriba");
						//playerSpriteLv2Two.stopAction(actionMove);
						playerSpriteLv2Two.stopAllActions();
						playerSpriteLv2Two.setTexture(g_ressources[28]);
						//Verificamos si ha ganado
					    if(maxCratesLv2 === counterCratesLv2){
					    	console.log("Win");
					    	playerSpriteLv2Two.stopAllActions();
					    	var funcT=new cc.CallFunc(function(){
								console.log("Termina Win Anim");
								//playerSpriteLv2Two.stopAction(actionMove);
								//playerSpriteLv2Two.stopAllActions();
								//playerSpriteLv2Two.runAction(stand);
							},playerSpriteLv2Two);
					    	playerSpriteLv2Two.runAction(cc.sequence(animWinLv2Two,funcT));
					    }

					    console.log('level2',level2);
						console.log('crates',cratesArrayLv2);
						console.log(level2[playerTwoPositionLv2.y][playerTwoPositionLv2.x]);
					    //Player over crate drop
					    if(level2[playerTwoPositionLv2.y][playerTwoPositionLv2.x] === 6){
					    	console.log("Crates Changed");
							var placeCrateSuccess = cratesArrayLv2[playerTwoPositionLv2.y][playerTwoPositionLv2.x];
							console.log('Crates',placeCrateSuccess);
					    	placeCrateSuccess.setTexture(g_ressources[17]);
					    }else{
						    //Not over crates drop
							for(i=0;i<11;i++){
								for(j=0;j<15;j++){
									switch(level2[i][j]){
										case 2:
											var placeCrate = cratesArrayLv2[i][j];
											placeCrate.setTexture(g_ressources[16]);
											//placeCrate.setPosition(165+25*j,186-25*i);
											break;
									}
								}
							}
					    }
					},playerSpriteLv2Two);
					playerSpriteLv2Two.runAction(supLv2);
					playerSpriteLv2Two.runAction(cc.sequence(actionMove,func)); 
					break;
				
			}
		    
			/*playerSpriteLv2.setPosition(50+50*playerPositionLv2.x,600-
			50*playerPositionLv2.y);*/
			break;
		case 3:
		case 5:
			if(level2[playerTwoPositionLv2.y+deltaY*2][playerTwoPositionLv2.x+deltaX*2]==0
			|| level2[playerTwoPositionLv2.y+deltaY*2][playerTwoPositionLv2.x+deltaX*2]==2){
				level2[playerTwoPositionLv2.y][playerTwoPositionLv2.x]-=4;
				playerTwoPositionLv2.x+=deltaX;
				playerTwoPositionLv2.y+=deltaY;
				level2[playerTwoPositionLv2.y][playerTwoPositionLv2.x]+=1;
				/*playerSpriteLv2.setPosition(165+25*playerPositionLv2.x,185-
				25*playerPositionLv2.y);*/
				// Move the sprite
			    /*var actionMove = cc.MoveTo.create(1, cc.p(50+50*playerPositionLv2.x, 
			    	600-50*playerPositionLv2.y));
			    var func=new cc.CallFunc(function(){
					console.log("Termina Movimiento");
					playerSpriteLv2.stopAction(actionMove);
				},playerSpriteLv2);
			    playerSpriteLv2.runAction(cc.sequence(actionMove,func));*/
			    switch(playerDirectionLv2){
				//downLv2 
				case 2:
					playerSpriteLv2Two.setFlippedX(false);
					playerSpriteLv2Two.stopAllActions();
					// Move the sprite
				    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerTwoPositionLv2.x, 
				    	600-50*playerTwoPositionLv2.y));
				    var func=new cc.CallFunc(function(){
						console.log("Termina Movimiento abajo");
						//playerSpriteLv2.stopAction(actionMove);
						playerSpriteLv2Two.stopAllActions();
						playerSpriteLv2Two.runAction(stand);
						//Verificamos si ha ganado
					    if(maxCratesLv2 === counterCratesLv2){
					    	console.log("Win");
					    	playerSpriteLv2Two.stopAllActions();
					    	var funcT=new cc.CallFunc(function(){
								console.log("Termina Win Anim");
								//playerSpriteLv2.stopAction(actionMove);
								//playerSpriteLv2Two.stopAllActions();
								//playerSpriteLv2Two.runAction(Stand);
							},playerSpriteLv2Two);
					    	playerSpriteLv2Two.runAction(cc.sequence(animWinLv2Two,funcT));
					    }	
					    console.log('level2',level2);
						console.log('crates',cratesArrayLv2);
						console.log(level2[playerTwoPositionLv2.y][playerTwoPositionLv2.x]);
					    //Player over crate drop
					    if(level2[playerTwoPositionLv2.y][playerTwoPositionLv2.x] === 6){
					    	console.log("Crates Changed");
							var placeCrateSuccess = cratesArrayLv2[playerTwoPositionLv2.y][playerTwoPositionLv2.x];
							console.log('Crates',placeCrateSuccess);
					    	placeCrateSuccess.setTexture(g_ressources[17]);
					    }else{
						    //Not over crates drop
							for(i=0;i<11;i++){
								for(j=0;j<15;j++){
									switch(level2[i][j]){
										case 2:
											var placeCrate = cratesArrayLv2[i][j];
											placeCrate.setTexture(g_ressources[16]);
											//placeCrate.setPosition(165+25*j,186-25*i);
											break;
									}
								}
							}
					    }
					},playerSpriteLv2Two);
					playerSpriteLv2Two.runAction(sdownLv2);
					playerSpriteLv2Two.runAction(cc.sequence(actionMove,func)); 
					break;
				//leftLv2
				case 4:
					// Move the sprite
				    playerSpriteLv2Two.setFlippedX(true);
				    playerSpriteLv2Two.stopAllActions();
				    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerTwoPositionLv2.x, 
				    	600-50*playerTwoPositionLv2.y));
				    var func=new cc.CallFunc(function(){
						console.log("Termina Movimiento izquierda");
						//playerSpriteLv2Two.stopAction(actionMove);
						playerSpriteLv2Two.stopAllActions();
						playerSpriteLv2Two.runAction(stand_leftLv2);
						//Verificamos si ha ganado
					    if(maxCratesLv2 === counterCratesLv2){
					    	console.log("Win");
					    	playerSpriteLv2Two.stopAllActions();
					    	var funcT=new cc.CallFunc(function(){
								console.log("Termina Win Anim");
								//playerSpriteLv2Two.stopAction(actionMove);
								//playerSpriteLv2Two.stopAllActions();
								//playerSpriteLv2Two.runAction(stand);
							},playerSpriteLv2Two);
					    	playerSpriteLv2Two.runAction(cc.sequence(animWinLv2Two,funcT));
					    }

					    console.log('level2',level2);
						console.log('crates',cratesArrayLv2);
						console.log(level2[playerTwoPositionLv2.y][playerTwoPositionLv2.x]);
					    //Player over crate drop
					    if(level2[playerTwoPositionLv2.y][playerTwoPositionLv2.x] === 6){
					    	console.log("Crates Changed");
							var placeCrateSuccess = cratesArrayLv2[playerTwoPositionLv2.y][playerTwoPositionLv2.x];
							console.log('Crates',placeCrateSuccess);
					    	placeCrateSuccess.setTexture(g_ressources[17]);
					    }else{
						    //Not over crates drop
							for(i=0;i<11;i++){
								for(j=0;j<15;j++){
									switch(level2[i][j]){
										case 2:
											var placeCrate = cratesArrayLv2[i][j];
											placeCrate.setTexture(g_ressources[16]);
											//placeCrate.setPosition(165+25*j,186-25*i);
											break;
									}
								}
							}
					    }
					},playerSpriteLv2Two);
					playerSpriteLv2Two.runAction(sleftLv2);
					playerSpriteLv2Two.runAction(cc.sequence(actionMove,func)); 
					break;
				//rightLv2
				case 6:
					playerSpriteLv2Two.setFlippedX(false);
					playerSpriteLv2Two.stopAllActions();
					// Move the sprite
				    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerTwoPositionLv2.x, 
				    	600-50*playerTwoPositionLv2.y));
				    var func=new cc.CallFunc(function(){
						console.log("Termina Movimiento derecha");
						//playerSpriteLv2Two.stopAction(actionMove);
						playerSpriteLv2Two.stopAllActions();
						playerSpriteLv2Two.runAction(stand_leftLv2);
						//Verificamos si ha ganado
					    if(maxCratesLv2 === counterCratesLv2){
					    	console.log("Win");
					    	playerSpriteLv2Two.stopAllActions();
					    	var funcT=new cc.CallFunc(function(){
								console.log("Termina Win Anim");
								//playerSpriteLv2Two.stopAction(actionMove);
								//playerSpriteLv2Two.stopAllActions();
								//playerSpriteLv2Two.runAction(stand);
							},playerSpriteLv2Two);
					    	playerSpriteLv2Two.runAction(cc.sequence(animWinLv2Two,funcT));
					    }

					    console.log('level2',level2);
						console.log('crates',cratesArrayLv2);
						console.log(level2[playerTwoPositionLv2.y][playerTwoPositionLv2.x]);
					    //Player over crate drop
					    if(level2[playerTwoPositionLv2.y][playerTwoPositionLv2.x] === 6){
					    	console.log("Crates Changed");
							var placeCrateSuccess = cratesArrayLv2[playerTwoPositionLv2.y][playerTwoPositionLv2.x];
							console.log('Crates',placeCrateSuccess);
					    	placeCrateSuccess.setTexture(g_ressources[17]);
					    }else{
						    //Not over crates drop
							for(i=0;i<11;i++){
								for(j=0;j<15;j++){
									switch(level2[i][j]){
										case 2:
											var placeCrate = cratesArrayLv2[i][j];
											placeCrate.setTexture(g_ressources[16]);
											//placeCrate.setPosition(165+25*j,186-25*i);
											break;
									}
								}
							}
					    }
					},playerSpriteLv2Two);
					playerSpriteLv2Two.runAction(sleftLv2);
					playerSpriteLv2Two.runAction(cc.sequence(actionMove,func)); 
					break;
				//upLv2
				case 8:
					playerSpriteLv2Two.setFlippedX(false);
					playerSpriteLv2Two.stopAllActions();
					// Move the sprite
				    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerTwoPositionLv2.x, 
				    	600-50*playerTwoPositionLv2.y));
				    var func=new cc.CallFunc(function(){
						console.log("Termina Movimiento arriba");
						//playerSpriteLv2Two.stopAction(actionMove);
						playerSpriteLv2Two.stopAllActions();
						playerSpriteLv2Two.setTexture(g_ressources[28]);
						//Verificamos si ha ganado
					    if(maxCratesLv2 === counterCratesLv2){
					    	console.log("Win");
					    	playerSpriteLv2Two.stopAllActions();
					    	var funcT=new cc.CallFunc(function(){
								console.log("Termina Win Anim");
								//playerSpriteLv2Two.stopAction(actionMove);
								//playerSpriteLv2Two.stopAllActions();
								//playerSpriteLv2Two.runAction(stand);
							},playerSpriteLv2Two);
					    	playerSpriteLv2Two.runAction(cc.sequence(animWinLv2Two,funcT));
					    }

					    console.log('level2',level2);
						console.log('crates',cratesArrayLv2);
						console.log(level2[playerTwoPositionLv2.y][playerTwoPositionLv2.x]);
					    //Player over crate drop
					    if(level2[playerTwoPositionLv2.y][playerTwoPositionLv2.x] === 6){
					    	console.log("Crates Changed");
							var placeCrateSuccess = cratesArrayLv2[playerTwoPositionLv2.y][playerTwoPositionLv2.x];
							console.log('Crates',placeCrateSuccess);
					    	placeCrateSuccess.setTexture(g_ressources[17]);
					    }else{
						    //Not over crates drop
							for(i=0;i<11;i++){
								for(j=0;j<15;j++){
									switch(level2[i][j]){
										case 2:
											var placeCrate = cratesArrayLv2[i][j];
											placeCrate.setTexture(g_ressources[16]);
											//placeCrate.setPosition(165+25*j,186-25*i);
											break;
									}
								}
							}
					    }
					},playerSpriteLv2Two);
					playerSpriteLv2Two.runAction(supLv2);
					playerSpriteLv2Two.runAction(cc.sequence(actionMove,func)); 
					break;
				
			}
				/*playerSpriteLv2Two.setPosition(50+50*playerPositionLv2.x,600-
				50*playerPositionLv2.y);*/
				level2[playerTwoPositionLv2.y+deltaY][playerTwoPositionLv2.x+deltaX]+=3;
				var movingCrate = cratesArrayLv2[playerTwoPositionLv2.y]
				[playerTwoPositionLv2.x];
				/*movingCrate.setPosition(movingCrate.getPosition().
				x+25*deltaX,movingCrate.getPosition().y-25*deltaY);*/
				
			    var actionMoveCrate = cc.MoveTo.create(0.5, cc.p(movingCrate.getPosition().x+50*deltaX
			    	,movingCrate.getPosition().y-50*deltaY));
			    var funcCrate=new cc.CallFunc(function(){
					console.log("Termina Movimiento");
					playerSpriteLv2Two.stopAction(actionMoveCrate);
				},movingCrate);
			    movingCrate.runAction(cc.sequence(actionMoveCrate,funcCrate));
			    //Crate over crate Drop
			    if(level2[playerTwoPositionLv2.y+deltaY][playerTwoPositionLv2.x+deltaX] === 5 ){
			    	//Ha colocado en el lugar correcto el objeto
			    	var placeCrateSuccess = cratesArrayLv2[playerTwoPositionLv2.y+deltaY][playerTwoPositionLv2.x+deltaX];
			    	placeCrateSuccess.setTexture(g_ressources[17]);
			    	counterCratesLv2++;
			    	layerTwo.addscoreLv2(250*counterCratesLv2);
			    }/*else{
			    	var placeCrateSuccess = level2[playerPositionLv2.y+deltaY][playerPositionLv2.x+deltaX];
			    	placeCrateSuccess.setTexture(g_ressources[16]);
			    }*/

				/*movingCrate.setPosition(movingCrate.getPosition().
				x+50*deltaX,movingCrate.getPosition().y-50*deltaY);*/

				cratesArrayLv2[playerTwoPositionLv2.y+deltaY][playerTwoPositionLv2.
				x+deltaX]=movingCrate;
				cratesArrayLv2[playerTwoPositionLv2.y][playerTwoPositionLv2.x]=null;
			}
		break;
		case 8:
			level2[playerTwoPositionLv2.y][playerTwoPositionLv2.x]-=4;
			playerTwoPositionLv2.x+=deltaX;
			playerTwoPositionLv2.y+=deltaY;
			level2[playerTwoPositionLv2.y][playerTwoPositionLv2.x]+=4;
			console.log('level2 Player',level2[playerTwoPositionLv2.y][playerTwoPositionLv2.x]);
			switch(playerDirectionLv2){
				//downLv2 
				case 2:
					playerSpriteLv2Two.setFlippedX(false);
					playerSpriteLv2Two.stopAllActions();
					// Move the sprite
				    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerTwoPositionLv2.x, 
				    	600-50*playerTwoPositionLv2.y));
				    var func=new cc.CallFunc(function(){
						console.log("Termina Movimiento abajo");
						//playerSpriteLv2.stopAction(actionMove);
						playerSpriteLv2Two.stopAllActions();
						playerSpriteLv2Two.runAction(stand);
						//Verificamos si ha ganado
					    if(maxCratesLv2 === counterCratesLv2){
					    	console.log("Win");
					    	playerSpriteLv2Two.stopAllActions();
					    	var funcT=new cc.CallFunc(function(){
								console.log("Termina Win Anim");
								//playerSpriteLv2.stopAction(actionMove);
								//playerSpriteLv2Two.stopAllActions();
								//playerSpriteLv2Two.runAction(Stand);
							},playerSpriteLv2Two);
					    	playerSpriteLv2Two.runAction(cc.sequence(animWinLv2Two,funcT));
					    }	
					    console.log('level2',level2);
						console.log('crates',cratesArrayLv2);
						console.log(level2[playerTwoPositionLv2.y][playerTwoPositionLv2.x]);
					    //Player over crate drop
					    if(level2[playerTwoPositionLv2.y][playerTwoPositionLv2.x] === 6){
					    	console.log("Crates Changed");
							var placeCrateSuccess = cratesArrayLv2[playerTwoPositionLv2.y][playerTwoPositionLv2.x];
							console.log('Crates',placeCrateSuccess);
					    	placeCrateSuccess.setTexture(g_ressources[17]);
					    }else{
						    //Not over crates drop
							for(i=0;i<11;i++){
								for(j=0;j<15;j++){
									switch(level2[i][j]){
										case 2:
											var placeCrate = cratesArrayLv2[i][j];
											placeCrate.setTexture(g_ressources[16]);
											//placeCrate.setPosition(165+25*j,186-25*i);
											break;
									}
								}
							}
					    }
					},playerSpriteLv2Two);
					playerSpriteLv2Two.runAction(sdownLv2);
					playerSpriteLv2Two.runAction(cc.sequence(actionMove,func)); 
					break;
				//leftLv2
				case 4:
					// Move the sprite
				    playerSpriteLv2Two.setFlippedX(true);
				    playerSpriteLv2Two.stopAllActions();
				    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerTwoPositionLv2.x, 
				    	600-50*playerTwoPositionLv2.y));
				    var func=new cc.CallFunc(function(){
						console.log("Termina Movimiento izquierda");
						//playerSpriteLv2Two.stopAction(actionMove);
						playerSpriteLv2Two.stopAllActions();
						playerSpriteLv2Two.runAction(stand_leftLv2);
						//Verificamos si ha ganado
					    if(maxCratesLv2 === counterCratesLv2){
					    	console.log("Win");
					    	playerSpriteLv2Two.stopAllActions();
					    	var funcT=new cc.CallFunc(function(){
								console.log("Termina Win Anim");
								//playerSpriteLv2Two.stopAction(actionMove);
								//playerSpriteLv2Two.stopAllActions();
								//playerSpriteLv2Two.runAction(stand);
							},playerSpriteLv2Two);
					    	playerSpriteLv2Two.runAction(cc.sequence(animWinLv2Two,funcT));
					    }

					    console.log('level2',level2);
						console.log('crates',cratesArrayLv2);
						console.log(level2[playerTwoPositionLv2.y][playerTwoPositionLv2.x]);
					    //Player over crate drop
					    if(level2[playerTwoPositionLv2.y][playerTwoPositionLv2.x] === 6){
					    	console.log("Crates Changed");
							var placeCrateSuccess = cratesArrayLv2[playerTwoPositionLv2.y][playerTwoPositionLv2.x];
							console.log('Crates',placeCrateSuccess);
					    	placeCrateSuccess.setTexture(g_ressources[17]);
					    }else{
						    //Not over crates drop
							for(i=0;i<11;i++){
								for(j=0;j<15;j++){
									switch(level2[i][j]){
										case 2:
											var placeCrate = cratesArrayLv2[i][j];
											placeCrate.setTexture(g_ressources[16]);
											//placeCrate.setPosition(165+25*j,186-25*i);
											break;
									}
								}
							}
					    }
					},playerSpriteLv2Two);
					playerSpriteLv2Two.runAction(sleftLv2);
					playerSpriteLv2Two.runAction(cc.sequence(actionMove,func)); 
					break;
				//rightLv2
				case 6:
					playerSpriteLv2Two.setFlippedX(false);
					playerSpriteLv2Two.stopAllActions();
					// Move the sprite
				    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerTwoPositionLv2.x, 
				    	600-50*playerTwoPositionLv2.y));
				    var func=new cc.CallFunc(function(){
						console.log("Termina Movimiento derecha");
						//playerSpriteLv2Two.stopAction(actionMove);
						playerSpriteLv2Two.stopAllActions();
						playerSpriteLv2Two.runAction(stand_leftLv2);
						//Verificamos si ha ganado
					    if(maxCratesLv2 === counterCratesLv2){
					    	console.log("Win");
					    	playerSpriteLv2Two.stopAllActions();
					    	var funcT=new cc.CallFunc(function(){
								console.log("Termina Win Anim");
								//playerSpriteLv2Two.stopAction(actionMove);
								//playerSpriteLv2Two.stopAllActions();
								//playerSpriteLv2Two.runAction(stand);
							},playerSpriteLv2Two);
					    	playerSpriteLv2Two.runAction(cc.sequence(animWinLv2Two,funcT));
					    }

					    console.log('level2',level2);
						console.log('crates',cratesArrayLv2);
						console.log(level2[playerTwoPositionLv2.y][playerTwoPositionLv2.x]);
					    //Player over crate drop
					    if(level2[playerTwoPositionLv2.y][playerTwoPositionLv2.x] === 6){
					    	console.log("Crates Changed");
							var placeCrateSuccess = cratesArrayLv2[playerTwoPositionLv2.y][playerTwoPositionLv2.x];
							console.log('Crates',placeCrateSuccess);
					    	placeCrateSuccess.setTexture(g_ressources[17]);
					    }else{
						    //Not over crates drop
							for(i=0;i<11;i++){
								for(j=0;j<15;j++){
									switch(level2[i][j]){
										case 2:
											var placeCrate = cratesArrayLv2[i][j];
											placeCrate.setTexture(g_ressources[16]);
											//placeCrate.setPosition(165+25*j,186-25*i);
											break;
									}
								}
							}
					    }
					},playerSpriteLv2Two);
					playerSpriteLv2Two.runAction(sleftLv2);
					playerSpriteLv2Two.runAction(cc.sequence(actionMove,func)); 
					break;
				//upLv2
				case 8:
					playerSpriteLv2Two.setFlippedX(false);
					playerSpriteLv2Two.stopAllActions();
					// Move the sprite
				    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerTwoPositionLv2.x, 
				    	600-50*playerTwoPositionLv2.y));
				    var func=new cc.CallFunc(function(){
						console.log("Termina Movimiento arriba");
						//playerSpriteLv2Two.stopAction(actionMove);
						playerSpriteLv2Two.stopAllActions();
						playerSpriteLv2Two.setTexture(g_ressources[28]);
						//Verificamos si ha ganado
					    if(maxCratesLv2 === counterCratesLv2){
					    	console.log("Win");
					    	playerSpriteLv2Two.stopAllActions();
					    	var funcT=new cc.CallFunc(function(){
								console.log("Termina Win Anim");
								//playerSpriteLv2Two.stopAction(actionMove);
								//playerSpriteLv2Two.stopAllActions();
								//playerSpriteLv2Two.runAction(stand);
							},playerSpriteLv2Two);
					    	playerSpriteLv2Two.runAction(cc.sequence(animWinLv2Two,funcT));
					    }

					    console.log('level2',level2);
						console.log('crates',cratesArrayLv2);
						console.log(level2[playerTwoPositionLv2.y][playerTwoPositionLv2.x]);
					    //Player over crate drop
					    if(level2[playerTwoPositionLv2.y][playerTwoPositionLv2.x] === 6){
					    	console.log("Crates Changed");
							var placeCrateSuccess = cratesArrayLv2[playerTwoPositionLv2.y][playerTwoPositionLv2.x];
							console.log('Crates',placeCrateSuccess);
					    	placeCrateSuccess.setTexture(g_ressources[17]);
					    }else{
						    //Not over crates drop
							for(i=0;i<11;i++){
								for(j=0;j<15;j++){
									switch(level2[i][j]){
										case 2:
											var placeCrate = cratesArrayLv2[i][j];
											placeCrate.setTexture(g_ressources[16]);
											//placeCrate.setPosition(165+25*j,186-25*i);
											break;
									}
								}
							}
					    }
					},playerSpriteLv2Two);
					playerSpriteLv2Two.runAction(supLv2);
					playerSpriteLv2Two.runAction(cc.sequence(actionMove,func)); 
					break;
				
			}

			if(level2[playerTwoPositionLv2.y][playerTwoPositionLv2.x] === 12 ){
		    	//Quitar Sprites de crates
		    	var coin = cratesArrayLv2[playerTwoPositionLv2.y][playerTwoPositionLv2.x];
		    	coin.stopAllActions();
		    	// Move the sprite
			    var actionMove = cc.MoveTo.create(0.5, cc.p(coin.x, coin.y + 50));
			    var funcT=new cc.CallFunc(function(){
					console.log("Termina Coin Anim");
					//playerSpriteLv2.stopAction(actionMove);
					coin.stopAllActions();
			    	layerTwo.removeChild(coin);
			    	cratesArrayLv2[playerTwoPositionLv2.y][playerTwoPositionLv2.x] = null;
			    	console.log("Mas dos");
					layerTwo.addscoreLv2(100);
			    	level2[playerTwoPositionLv2.y][playerTwoPositionLv2.x] = 4;
			    	console.log("level2 Changed",level2);
			    	console.log("Crates Coin",cratesArrayLv2);
				},coin);
		    	coin.runAction(actionMove);
		    	coin.runAction(cc.sequence(takedCoinLv2,funcT));
		    }
		break;
		case 10:
			level2[playerTwoPositionLv2.y][playerTwoPositionLv2.x]-=4;
			playerTwoPositionLv2.x+=deltaX;
			playerTwoPositionLv2.y+=deltaY;
			level2[playerTwoPositionLv2.y][playerTwoPositionLv2.x]+=4;
			switch(playerDirectionLv2){
				//downLv2 
				case 2:
					playerSpriteLv2Two.setFlippedX(false);
					playerSpriteLv2Two.stopAllActions();
					// Move the sprite
				    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerTwoPositionLv2.x, 
				    	600-50*playerTwoPositionLv2.y));
				    var func=new cc.CallFunc(function(){
						console.log("Termina Movimiento abajo");
						//playerSpriteLv2.stopAction(actionMove);
						playerSpriteLv2Two.stopAllActions();
						playerSpriteLv2Two.runAction(stand);
						//Verificamos si ha ganado
					    if(maxCratesLv2 === counterCratesLv2){
					    	console.log("Win");
					    	playerSpriteLv2Two.stopAllActions();
					    	var funcT=new cc.CallFunc(function(){
								console.log("Termina Win Anim");
								//playerSpriteLv2.stopAction(actionMove);
								//playerSpriteLv2Two.stopAllActions();
								//playerSpriteLv2Two.runAction(Stand);
							},playerSpriteLv2Two);
					    	playerSpriteLv2Two.runAction(cc.sequence(animWinLv2Two,funcT));
					    }	
					    console.log('level2',level2);
						console.log('crates',cratesArrayLv2);
						console.log(level2[playerTwoPositionLv2.y][playerTwoPositionLv2.x]);
					    //Player over crate drop
					    if(level2[playerTwoPositionLv2.y][playerTwoPositionLv2.x] === 6){
					    	console.log("Crates Changed");
							var placeCrateSuccess = cratesArrayLv2[playerTwoPositionLv2.y][playerTwoPositionLv2.x];
							console.log('Crates',placeCrateSuccess);
					    	placeCrateSuccess.setTexture(g_ressources[17]);
					    }else{
						    //Not over crates drop
							for(i=0;i<11;i++){
								for(j=0;j<15;j++){
									switch(level2[i][j]){
										case 2:
											var placeCrate = cratesArrayLv2[i][j];
											placeCrate.setTexture(g_ressources[16]);
											//placeCrate.setPosition(165+25*j,186-25*i);
											break;
									}
								}
							}
					    }
					},playerSpriteLv2Two);
					playerSpriteLv2Two.runAction(sdownLv2);
					playerSpriteLv2Two.runAction(cc.sequence(actionMove,func)); 
					break;
				//leftLv2
				case 4:
					// Move the sprite
				    playerSpriteLv2Two.setFlippedX(true);
				    playerSpriteLv2Two.stopAllActions();
				    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerTwoPositionLv2.x, 
				    	600-50*playerTwoPositionLv2.y));
				    var func=new cc.CallFunc(function(){
						console.log("Termina Movimiento izquierda");
						//playerSpriteLv2Two.stopAction(actionMove);
						playerSpriteLv2Two.stopAllActions();
						playerSpriteLv2Two.runAction(stand_leftLv2);
						//Verificamos si ha ganado
					    if(maxCratesLv2 === counterCratesLv2){
					    	console.log("Win");
					    	playerSpriteLv2Two.stopAllActions();
					    	var funcT=new cc.CallFunc(function(){
								console.log("Termina Win Anim");
								//playerSpriteLv2Two.stopAction(actionMove);
								//playerSpriteLv2Two.stopAllActions();
								//playerSpriteLv2Two.runAction(stand);
							},playerSpriteLv2Two);
					    	playerSpriteLv2Two.runAction(cc.sequence(animWinLv2Two,funcT));
					    }

					    console.log('level2',level2);
						console.log('crates',cratesArrayLv2);
						console.log(level2[playerTwoPositionLv2.y][playerTwoPositionLv2.x]);
					    //Player over crate drop
					    if(level2[playerTwoPositionLv2.y][playerTwoPositionLv2.x] === 6){
					    	console.log("Crates Changed");
							var placeCrateSuccess = cratesArrayLv2[playerTwoPositionLv2.y][playerTwoPositionLv2.x];
							console.log('Crates',placeCrateSuccess);
					    	placeCrateSuccess.setTexture(g_ressources[17]);
					    }else{
						    //Not over crates drop
							for(i=0;i<11;i++){
								for(j=0;j<15;j++){
									switch(level2[i][j]){
										case 2:
											var placeCrate = cratesArrayLv2[i][j];
											placeCrate.setTexture(g_ressources[16]);
											//placeCrate.setPosition(165+25*j,186-25*i);
											break;
									}
								}
							}
					    }
					},playerSpriteLv2Two);
					playerSpriteLv2Two.runAction(sleftLv2);
					playerSpriteLv2Two.runAction(cc.sequence(actionMove,func)); 
					break;
				//rightLv2
				case 6:
					playerSpriteLv2Two.setFlippedX(false);
					playerSpriteLv2Two.stopAllActions();
					// Move the sprite
				    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerTwoPositionLv2.x, 
				    	600-50*playerTwoPositionLv2.y));
				    var func=new cc.CallFunc(function(){
						console.log("Termina Movimiento derecha");
						//playerSpriteLv2Two.stopAction(actionMove);
						playerSpriteLv2Two.stopAllActions();
						playerSpriteLv2Two.runAction(stand_leftLv2);
						//Verificamos si ha ganado
					    if(maxCratesLv2 === counterCratesLv2){
					    	console.log("Win");
					    	playerSpriteLv2Two.stopAllActions();
					    	var funcT=new cc.CallFunc(function(){
								console.log("Termina Win Anim");
								//playerSpriteLv2Two.stopAction(actionMove);
								//playerSpriteLv2Two.stopAllActions();
								//playerSpriteLv2Two.runAction(stand);
							},playerSpriteLv2Two);
					    	playerSpriteLv2Two.runAction(cc.sequence(animWinLv2Two,funcT));
					    }

					    console.log('level2',level2);
						console.log('crates',cratesArrayLv2);
						console.log(level2[playerTwoPositionLv2.y][playerTwoPositionLv2.x]);
					    //Player over crate drop
					    if(level2[playerTwoPositionLv2.y][playerTwoPositionLv2.x] === 6){
					    	console.log("Crates Changed");
							var placeCrateSuccess = cratesArrayLv2[playerTwoPositionLv2.y][playerTwoPositionLv2.x];
							console.log('Crates',placeCrateSuccess);
					    	placeCrateSuccess.setTexture(g_ressources[17]);
					    }else{
						    //Not over crates drop
							for(i=0;i<11;i++){
								for(j=0;j<15;j++){
									switch(level2[i][j]){
										case 2:
											var placeCrate = cratesArrayLv2[i][j];
											placeCrate.setTexture(g_ressources[16]);
											//placeCrate.setPosition(165+25*j,186-25*i);
											break;
									}
								}
							}
					    }
					},playerSpriteLv2Two);
					playerSpriteLv2Two.runAction(sleftLv2);
					playerSpriteLv2Two.runAction(cc.sequence(actionMove,func)); 
					break;
				//upLv2
				case 8:
					playerSpriteLv2Two.setFlippedX(false);
					playerSpriteLv2Two.stopAllActions();
					// Move the sprite
				    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerTwoPositionLv2.x, 
				    	600-50*playerTwoPositionLv2.y));
				    var func=new cc.CallFunc(function(){
						console.log("Termina Movimiento arriba");
						//playerSpriteLv2Two.stopAction(actionMove);
						playerSpriteLv2Two.stopAllActions();
						playerSpriteLv2Two.setTexture(g_ressources[28]);
						//Verificamos si ha ganado
					    if(maxCratesLv2 === counterCratesLv2){
					    	console.log("Win");
					    	playerSpriteLv2Two.stopAllActions();
					    	var funcT=new cc.CallFunc(function(){
								console.log("Termina Win Anim");
								//playerSpriteLv2Two.stopAction(actionMove);
								//playerSpriteLv2Two.stopAllActions();
								//playerSpriteLv2Two.runAction(stand);
							},playerSpriteLv2Two);
					    	playerSpriteLv2Two.runAction(cc.sequence(animWinLv2Two,funcT));
					    }

					    console.log('level2',level2);
						console.log('crates',cratesArrayLv2);
						console.log(level2[playerTwoPositionLv2.y][playerTwoPositionLv2.x]);
					    //Player over crate drop
					    if(level2[playerTwoPositionLv2.y][playerTwoPositionLv2.x] === 6){
					    	console.log("Crates Changed");
							var placeCrateSuccess = cratesArrayLv2[playerTwoPositionLv2.y][playerTwoPositionLv2.x];
							console.log('Crates',placeCrateSuccess);
					    	placeCrateSuccess.setTexture(g_ressources[17]);
					    }else{
						    //Not over crates drop
							for(i=0;i<11;i++){
								for(j=0;j<15;j++){
									switch(level2[i][j]){
										case 2:
											var placeCrate = cratesArrayLv2[i][j];
											placeCrate.setTexture(g_ressources[16]);
											//placeCrate.setPosition(165+25*j,186-25*i);
											break;
									}
								}
							}
					    }
					},playerSpriteLv2Two);
					playerSpriteLv2Two.runAction(supLv2);
					playerSpriteLv2Two.runAction(cc.sequence(actionMove,func)); 
					break;
				
			}
			break;
	}
	}
}

var SecondlevelScene = cc.Scene.extend({
 	onEnter:function () {
 		this._super();
 		reinitlevel2();
 		layerTwo = new Secondlevel2Layer();
 		this.addChild(layerTwo);
 	}
 });

function reinitlevel2(){
	level22 = [
		[0,0,0,0,0,0,0,1,1,1,1,1,0,0,0],
		[0,1,1,1,1,1,1,1,0,0,0,1,1,0,0],
		[1,1,2,1,0,4,1,1,0,3,3,0,1,0,0],
		[1,2,0,0,0,3,0,0,0,0,0,0,1,0,0],
		[1,2,0,3,0,0,1,1,1,0,0,4,1,0,0],
		[1,1,1,10,1,1,1,1,1,3,1,1,1,0,0],
		[1,0,3,0,0,1,1,1,0,0,2,1,1,0,0],
		[1,0,3,0,3,0,3,0,0,0,2,1,1,0,0],
		[1,2,0,0,0,1,1,1,0,0,2,1,1,0,0],
		[1,2,0,0,0,1,0,1,0,0,2,1,1,0,0],
		[1,0,0,1,1,1,0,1,1,1,1,1,1,0,0],
		[1,1,1,1,0,0,0,0,0,0,0,0,0,0,0]
		];
		cratesArrayLv2 = [];
        playerPositionLv2;
        playerSpriteLv2;
		// 2 downLv2
		// 4 leftLv2 
		// 5 stand
		// 6 rightLv2
		// 8 upLv2
		// 45 stand - leftLv2
		// 56 stand - rightLv2
		playerDirectionLv2 = 5;

	    playerTwoPositionLv2;
		 playerSpriteLv2Two;

		 startTouchLv2;
		 endTouchLv2;
		 swipeToleranceLv2 = 10;
		 leftLv2 = null;
		 rightLv2 = null;
		 upLv2 = null;
		 downLv2 = null;
		//Chrono Trigger
		 sleftLv2 = null;
		 srightLv2 = null;
		 supLv2 = null;
		 sdownLv2 = null;
		 stand = null;
		 stand_upLv2 = null;
		 stand_leftLv2 = null;
		 chrono_winLv2 = null;
		//Chrono Trigger
		 animateCrateLv2 = null;
		 animateCrateLv22 = null;
		 counterCratesLv2 = 0;
		 maxCratesLv2 = 0;
		 animWinLv2 = null;
		 animWinLv2Two = null;
		 counterPlayersLv2 = 0;
		 animateCoinsLv2 = null;
		 takedCoinLv2 = null;

		//Rock downLv2 Mario RPG
		 animateRockdownLv2 = null;

		//Mario over
		 animmarioOverLv2Lv2 = null;
		 animMOverupLv2 = null;
		 marioOverLv2 = 0;

		//Chrono Over
		 animchronoOverLv2Lv2 = null;
		 chronoOverLv2 = 0;
}