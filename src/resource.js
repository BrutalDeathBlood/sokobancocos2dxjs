var BackGround_png = "resource/background.png";
var Start_N_png = "resource/start_N.png";
var Start_S_png = "resource/start_S.png";
var Sushi_png = "resource/sushi_1n/sushi_1n.png"
var Sushi_plist  = "resource/sushi.plist";
var sushi_uno= "resource/Player.png";
var Sushi_png_2 = "resource/player_2.png";
var particle = "resource/wall.png";
var place = "resource/Planet_1.png";
var mario_plist = "resource/Mario.plist";
var mario = "resource/Mario.png";
var mario_stand = "resource/mario-walk-2.png";
var mario_stand_left = "resource/mario-walk-8.png";
var mario_stand_up = "resource/mario-walk-5.png";
var star_crate_plist = "resource/star_crate.plist";
var star_crate = "resource/star_crate.png";
var drop_crate_1 = "resource/crate_drop_1.png";
var drop_crate_2 = "resource/crate_drop_2.png";
var mario_win_plist = "resource/mario_win.plist";
var mario_win = "resource/mario_win.png";
var coins_plist = "resource/coins.plist";
var coins = "resource/coins.png";
var coins_taked_plist = "resource/coins_taked.plist";
var coins_taked = "resource/coins_taked.png";
var chrono_stand_plist = "resource/chrono_stand.plist";
var chrono_stand = "resource/chrono_stand.png";
var chrono_down_plist = "resource/chrono_down.plist";
var chrono_down = "resource/chrono_down.png";
var chrono_up_plist = "resource/chrono_up.plist";
var chrono_up = "resource/chrono_up.png";
var chrono_stand_up = "resource/chrono-stand-up.png";
var chrono_left_plist = "resource/chrono_left.plist";
var chrono_left = "resource/chrono_left.png";
var chrono_stand_left_plist = "resource/chrono_stand_left.plist";
var chrono_stand_left = "resource/chrono_stand_left.png";
var chrono_win_plist = "resource/chrono_win.plist";
var chrono_win = "resource/chrono_win.png";
var rock_down_plist = "resource/rock-down.plist";
var rock_down = "resource/rock-down.png";
var mario_over_plist = "resource/mario-over.plist";
var mario_over = "resource/mario-over.png";
var mario_over_up_plist = "resource/mario-over-up.plist";
var mario_over_up = "resource/mario-over-up.png";
var chrono_over_plist = "resource/chrono_over.plist";
var chrono_over = "resource/chrono_over.png";
var dkc_balloon_plist = "resource/dkc-balloon.plist";
var dkc_balloon = "resource/dkc-balloon.png";
var dkc_balloon_explode_plist = "resource/dkc-balloon-explode.plist";
var dkc_balloon_explode = "resource/dkc-balloon-explode.png";
var dkc_ball = "resource/dkc-balloon-1.png";
var mario_game_over_plist = "resource/mario-game-over.plist";
var mario_game_over = "resource/mario-game-over.png";
var chrono_game_over_plist = "resource/chrono-game-over.plist";
var chrono_game_over = "resource/chrono-game-over.png";
var g_ressources = [
	//image
    coins_taked_plist,
    coins_taked,
    Start_S_png,
    Sushi_png,
    Sushi_plist,
    Sushi_png_2,
    sushi_uno,
    particle,
    place,
    mario_plist, //9
    mario,       //10
    mario_stand, //11
    mario_stand_left, //12
    mario_stand_up, //13
    star_crate_plist, //14
    star_crate,      //15
    drop_crate_1,	 //16
    drop_crate_2,	 //17
    mario_win_plist, //18
    mario_win,       //19
    coins_plist,     //20
    coins,           //21 /* Chrono Trigger */ 
    chrono_stand_plist, //22
    chrono_stand,    //23
    chrono_down_plist,  //24
    chrono_down,        //25
    chrono_up_plist,    //26
    chrono_up,          //27
    chrono_stand_up,    //28
    chrono_left_plist,  //29
    chrono_left,        //30
    chrono_stand_left_plist, //31
    chrono_stand_left,   //32
    chrono_win_plist,    //33
    chrono_win,          //34
    rock_down_plist,     //35
    rock_down,           //36
    mario_over_plist,    //37
    mario_over,          //38
    mario_over_up_plist, //39
    mario_over_up,       //40
    chrono_over_plist,   //41
    chrono_over,         //42
    dkc_balloon_plist,   //43
    dkc_balloon,         //44
    dkc_balloon_explode_plist,  //45
    dkc_balloon_explode,    //46
    dkc_ball,                //47
    mario_game_over_plist,   //48
    mario_game_over,         //49
    chrono_game_over_plist,  //50
    chrono_game_over,         //51
    BackGround_png,//52
    Start_N_png, //53
    Start_S_png //54
];
