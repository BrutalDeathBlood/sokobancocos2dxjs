var layer;
var GameOverLayer = cc.Layer.extend({
	continueLabel: null,
	continues: 3,
 	ctor:function () {
 		this._super();
		var size = cc.winSize;

		//Agregamos plist a spriteFrameCache
 		cc.spriteFrameCache.addSpriteFrames(g_ressources[43]);
 		cc.spriteFrameCache.addSpriteFrames(g_ressources[45]);
 		cc.spriteFrameCache.addSpriteFrames(g_ressources[48]);
 		cc.spriteFrameCache.addSpriteFrames(g_ressources[50]);

 		//Agregamos los Frames a un Array
 		//Anim Move Left
		var animFramesBalloon = [];
		var str = "";
		for(var i = 1; i < 8; i++){
			str = "dkc-balloon-" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFramesBalloon.push(frame);
		}
		//Reverse Move
		for(var i = 7; i > 0; i--){
			str = "dkc-balloon-" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFramesBalloon.push(frame);
		}
		//this.setFlippedX(true);
		//Animación 
		var animMoveBalloon = cc.Animation.create(animFramesBalloon, 0.1);
		var animationBalloon = cc.RepeatForever.create(cc.Animate.create(animMoveBalloon));
		var animationBalloonOnce = cc.Animate.create(animMoveBalloon);

		//Animation Balloon Explode
		var animFramesBalloonExplode = [];
		var str = "";
		for(var i = 1; i < 13; i++){
			str = "dkc-balloon-explode-" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFramesBalloonExplode.push(frame);
		}
		var animMoveBalloonExplode = cc.Animation.create(animFramesBalloonExplode, 0.05);
		var animationBalloonExplode = cc.Animate.create(animMoveBalloonExplode);

		//Animation Mario
		var animFramesMario = [];
		var str = "";
		for(var i = 4; i < 11; i++){
			if(i !== 6){
				str = "mario-game-over-" + i + ".png";
				var frame = cc.spriteFrameCache.getSpriteFrame(str);
				animFramesMario.push(frame);
			}
		}
		animFramesMario.push("mario-game-over-9.png");
		animFramesMario.push("mario-game-over-10.png");
		animFramesMario.push("mario-game-over-9.png");
		animFramesMario.push("mario-game-over-10.png");
		animFramesMario.push("mario-game-over-9.png");
		for(var i = 10; i > 3; i--){
			if(i !== 6){
				str = "mario-game-over-" + i + ".png";
				var frame = cc.spriteFrameCache.getSpriteFrame(str);
				animFramesMario.push(frame);
			}
		}
		var animMario = cc.Animation.create(animFramesMario, 0.2);
		var animationMario= cc.RepeatForever.create(cc.Animate.create(animMario));

		//Animation Chrono
		//Animation Balloon Explode
		var animFramesChrono = [];
		var str = "";
		for(var i = 5; i > 0; i--){
			str = "chrono-game-over-" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFramesChrono.push(frame);
		}
		animFramesChrono.push("chrono-game-over-2.png");
		animFramesChrono.push("chrono-game-over-1.png");
		animFramesChrono.push("chrono-game-over-2.png");
		for(var i = 1; i < 6; i++){
			str = "chrono-game-over-" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFramesChrono.push(frame);
		}
		var animMoveChrono = cc.Animation.create(animFramesChrono, 0.3);
		var animationChrono = cc.RepeatForever.create(cc.Animate.create(animMoveChrono));

 		var scoreLabel = new cc.LabelTTF("Game Over", "Arial", 40);
		scoreLabel.attr({
			x:size.width / 2 - 50,
			y:size.height - 300
		});
		this.addChild(scoreLabel, 2);

		continueLabel = new cc.LabelTTF("3", "Arial", 30);
		continueLabel.attr({
			x: 100+50*8,
			y: 600-50*4
		});
		this.addChild(continueLabel,2);

		var balloonSprite = cc.Sprite.create(cc.spriteFrameCache.getSpriteFrame('dkc-balloon-1.png'));
		balloonSprite.setPosition(60+50*8,600-50*4);
		balloonSprite.setScale(1.5);
		var func = new cc.CallFunc(function(){
			var ballonFade = cc.FadeOut.create(0.3);
			var ballFadeIn = cc.FadeIn.create(0.1);
			balloonSprite.stopAllActions();
			var funcT = new cc.CallFunc(function(){
				console.log("CallBack");
				layer.continues--;
				balloonSprite.setTexture(g_ressources[47]);
				var funcY = new cc.CallFunc(function(){
					console.log("SCall Back");
					balloonSprite.stopAllActions();
					balloonSprite.runAction(animationBalloon);
					continueLabel.setString(""+layer.continues);
				},balloonSprite);
				balloonSprite.runAction(new cc.sequence(ballFadeIn,funcY));
			},balloonSprite);
			balloonSprite.runAction(new cc.sequence(ballonFade,funcT));
		},balloonSprite);
		balloonSprite.runAction(new cc.sequence(animationBalloonOnce,animationBalloonExplode,func));
		this.addChild(balloonSprite,1);

		//Mario Sprite
		var marioSprite = cc.Sprite.create(cc.spriteFrameCache.getSpriteFrame('mario-game-over-1.png'));
		marioSprite.setPosition(50+50*3,600-50*5);
		marioSprite.setScale(1.5);
		marioSprite.runAction(animationMario);
		this.addChild(marioSprite,1);

		//Chrono sprite
		var chronoSprite = cc.Sprite.create(cc.spriteFrameCache.getSpriteFrame('chrono-game-over-5.png'));
		chronoSprite.setPosition(50+50*9,600-50*5);
		chronoSprite.setScale(1.5);
		chronoSprite.runAction(animationChrono);
		this.addChild(chronoSprite,1);

		//Menu Item
		var TryAgainItem = new cc.MenuItemFont(
				"Try Again",
				function () {
					cc.log("Menu is clicked!");
					var transition = new cc.TransitionFade(1, new PlayScene(),cc.color(255,255,255,255));
					cc.director.runScene(transition);
				}, this);
		TryAgainItem.attr({
			x: size.width/2,
			y: size.height / 2 - 60,
			anchorX: 0.5,
			anchorY: 0.5
		});

		var menu = new cc.Menu(TryAgainItem);
		menu.x = 0;
		menu.y = 0;
		this.addChild(menu, 1);
	}
 });

 var GameOverScene = cc.Scene.extend({
 	onEnter:function () {
 		this._super();
 		layer = new GameOverLayer();
 		this.addChild(layer);
 	}
 });