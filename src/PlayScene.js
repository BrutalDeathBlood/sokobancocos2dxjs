/*var level = [
[1,1,1,1,1,1,1],
[1,1,0,0,0,0,1],
[1,1,3,0,2,0,1],
[1,0,0,4,0,0,1],
[1,0,3,1,2,0,1],
[1,0,0,1,1,1,1],
[1,1,1,1,1,1,1]
];*/ //7*7
//9*9 Matrix
//Primer Nivel
/*var level = [
[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
[1,1,0,0,0,0,1,1,1,1,1,1,1,1,1],
[1,1,3,0,2,0,4,0,1,1,1,1,1,1,1],
[1,0,0,4,0,0,0,0,1,1,1,1,1,1,1],
[1,0,3,1,2,0,0,0,1,1,1,1,1,1,1],
[1,0,0,1,1,1,1,0,1,1,1,1,1,1,1],
[1,1,0,1,1,1,1,0,1,1,1,1,1,1,1],
[1,0,0,0,0,0,0,8,1,1,1,1,1,1,1],
[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
[0,0,0,1,1,1,1,1,1,0,0,0,0,0,0]
];*/
var level = [
		[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
		[1,1,0,0,0,0,1,1,1,1,1,1,1,1,1],
		[1,1,3,0,2,0,8,0,1,1,1,1,1,1,1],
		[1,8,0,4,8,0,0,8,1,1,1,1,1,1,1],
		[1,0,3,1,2,0,8,0,1,1,1,1,1,1,1],
		[1,8,0,1,1,1,1,8,1,1,1,1,1,1,1],
		[1,1,0,1,1,1,1,0,1,1,1,1,1,1,1],
		[1,0,8,0,8,0,0,8,1,1,1,1,1,1,1],
		[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
		[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
		[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
		[0,0,0,1,1,1,1,1,1,0,0,0,0,0,0]
		];
//Segundo Nivel
/*var level = [
[0,0,0,0,0,0,0,1,1,1,1,1,0,0,0],
[0,1,1,1,1,1,1,1,0,0,0,1,1,0,0],
[1,1,2,1,0,4,1,1,0,3,3,0,1,0,0],
[1,2,0,0,0,3,0,0,0,0,0,0,1,0,0],
[1,2,0,3,0,0,1,1,1,0,0,4,1,0,0],
[1,1,1,10,1,1,1,1,1,3,1,1,1,0,0],
[1,0,3,0,0,1,1,1,0,0,2,1,1,0,0],
[1,0,3,0,3,0,3,0,0,0,2,1,1,0,0],
[1,2,0,0,0,1,1,1,0,0,2,1,1,0,0],
[1,2,0,0,0,1,0,1,0,0,2,1,1,0,0],
[1,0,0,1,1,1,0,1,1,1,1,1,1,0,0],
[1,1,1,1,0,0,0,0,0,0,0,0,0,0,0]
];*/
//Max 16 cols
//Max 12 rows

var cratesArray = [];
var coinsArray = []; 
var playerPosition;
var playerSprite;
// 2 down
// 4 left 
// 5 stand
// 6 right
// 8 up
// 45 stand - left
// 56 stand - right
var playerDirection = 5;

var playerTwoPosition;
var playerSpriteTwo;

var startTouch;
var endTouch;
var swipeTolerance = 10;
var left = null;
var right = null;
var up = null;
var down = null;
//Chrono Trigger
var sleft = null;
var sright = null;
var sup = null;
var sdown = null;
var stand = null;
var stand_up = null;
var stand_left = null;
var chrono_win = null;
//Chrono Trigger
var animateCrate = null;
var animateCrate2 = null;
var counterCrates = 0;
var maxCrates = 0;
var animWin = null;
var animWinTwo = null;
var counterPlayers = 0;
var animateCoins = null;
var takedCoin = null;
var animateCoinsArray = [];

//Rock Down Mario RPG
var animateRockDown = null;

//Mario over
var animMarioOver = null;
var animMOverUp = null;
var marioOver = 0;

//Chrono Over
var animChronoOver = null;
var chronoOver = 0;

var layer;

var PlayLayer = cc.Layer.extend({
 	bgSprite: null,
 	SushiSprites: null,
 	timeout : 60,
 	timeoutLabel: 60,
	scoreLabel: null,
	movesLabel: null,
	score: 0,
	moves: 50,
 	ctor:function () {
 		this._super();
 		this.SushiSprites = [];

		cc.eventManager.addListener(listener, this);

 		var size = cc.winSize;
 		//Agregamos plist a spriteFrameCache
 		cc.spriteFrameCache.addSpriteFrames(g_ressources[0]);
 		cc.spriteFrameCache.addSpriteFrames(g_ressources[9]);
 		cc.spriteFrameCache.addSpriteFrames(g_ressources[14]);
 		cc.spriteFrameCache.addSpriteFrames(g_ressources[18]);
 		cc.spriteFrameCache.addSpriteFrames(g_ressources[20]);
 		cc.spriteFrameCache.addSpriteFrames(g_ressources[22]);
 		cc.spriteFrameCache.addSpriteFrames(g_ressources[24]);
 		cc.spriteFrameCache.addSpriteFrames(g_ressources[26]);
 		cc.spriteFrameCache.addSpriteFrames(g_ressources[29]);
 		cc.spriteFrameCache.addSpriteFrames(g_ressources[31]);
 		cc.spriteFrameCache.addSpriteFrames(g_ressources[33]);
 		cc.spriteFrameCache.addSpriteFrames(g_ressources[35]);
 		cc.spriteFrameCache.addSpriteFrames(g_ressources[37]);
 		cc.spriteFrameCache.addSpriteFrames(g_ressources[39]);
 		cc.spriteFrameCache.addSpriteFrames(g_ressources[41]);


 		//Agregamos los Frames a un Array
 		//Anim Move Left
		var animFramesLeft = [];
		var str = "";
		for(var i = 7; i < 10; i+=2){
			str = "mario-walk-" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFramesLeft.push(frame);
		}
		//this.setFlippedX(true);
		//Animación 
		var animMoveLeft = cc.Animation.create(animFramesLeft, 0.1);
		left = cc.RepeatForever.create(cc.Animate.create(animMoveLeft));

		//Chrono Left
		//Anim Move Left
		var animFramesLeftChrono = [];
		var str = "";
		for(var i = 1; i < 7; i++){
			str = "chrono-left-" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFramesLeftChrono.push(frame);
		}
		//this.setFlippedX(true);
		//Animación 
		var animMoveLeftchrono = cc.Animation.create(animFramesLeftChrono, 0.1);
		sleft = cc.RepeatForever.create(cc.Animate.create(animMoveLeftchrono));


		//Anim Move Up
		var animFramesUp = [];
		var str = "";
		for(var i = 4; i < 7; i+=2){
			str = "mario-walk-" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFramesUp.push(frame);
		}
		//this.setFlippedX(true);
		//Animación 
		var animMoveUp = cc.Animation.create(animFramesUp, 0.1);
		up = cc.RepeatForever.create(cc.Animate.create(animMoveUp));

		//Chrono Up
		//Anim Move Up
		var animFramesUpChrono = [];
		var str = "";
		for(var i = 1; i < 7; i++){
			str = "chrono-up-" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFramesUpChrono.push(frame);
		}
		//this.setFlippedX(true);
		//Animación 
		var animMoveUpChrono = cc.Animation.create(animFramesUpChrono, 0.1);
		sup = cc.RepeatForever.create(cc.Animate.create(animMoveUpChrono));

		//Anim Move Down
		var animFramesDown = [];
		var str = "";
		for(var i = 1; i < 4; i+=2){
			str = "mario-walk-" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFramesDown.push(frame);
		}
		//this.setFlippedX(true);
		//Animación 
		var animMoveDown = cc.Animation.create(animFramesDown, 0.1);
		down = cc.RepeatForever.create(cc.Animate.create(animMoveDown));

		//Anim Move Down Chrono
		var animFramesDownChrono = [];
		var str = "";
		for(var i = 1; i < 7; i++){
			str = "chrono-down-" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFramesDownChrono.push(frame);
		}
		//this.setFlippedX(true);
		//Animación 
		var animMoveDownChrono = cc.Animation.create(animFramesDownChrono, 0.1);
		sdown = cc.RepeatForever.create(cc.Animate.create(animMoveDownChrono));

		//Anim Chrono Stand
		var animFramesStand = [];
		var str = "";
		for(var i = 1; i < 4; i++){
			str = "chrono-stand-" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFramesStand.push(frame);
		}
		for(var i = 3; i > 0; i--){
			str = "chrono-stand-" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFramesStand.push(frame);
		}
		//this.setFlippedX(true);
		//Animación 
		var animMoveStand = cc.Animation.create(animFramesStand, 0.1);
		stand = cc.Animate.create(animMoveStand);

		//Anim Chrono Stand Left
		var animFramesStandLeft = [];
		var str = "";
		for(var i = 1; i < 4; i++){
			str = "chrono-stand-left-" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFramesStandLeft.push(frame);
		}
		for(var i = 3; i > 0; i--){
			str = "chrono-stand-left-" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFramesStandLeft.push(frame);
		}
		//this.setFlippedX(true);
		//Animación 
		var animMoveStandLeft = cc.Animation.create(animFramesStandLeft, 0.1);
		stand_left = cc.Animate.create(animMoveStandLeft);

		//Animation star crate
		var animFramesCrate = [];
		var str = "";
		for(var i = 1; i < 5; i++){
			str = "star_crate_" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFramesCrate.push(frame);
		}
		//this.setFlippedX(true);
		//Animación 
		var animCrate = cc.Animation.create(animFramesCrate, 0.2);
		animateCrate = cc.RepeatForever.create(cc.Animate.create(animCrate));
		animateCrate2 = cc.RepeatForever.create(cc.Animate.create(animCrate));

		//Animation win
		var animFramesWin = [];
		var str = "";
		for(var i = 1; i < 5; i++){
			str = "mario-win-" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFramesWin.push(frame);
		}
		//this.setFlippedX(true);
		//Animación 
		var animWinMario = cc.Animation.create(animFramesWin, 0.1);
		animWin = cc.Animate.create(animWinMario);

		//Animation win Chrono
		var animFramesWinXChrono = [];
		var str = "";
		for(var i = 1; i < 3; i++){
			str = "chrono-win-" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFramesWinXChrono.push(frame);
		}
		//this.setFlippedX(true);
		//Animación 
		var animWinChrono = cc.Animation.create(animFramesWinXChrono, 0.1);
		animWinTwo = cc.Animate.create(animWinChrono);

		//Animation coins
		var animFramesCoins = [];
		var str = "";
		for(var i = 1; i < 9; i++){
			str = "coin-" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFramesCoins.push(frame);
		}
		//this.setFlippedX(true);
		//Animación 
		var animCoins = cc.Animation.create(animFramesCoins, 0.1);
		
		for(var j = 0; j < 10; j++){
			animateCoinsArray.push(cc.RepeatForever.create(cc.Animate.create(animCoins)));
		}


		//Animation coins taked
		var animFramesCoinsTaked = [];
		var str = "";
		for(var i = 1; i < 15; i++){
			str = "coins-taked-" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFramesCoinsTaked.push(frame);
		}
		//this.setFlippedX(true);
		//Animación 
		var animCoinsTaked = cc.Animation.create(animFramesCoinsTaked, 0.1);
		takedCoin = cc.Animate.create(animCoinsTaked);

		//Animation Rock Down
		var animFramesRock = [];
		var str = "";
		for(var i = 3; i > 0; i--){
			str = "rock-down-" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFramesRock.push(frame);
		}
		//this.setFlippedX(true);
		//Animación 
		var animRock = cc.Animation.create(animFramesRock, 0.2);
		animateRockDown = cc.Animate.create(animRock);

		//Animation Mario Over
		var animFramesMarioOver = [];
		var str = "";
		for(var i = 1; i < 3; i++){
			str = "mario-over-" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFramesMarioOver.push(frame);
		}
		//this.setFlippedX(true);
		//Animación 
		var animMarioOv = cc.Animation.create(animFramesMarioOver, 0.2);
		animMarioOver = cc.Animate.create(animMarioOv);

		//Animation Mario Over Up
		var animFramesMarioOverUp = [];
		var str = "";
		for(var i = 1; i < 3; i++){
			str = "mario-over-up-" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFramesMarioOverUp.push(frame);
		}
		//this.setFlippedX(true);
		//Animación 
		var animMOvup = cc.Animation.create(animFramesMarioOverUp, 0.2);
		animMOverUp = cc.Animate.create(animMOvup);

		//Animation Chrono Over 
		var animFramesChronoOver = [];
		var str = "";
		for(var i = 1; i < 4; i++){
			str = "chrono-over-" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFramesChronoOver.push(frame);
		}
		//this.setFlippedX(true);
		//Animación 
		var animChronoOv = cc.Animation.create(animFramesChronoOver, 0.2);
		animChronoOver = cc.Animate.create(animChronoOv);

 		// add bg
 		/*this.bgSprite = new cc.Sprite(g_ressources[0]);
 		this.bgSprite.attr({
 			x: size.width / 2,
 			y: size.height / 2,
 			//scale: 0.5,
 			rotation: 180
 		});
 		this.addChild(this.bgSprite, 0);

 		cc.spriteFrameCache.addSpriteFrames(g_ressources[4]);

 		
 		//this.addSushi();
 		this.schedule(this.update,1,16*1024,1);

 		this.scoreLabel = new cc.LabelTTF("score:0", "Arial", 20);
		this.scoreLabel.attr({
			x:size.width / 2 + 100,
			y:size.height - 20
		});
		this.addChild(this.scoreLabel, 5);*/

		// timeout 60
		/*this.timeoutLabel = cc.LabelTTF.create("" + this.timeout, "Arial", 30);
		this.timeoutLabel.x = 20;
		this.timeoutLabel.y = size.height - 20;
		this.addChild(this.timeoutLabel, 5);*/
		
		//this.schedule(this.timer,1,this.timeout,1);
		
		//Score Label
		this.scoreLabel = new cc.LabelTTF("Score: " + this.score, "Arial", 20);
		this.scoreLabel.attr({
			x:size.width / 2 + 150,
			y:20
		});
		this.addChild(this.scoreLabel, 5);

		//Moves Label
		this.movesLabel = new cc.LabelTTF("Moves: " + this.moves, "Arial", 20);
		this.movesLabel.attr({
			x:size.width / 2 + 300,
			y:20
		});
		this.addChild(this.movesLabel, 5);
		
		//this.addChild(levelSprite);

		//Inicializar Nivel
		var count = 0;
		var counterCoins = 0;
		for(i=0;i<12;i++){
			cratesArray[i]=[];
			coinsArray[i]=[];
				for(j=0;j<15;j++){
					coinsArray[i][j]=null;
					switch(level[i][j]){
						case 1:
							var wall = new cc.Sprite.create(g_ressources[7]);
							//wall.setPosition(165+25*j,186-25*i);
							wall.setScale(1.5);
							wall.setPosition(50+50*j,600-50*i);
							this.addChild(wall);
							cratesArray[i][j]=null;
							break;
						case 2:
							var placeCrate = new cc.Sprite.create(g_ressources[16]);
							//placeCrate.setPosition(165+25*j,186-25*i);
							placeCrate.setPosition(50+50*j,600-50*i);
							placeCrate.setScale(1.5);
							cratesArray[i][j]=placeCrate;
							this.addChild(placeCrate,0);
							maxCrates++;
							break;
						case 4:
						case 6:
							counterPlayers++;
							switch(counterPlayers){
								case 1: 
									playerSprite = new cc.Sprite.create(cc.spriteFrameCache.getSpriteFrame('mario-walk-2.png'));
									//playerSprite.setPosition(165+25*j,185-25*i);
									playerSprite.setPosition(50+50*j,600-50*i);
									playerSprite.setScale(1.5);
									playerSprite.setCascadeOpacityEnabled(true);
									playerSprite.setOpacity(255);
									this.addChild(playerSprite,1);
									playerPosition = {x:j,y:i};
									cratesArray[i][j]=null;
									var fadeInM = cc.FadeIn.create(1.0);
									playerSprite.runAction(fadeInM);
									break;
								case 2: 
									playerSpriteTwo = new cc.Sprite.create(cc.spriteFrameCache.getSpriteFrame('chrono-stand-1.png'));
									//playerSprite.setPosition(165+25*j,185-25*i);
									playerSpriteTwo.setPosition(50+50*j,600-50*i);
									playerSpriteTwo.setScale(1.5);
									playerSpriteTwo.setCascadeOpacityEnabled(true);
									playerSpriteTwo.setOpacity(255);
									playerSpriteTwo.runAction(stand);
									this.addChild(playerSpriteTwo,1);
									var fadeInM2 = cc.FadeIn.create(0.9);
									playerSpriteTwo.runAction(fadeInM2);
									playerTwoPosition = {x:j,y:i};
									cratesArray[i][j]=null;
									break;
							}
							break;
						case 3:
						case 5:
							var crateSprite = cc.Sprite.create();
							//crateSprite.setPosition(165+25*j,185-25*i);
							crateSprite.setPosition(50+50*j,600-50*i);
							crateSprite.setScale(1.5);
							switch(count){
								case 0:
									crateSprite.runAction(animateCrate);
									break;
								case 1:
									crateSprite.runAction(animateCrate2);
									break;
								default:
									crateSprite.setTexture(g_ressources[17]);
								break;
							}
							count++;
							this.addChild(crateSprite,1);
							cratesArray[i][j]=crateSprite;
							break;
						case 8:
							var coinSprite = new cc.Sprite.create();
							coinSprite.setPosition(50+50*j,600-50*i);
							coinSprite.setScale(1.5);
							coinSprite.runAction(animateCoinsArray[counterCoins]);
							this.addChild(coinSprite,1);
							coinsArray[i][j]=coinSprite;
							counterCoins++;
							break;
						case 10:
							var rockSprite = cc.Sprite.create(cc.spriteFrameCache.getSpriteFrame('rock-down-1.png'));
							rockSprite.setPosition(50+50*j,600-50*i);
							rockSprite.setScale(1.5);
							var actionMove = cc.MoveTo.create(0.5, cc.p(rockSprite.getPosition().x, rockSprite.getPosition().y + 50));
							var actionMoveDown = cc.MoveTo.create(0.5, cc.p(rockSprite.getPosition().x, rockSprite.getPosition().y ));
							//Calback
							var funcT=new cc.CallFunc(function(){
								//console.log("Termina Anim Rock");
								//playerSprite.stopAction(actionMove);
								//playerSprite.stopAllActions();
								//playerSprite.setTexture(g_ressources[11]);
								//console.log('Mario',playerSprite.getPosition());
								//console.log('Rock',rockSprite.getPosition());
								if(playerSprite !== null && playerSprite.getPosition().x.toFixed(2) === rockSprite.getPosition().x.toFixed(2)
									&& playerSprite.getPosition().y.toFixed(2) === rockSprite.getPosition().y.toFixed(2)){
									//Fue aplastado
									marioOver = 1;
									console.log("Aplastado!");
									playerSprite.stopAllActions();
									var fadeOutMove = cc.FadeOut.create(1.0);
									var funcOverMario = new cc.CallFunc(function(){
										console.log("Se quita Mario del tablero ");
										//Cambiamos a Piedra de nuevo
										//Not over crates drop
										//Regresamos el estado que tenia rock Down
										level[playerPosition.y][playerPosition.x] = 10;
										playerSprite.setPosition(0,0);
										layer.removeChild(playerSprite);
										playerSprite = null;
										marioOver = 2;
									},playerSprite);
									switch(playerDirection){
										case 8:
											playerSprite.runAction(new cc.sequence(animMOverUp,fadeOutMove,funcOverMario));
											break;
										default:
											playerSprite.runAction(new cc.sequence(animMarioOver,fadeOutMove,funcOverMario));
											break;
									}
								}

								if(playerSpriteTwo !== null && playerSpriteTwo.getPosition().x.toFixed(2) === rockSprite.getPosition().x.toFixed(2)
									&& playerSpriteTwo.getPosition().y.toFixed(2) === rockSprite.getPosition().y.toFixed(2)){
									//Fue aplastado
									chronoOver = 1;
									console.log("Aplastado Chrono!");
									playerSpriteTwo.stopAllActions();
									var fadeChrono = cc.FadeOut.create(0.9);
									var funcChr = new cc.CallFunc(function(){
										console.log("Se quita Chrono del tablero ");
										//Regresamos el estado que tenia rock Down
										level[playerTwoPosition.y][playerTwoPosition.x] = 10;
										playerSpriteTwo.setPosition(0,0);
										layer.removeChild(playerSpriteTwo);
										playerSpriteTwo = null;
										chronoOver = 2;
									},playerSpriteTwo);
					
									playerSpriteTwo.runAction(new cc.sequence(animChronoOver,fadeChrono,funcChr));
									
								}
							},rockSprite);

					    	var anim = new cc.RepeatForever(new cc.sequence(actionMove,animateRockDown,actionMoveDown,funcT));
					    	//rockSprite.runAction(actionMove);
					    	//rockSprite.runAction(cc.sequence(animateRockDown,actionMoveDown));
					    	rockSprite.runAction(anim);
							this.addChild(rockSprite,1);
							cratesArray[i][j]=rockSprite;
							break;
						default:
							cratesArray[i][j]=null;
					}
				}
		}

		this.schedule(this.update,1,16*1024,1);

 		return true;
 	},
 	addSushi : function() {
		
		//var sushi = new cc.Sprite(g_ressources[3]);
		var sushi = new SushiSprite(g_ressources[3]);

		var size = cc.winSize;

		var x = sushi.width/2+size.width/2*cc.random0To1();
		sushi.attr({
			x: x,
			y:size.height - 30
		});
		
		this.addChild(sushi,5);

		var dorpAction = cc.MoveTo.create(4, cc.p(sushi.x,-30));
		sushi.runAction(dorpAction);

		this.SushiSprites.push(sushi);

	},
	update : function() {
		//this.addSushi();
		//this.removeSushi();
		//console.log('update!');
		//Not over crates drop
		for(i=0;i<11;i++){
			for(j=0;j<15;j++){
				switch(level[i][j]){
					case -4:
						level[i][j] = 0;
						//placeCrate.setPosition(165+25*j,186-25*i);
						break;
				}
			}
		}
		//console.log('level',level);
		if((chronoOver === 2 && marioOver === 2) ||
			this.moves === 0){
			this.unschedule(this.update);
			var transition= new cc.TransitionFade(1, new GameOverScene(),cc.color(255,255,255,255));
			cc.director.runScene(transition);
		}
	},
	moveNextLevel: function(){
		console.log("Entra en move next");
		this.unschedule(this.update);
		var transitionLevel = new cc.TransitionFade(1, new GameOverScene(),cc.color(255,255,255,255));
		cc.director.runScene(transitionLevel);
	},
	removeSushi : function() {
		//remove sushi at teh bottom of the screen 
		for (var i = 0; i < this.SushiSprites.length; i++) {
			cc.log("removeSushi.........");
			console.log(this.SushiSprites[i].y);
			if(0 > this.SushiSprites[i].y) {
				cc.log("==============remove:"+i);
				this.SushiSprites[i].removeFromParent();
				//this.removeChild(this.SushiSprites[i],true);
				this.SushiSprites[i] = undefined;
				this.SushiSprites.splice(i,1);
				i = i-1;
			}
		}
	},
	addScore:function(points){
		this.score += points;
		this.scoreLabel.setString("Score:" + this.score);
	},
	SubMoves:function(){
		console.log("Se llama a sub moves");
		this.moves -= 1;
		this.movesLabel.setString("Moves: " + this.moves);
	},
	timer : function() {
		if (this.timeout == 0) {
			//cc.log('游戏结束');
			var gameOver = new cc.LayerColor(cc.color(225,225,225,100));
			var size = cc.winSize;
			var titleLabel = new cc.LabelTTF("Game Over", "Arial", 38);
			titleLabel.attr({
				x:size.width / 2 ,
				y:size.height / 2
			});
			gameOver.addChild(titleLabel, 5);
			var TryAgainItem = new cc.MenuItemFont(
					"Try Again",
					function () {
						cc.log("Menu is clicked!");
						var transition= cc.TransitionFade(1, new PlayScene(),cc.color(255,255,255,255));
						cc.director.runScene(transition);
					}, this);
			TryAgainItem.attr({
				x: size.width/2,
				y: size.height / 2 - 60,
				anchorX: 0.5,
				anchorY: 0.5
			});

			var menu = new cc.Menu(TryAgainItem);
			menu.x = 0;
			menu.y = 0;
			gameOver.addChild(menu, 1);
			this.getParent().addChild(gameOver);
			
			this.unschedule(this.update);
			this.unschedule(this.timer);
			return;
		}

		this.timeout -=1;
		this.timeoutLabel.setString("" + this.timeout);

	}
 });

var listener = cc.EventListener.create({
	event: cc.EventListener.TOUCH_ONE_BY_ONE,
	swallowTouches: true,
	onTouchBegan:function (touch,event) {
		startTouch = touch.getLocation();
		return true;
	},
	onTouchEnded:function(touch, event){
		endTouch = touch.getLocation();
		swipeDirection();
	}
});

/*function swipeDirection(){
	var distX = startTouch.x - endTouch.x;
	var distY = startTouch.y - endTouch.y;
	if(Math.abs(distX)+Math.abs(distY)>swipeTolerance){
		if(Math.abs(distX)>Math.abs(distY)){
			if(distX>0){
				playerSprite.setPosition(playerSprite.getPosition().
				x-25,playerSprite.getPosition().y);
				//move(-1,0);
			}else{
				playerSprite.setPosition(playerSprite.getPosition().
				x+25,playerSprite.getPosition().y);
				//move(1,0);
			}
		}else{
			if(distY>0){
				playerSprite.setPosition(playerSprite.getPosition().
				x,playerSprite.getPosition().y-25);
				//move(0,1);
			}else{
				playerSprite.setPosition(playerSprite.getPosition().
				x,playerSprite.getPosition().y+25);
				//move(0,-1);
			}
		}
	}
}*/
function swipeDirection(){
	var distX = startTouch.x - endTouch.x;
	var distY = startTouch.y - endTouch.y;
	if(Math.abs(distX)+Math.abs(distY)>swipeTolerance){
		if(Math.abs(distX)>Math.abs(distY)){
			if(distX>0){
				//Move left
				playerDirection = 4;
				move(-1,0);
				//Quitamos un movimiento
				layer.SubMoves();
			}else{
				//Move right
				playerDirection = 6;
				move(1,0);
				//Quitamos un movimiento
				layer.SubMoves();
			}
		}else{
			if(distY>0){
				//Move down
				playerDirection = 2;
				move(0,1);
				//Quitamos un movimiento
				layer.SubMoves();
			}else{
				//Move up
				playerDirection = 8;
				move(0,-1);
				//Quitamos un movimiento
				layer.SubMoves();
			}
		}
	}
}

function move(deltaX,deltaY){
	if(marioOver === 0){
		console.log('Level Data :: '+level[playerPosition.y+deltaY][playerPosition.x+deltaX]);
		switch(level[playerPosition.y+deltaY][playerPosition.x+deltaX]){
			case 0:
			case 2:
				level[playerPosition.y][playerPosition.x]-=4;
				playerPosition.x+=deltaX;
				playerPosition.y+=deltaY;
				level[playerPosition.y][playerPosition.x]+=4;
				switch(playerDirection){
					//Down 
				case 2:
					playerSprite.setFlippedX(false);
					// Move the sprite
				    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerPosition.x, 
				    	600-50*playerPosition.y));
				    var func=new cc.CallFunc(function(){
						console.log("Termina Movimiento abajo");
						//playerSprite.stopAction(actionMove);
						playerSprite.stopAction(down);
						playerSprite.setTexture(g_ressources[11]);
						//Verificamos si ha ganado
					    if(maxCrates === counterCrates){
					    	console.log("Win");
					    	playerSprite.stopAllActions();
					    	var funcT=new cc.CallFunc(function(){
								console.log("Termina Win Anim");
								//playerSprite.stopAction(actionMove);
								//playerSprite.stopAllActions();
								//playerSprite.setTexture(g_ressources[11]);
								layer.moveNextLevel();
							},playerSprite);
					    	playerSprite.runAction(cc.sequence(animWin,funcT));
					    }	
					    console.log('level',level);
						console.log('crates',cratesArray);
						console.log('coins',coinsArray);
						console.log(level[playerPosition.y][playerPosition.x]);
					    //Player over crate drop
					    if(level[playerPosition.y][playerPosition.x] === 6){
					    	console.log("Crates Changed");
							var placeCrateSuccess = cratesArray[playerPosition.y][playerPosition.x];
							console.log('Crates',placeCrateSuccess);
					    	placeCrateSuccess.setTexture(g_ressources[17]);
					    }else{
						    //Not over crates drop
							for(i=0;i<11;i++){
								for(j=0;j<15;j++){
									switch(level[i][j]){
										case 2:
											var placeCrate = cratesArray[i][j];
											placeCrate.setTexture(g_ressources[16]);
											//placeCrate.setPosition(165+25*j,186-25*i);
											break;
									}
								}
							}
					    }
					},playerSprite);
					playerSprite.runAction(down);
					playerSprite.runAction(cc.sequence(actionMove,func)); 
					break;
				//Left
				case 4:
					// Move the sprite
				    playerSprite.setFlippedX(false);
				    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerPosition.x, 
				    	600-50*playerPosition.y));
				    var func=new cc.CallFunc(function(){
						console.log("Termina Movimiento izquierda");
						//playerSprite.stopAction(actionMove);
						playerSprite.stopAction(left);
						playerSprite.setTexture(g_ressources[12]);
						//Verificamos si ha ganado
					    if(maxCrates === counterCrates){
					    	console.log("Win");
					    	var funcT=new cc.CallFunc(function(){
								console.log("Termina Win Anim");
								//playerSprite.stopAction(actionMove);
								//playerSprite.stopAllActions();
								//playerSprite.setTexture(g_ressources[11]);
								layer.moveNextLevel();
							},playerSprite);
					    	playerSprite.runAction(cc.sequence(animWin,funcT));
					    }

					    console.log('level',level);
						console.log('crates',cratesArray);
						console.log('coins',coinsArray);
						console.log(level[playerPosition.y][playerPosition.x]);
					    //Player over crate drop
					    if(level[playerPosition.y][playerPosition.x] === 6){
					    	console.log("Crates Changed");
							var placeCrateSuccess = cratesArray[playerPosition.y][playerPosition.x];
							console.log('Crates',placeCrateSuccess);
					    	placeCrateSuccess.setTexture(g_ressources[17]);
					    }else{
						    //Not over crates drop
							for(i=0;i<11;i++){
								for(j=0;j<15;j++){
									switch(level[i][j]){
										case 2:
											var placeCrate = cratesArray[i][j];
											placeCrate.setTexture(g_ressources[16]);
											//placeCrate.setPosition(165+25*j,186-25*i);
											break;
									}
								}
							}
					    }
					},playerSprite);
					playerSprite.runAction(left);
					playerSprite.runAction(cc.sequence(actionMove,func)); 
					break;
				//Right
				case 6:
					playerSprite.setFlippedX(true);
					// Move the sprite
				    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerPosition.x, 
				    	600-50*playerPosition.y));
				    var func=new cc.CallFunc(function(){
						console.log("Termina Movimiento derecha");
						//playerSprite.stopAction(actionMove);
						playerSprite.stopAction(left);
						playerSprite.setTexture(g_ressources[12]);
						//Verificamos si ha ganado
					    if(maxCrates === counterCrates){
					    	console.log("Win");
					    	var funcT=new cc.CallFunc(function(){
								console.log("Termina Win Anim");
								//playerSprite.stopAction(actionMove);
								//playerSprite.stopAction(animWin);
								//playerSprite.setTexture(g_ressources[11]);
								layer.moveNextLevel();
							},playerSprite);
					    	playerSprite.runAction(cc.sequence(animWin,funcT));
					    }

					    console.log('level',level);
						console.log('crates',cratesArray);
						console.log('coins',coinsArray);
						console.log(level[playerPosition.y][playerPosition.x]);
					    //Player over crate drop
					    if(level[playerPosition.y][playerPosition.x] === 6){
					    	console.log("Crates Changed");
							var placeCrateSuccess = cratesArray[playerPosition.y][playerPosition.x];
							console.log('Crates',placeCrateSuccess);
					    	placeCrateSuccess.setTexture(g_ressources[17]);
					    }else{
						    //Not over crates drop
							for(i=0;i<11;i++){
								for(j=0;j<15;j++){
									switch(level[i][j]){
										case 2:
											var placeCrate = cratesArray[i][j];
											placeCrate.setTexture(g_ressources[16]);
											//placeCrate.setPosition(165+25*j,186-25*i);
											break;
									}
								}
							}
					    }
					},playerSprite);
					playerSprite.runAction(left);
					playerSprite.runAction(cc.sequence(actionMove,func)); 
					break;
				//Up
				case 8:
					playerSprite.setFlippedX(false);
					// Move the sprite
				    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerPosition.x, 
				    	600-50*playerPosition.y));
				    var func=new cc.CallFunc(function(){
						console.log("Termina Movimiento arriba");
						//playerSprite.stopAction(actionMove);
						playerSprite.stopAction(up);
						playerSprite.setTexture(g_ressources[13]);
						//Verificamos si ha ganado
					    if(maxCrates === counterCrates){
					    	console.log("Win");
					    	var funcT=new cc.CallFunc(function(){
								console.log("Termina Win Anim");
								//playerSprite.stopAction(actionMove);
								//playerSprite.stopAction(animWin);
								//playerSprite.setTexture(g_ressources[11]);
								layer.moveNextLevel();
							},playerSprite);
					    	playerSprite.runAction(cc.sequence(animWin,funcT));
					    }

					    console.log('level',level);
						console.log('crates',cratesArray);
						console.log('coins',coinsArray);
						console.log(level[playerPosition.y][playerPosition.x]);
					    //Player over crate drop
					    if(level[playerPosition.y][playerPosition.x] === 6){
					    	console.log("Crates Changed");
							var placeCrateSuccess = cratesArray[playerPosition.y][playerPosition.x];
							console.log('Crates',placeCrateSuccess);
					    	placeCrateSuccess.setTexture(g_ressources[17]);
					    }else{
						    //Not over crates drop
							for(i=0;i<11;i++){
								for(j=0;j<15;j++){
									switch(level[i][j]){
										case 2:
											var placeCrate = cratesArray[i][j];
											placeCrate.setTexture(g_ressources[16]);
											//placeCrate.setPosition(165+25*j,186-25*i);
											break;
									}
								}
							}
					    }
					},playerSprite);
					playerSprite.runAction(up);
					playerSprite.runAction(cc.sequence(actionMove,func)); 
					break;
				}
				break;
			case 3:
			case 5:
				if(level[playerPosition.y+deltaY*2][playerPosition.x+deltaX*2]==0
				|| level[playerPosition.y+deltaY*2][playerPosition.x+deltaX*2]==2
				|| level[playerPosition.y+deltaY*2][playerPosition.x+deltaX*2]==8){
					level[playerPosition.y][playerPosition.x]-=4;
					playerPosition.x+=deltaX;
					playerPosition.y+=deltaY;
					level[playerPosition.y][playerPosition.x]+=1;

					switch(playerDirection){
						//Down 
						case 2:
							playerSprite.setFlippedX(false);
							// Move the sprite
						    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerPosition.x, 
						    	600-50*playerPosition.y));
						    var func=new cc.CallFunc(function(){
								console.log("Termina Movimiento abajo");
								//playerSprite.stopAction(actionMove);
								playerSprite.stopAction(down);
								playerSprite.setTexture(g_ressources[11]);
								//Verificamos si ha ganado
							    if(maxCrates === counterCrates){
							    	console.log("Win Down");
							    	playerSprite.stopAllActions();
							    	var funcT=new cc.CallFunc(function(){
										console.log("Termina Win Anim");
										//playerSprite.stopAction(actionMove);
										layer.moveNextLevel();
									},playerSprite);
							    	playerSprite.runAction(cc.sequence(animWin,funcT));
							    	//playerSprite.runAction(animWin);
							    }

							    //Player over crate drop
							    if(level[playerPosition.y][playerPosition.x] === 6){
							    	console.log("Crates Changed");
									var placeCrateSuccess = cratesArray[playerPosition.y][playerPosition.x];
									console.log('Crates',placeCrateSuccess);
							    	placeCrateSuccess.setTexture(g_ressources[17]);
							    }else{
								    //Not over crates drop
									for(i=0;i<11;i++){
										for(j=0;j<15;j++){
											switch(level[i][j]){
												case 2:
													var placeCrate = cratesArray[i][j];
													placeCrate.setTexture(g_ressources[16]);
													//placeCrate.setPosition(165+25*j,186-25*i);
													break;
											}
										}
									}
							    }
							},playerSprite);
							playerSprite.runAction(down);
							playerSprite.runAction(cc.sequence(actionMove,func)); 
							break;
						//Left
						case 4:
							// Move the sprite
						    playerSprite.setFlippedX(false);
						    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerPosition.x, 
						    	600-50*playerPosition.y));
						    var func=new cc.CallFunc(function(){
								console.log("Termina Movimiento izquierda");
								//playerSprite.stopAction(actionMove);
								playerSprite.stopAction(left);
								playerSprite.setTexture(g_ressources[12]);
								//Verificamos si ha ganado
							    if(maxCrates === counterCrates){
							    	console.log("Win");
							    	var funcT=new cc.CallFunc(function(){
										console.log("Termina Win Anim");
										//playerSprite.stopAction(actionMove);
										//playerSprite.stopAction(animWin);
										//playerSprite.setTexture(g_ressources[11]);
										layer.moveNextLevel();
									},playerSprite);
							    	playerSprite.runAction(cc.sequence(animWin,funcT));
							    }
							    //Player over crate drop
							    if(level[playerPosition.y][playerPosition.x] === 6){
							    	console.log("Crates Changed");
									var placeCrateSuccess = cratesArray[playerPosition.y][playerPosition.x];
									console.log('Crates',placeCrateSuccess);
							    	placeCrateSuccess.setTexture(g_ressources[17]);
							    }else{
								    //Not over crates drop
									for(i=0;i<11;i++){
										for(j=0;j<15;j++){
											switch(level[i][j]){
												case 2:
													var placeCrate = cratesArray[i][j];
													placeCrate.setTexture(g_ressources[16]);
													//placeCrate.setPosition(165+25*j,186-25*i);
													break;
											}
										}
									}
							    }
							},playerSprite);
							playerSprite.runAction(left);
							playerSprite.runAction(cc.sequence(actionMove,func)); 
							break;
						//Right
						case 6:
							playerSprite.setFlippedX(true);
							// Move the sprite
						    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerPosition.x, 
						    	600-50*playerPosition.y));
						    var func=new cc.CallFunc(function(){
								console.log("Termina Movimiento derecha");
								//playerSprite.stopAction(actionMove);
								playerSprite.stopAction(left);
								playerSprite.setTexture(g_ressources[12]);
								//Verificamos si ha ganado
							    if(maxCrates === counterCrates){
							    	console.log("Win");
							    	var funcT=new cc.CallFunc(function(){
										console.log("Termina Win Anim");
										//playerSprite.stopAction(actionMove);
										//playerSprite.stopAction(animWin);
										//playerSprite.setTexture(g_ressources[11]);
										layer.moveNextLevel();
									},playerSprite);
							    	playerSprite.runAction(cc.sequence(animWin,funcT));
							    }
							    //Player over crate drop
							    if(level[playerPosition.y][playerPosition.x] === 6){
							    	console.log("Crates Changed");
									var placeCrateSuccess = cratesArray[playerPosition.y][playerPosition.x];
									console.log('Crates',placeCrateSuccess);
							    	placeCrateSuccess.setTexture(g_ressources[17]);
							    }else{
								    //Not over crates drop
									for(i=0;i<11;i++){
										for(j=0;j<15;j++){
											switch(level[i][j]){
												case 2:
													var placeCrate = cratesArray[i][j];
													placeCrate.setTexture(g_ressources[16]);
													//placeCrate.setPosition(165+25*j,186-25*i);
													break;
											}
										}
									}
							    }
							},playerSprite);
							playerSprite.runAction(left);
							playerSprite.runAction(cc.sequence(actionMove,func)); 
							break;
						//Up
						case 8:
							playerSprite.setFlippedX(false);
							// Move the sprite
						    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerPosition.x, 
						    	600-50*playerPosition.y));
						    var func=new cc.CallFunc(function(){
								console.log("Termina Movimiento arriba");
								//playerSprite.stopAction(actionMove);
								playerSprite.stopAction(up);
								playerSprite.setTexture(g_ressources[13]);
								//Verificamos si ha ganado
							    if(maxCrates === counterCrates){
							    	console.log("Win");
							    	var funcT=new cc.CallFunc(function(){
										console.log("Termina Win Anim");
										//playerSprite.stopAction(actionMove);
										//playerSprite.stopAction(animWin);
										//playerSprite.setTexture(g_ressources[11]);
										layer.moveNextLevel();
									},playerSprite);
							    	playerSprite.runAction(cc.sequence(animWin,funcT));
							    }
							    //Player over crate drop
							    if(level[playerPosition.y][playerPosition.x] === 6){
							    	console.log("Crates Changed");
									var placeCrateSuccess = cratesArray[playerPosition.y][playerPosition.x];
									console.log('Crates',placeCrateSuccess);
							    	placeCrateSuccess.setTexture(g_ressources[17]);
							    }else{
								    //Not over crates drop
									for(i=0;i<11;i++){
										for(j=0;j<15;j++){
											switch(level[i][j]){
												case 2:
													var placeCrate = cratesArray[i][j];
													placeCrate.setTexture(g_ressources[16]);
													//placeCrate.setPosition(165+25*j,186-25*i);
													break;
											}
										}
									}
							    }
							},playerSprite);
							playerSprite.runAction(up);
							playerSprite.runAction(cc.sequence(actionMove,func)); 
							break;
					}

					//Termina Switch
					level[playerPosition.y+deltaY][playerPosition.x+deltaX]+=3;
					var movingCrate = cratesArray[playerPosition.y]
					[playerPosition.x];
					/*movingCrate.setPosition(movingCrate.getPosition().
					x+25*deltaX,movingCrate.getPosition().y-25*deltaY);*/
					
				    var actionMoveCrate = cc.MoveTo.create(0.5, cc.p(movingCrate.getPosition().x+50*deltaX
				    	,movingCrate.getPosition().y-50*deltaY));
				    var funcCrate=new cc.CallFunc(function(){
						console.log("Termina Movimiento");
						playerSprite.stopAction(actionMoveCrate);
					},movingCrate);
				    movingCrate.runAction(cc.sequence(actionMoveCrate,funcCrate));
				    //Crate over crate Drop
				    if(level[playerPosition.y+deltaY][playerPosition.x+deltaX] === 5 ){
				    	//Ha colocado en el lugar correcto el objeto
				    	var placeCrateSuccess = cratesArray[playerPosition.y+deltaY][playerPosition.x+deltaX];
				    	placeCrateSuccess.setTexture(g_ressources[17]);
				    	counterCrates++;
				    	layer.addScore(250*counterCrates);
				    }

				    /*else if(level[movingCrate.getPosition().x+50*deltaX][movingCrate.getPosition().y-50*deltaY] === 11 
				    	|| level[movingCrate.getPosition().x+50*deltaX][movingCrate.getPosition().y-50*deltaY] === 13){
				    	console.log('crate over coin');
				    }
				    /*else{
				    	var placeCrateSuccess = level[playerPosition.y+deltaY][playerPosition.x+deltaX];
				    	placeCrateSuccess.setTexture(g_ressources[16]);
				    }*/

					/*movingCrate.setPosition(movingCrate.getPosition().
					x+50*deltaX,movingCrate.getPosition().y-50*deltaY);*/

					cratesArray[playerPosition.y+deltaY][playerPosition.
					x+deltaX]=movingCrate;
					cratesArray[playerPosition.y][playerPosition.x]=null;

				}

				console.log('level :: ',level);
				break;
			case 8:
				level[playerPosition.y][playerPosition.x]-=4;
				playerPosition.x+=deltaX;
				playerPosition.y+=deltaY;
				level[playerPosition.y][playerPosition.x]+=4;
				let value = level[playerPosition.y][playerPosition.x];
				let x_1 = playerPosition.x;
				let y_1 = playerPosition.y;
				console.log('Level Player',level[playerPosition.y][playerPosition.x]);
				switch(playerDirection){
					//Down 
					case 2:
						playerSprite.setFlippedX(false);
						// Move the sprite
					    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerPosition.x, 
					    	600-50*playerPosition.y));
					    var func=new cc.CallFunc(function(){
							console.log("Termina Movimiento abajo");
							//playerSprite.stopAction(actionMove);
							playerSprite.stopAction(down);
							playerSprite.setTexture(g_ressources[11]);
							//Verificamos si ha ganado
						    if(maxCrates === counterCrates){
						    	console.log("Win");
						    	playerSprite.stopAllActions();
						    	var funcT=new cc.CallFunc(function(){
									console.log("Termina Win Anim");
									//playerSprite.stopAction(actionMove);
									//playerSprite.stopAction(animWin);
									//playerSprite.setTexture(g_ressources[11]);
									layer.moveNextLevel();
								},playerSprite);
						    	playerSprite.runAction(cc.sequence(animWin,funcT));
						    }	
						    console.log('level',level);
							console.log('crates',cratesArray);
							console.log(level[playerPosition.y][playerPosition.x]);
						    //Player over crate drop
						    if(level[playerPosition.y][playerPosition.x] === 6){
						    	console.log("Crates Changed");
								var placeCrateSuccess = cratesArray[playerPosition.y][playerPosition.x];
								console.log('Crates',placeCrateSuccess);
						    	placeCrateSuccess.setTexture(g_ressources[17]);
						    }else{
							    //Not over crates drop
								for(i=0;i<11;i++){
									for(j=0;j<15;j++){
										switch(level[i][j]){
											case 2:
												var placeCrate = cratesArray[i][j];
												placeCrate.setTexture(g_ressources[16]);
												//placeCrate.setPosition(165+25*j,186-25*i);
												break;
										}
									}
								}
						    }
						},playerSprite);
						playerSprite.runAction(down);
						playerSprite.runAction(cc.sequence(actionMove,func)); 
						break;
					//Left
					case 4:
						// Move the sprite
					    playerSprite.setFlippedX(false);
					    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerPosition.x, 
					    	600-50*playerPosition.y));
					    var func=new cc.CallFunc(function(){
							console.log("Termina Movimiento izquierda");
							//playerSprite.stopAction(actionMove);
							playerSprite.stopAction(left);
							playerSprite.setTexture(g_ressources[12]);
							//Verificamos si ha ganado
						    if(maxCrates === counterCrates){
						    	console.log("Win");
						    	var funcT=new cc.CallFunc(function(){
									console.log("Termina Win Anim");
									//playerSprite.stopAction(actionMove);
									//playerSprite.stopAction(animWin);
									//playerSprite.setTexture(g_ressources[11]);
									layer.moveNextLevel();
								},playerSprite);
						    	playerSprite.runAction(cc.sequence(animWin,funcT));
						    }

						    console.log('level',level);
							console.log('crates',cratesArray);
							console.log(level[playerPosition.y][playerPosition.x]);
						    //Player over crate drop
						    if(level[playerPosition.y][playerPosition.x] === 6){
						    	console.log("Crates Changed");
								var placeCrateSuccess = cratesArray[playerPosition.y][playerPosition.x];
								console.log('Crates',placeCrateSuccess);
						    	placeCrateSuccess.setTexture(g_ressources[17]);
						    }else{
							    //Not over crates drop
								for(i=0;i<11;i++){
									for(j=0;j<15;j++){
										switch(level[i][j]){
											case 2:
												var placeCrate = cratesArray[i][j];
												placeCrate.setTexture(g_ressources[16]);
												//placeCrate.setPosition(165+25*j,186-25*i);
												break;
											}
									}
								}
						    }
						},playerSprite);
						playerSprite.runAction(left);
						playerSprite.runAction(cc.sequence(actionMove,func)); 
						break;
					//Right
					case 6:
						playerSprite.setFlippedX(true);
						// Move the sprite
					    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerPosition.x, 
					    	600-50*playerPosition.y));
					    var func=new cc.CallFunc(function(){
							console.log("Termina Movimiento derecha");
							//playerSprite.stopAction(actionMove);
							playerSprite.stopAction(left);
							playerSprite.setTexture(g_ressources[12]);
							//Verificamos si ha ganado
						    if(maxCrates === counterCrates){
						    	console.log("Win");
						    	var funcT=new cc.CallFunc(function(){
									console.log("Termina Win Anim");
									//playerSprite.stopAction(actionMove);
									//playerSprite.stopAction(animWin);
									//playerSprite.setTexture(g_ressources[11]);
									layer.moveNextLevel();								
								},playerSprite);
						    	playerSprite.runAction(cc.sequence(animWin,funcT));
						    }

						    console.log('level',level);
							console.log('crates',cratesArray);
							console.log(level[playerPosition.y][playerPosition.x]);
						    //Player over crate drop
						    if(level[playerPosition.y][playerPosition.x] === 6){
						    	console.log("Crates Changed");
								var placeCrateSuccess = cratesArray[playerPosition.y][playerPosition.x];
								console.log('Crates',placeCrateSuccess);
						    	placeCrateSuccess.setTexture(g_ressources[17]);
						    }else{
							    //Not over crates drop
								for(i=0;i<11;i++){
									for(j=0;j<15;j++){
										switch(level[i][j]){
											case 2:
												var placeCrate = cratesArray[i][j];
												placeCrate.setTexture(g_ressources[16]);
												//placeCrate.setPosition(165+25*j,186-25*i);
												break;
										}
									}

								}
						    }
						},playerSprite);
						playerSprite.runAction(left);
						playerSprite.runAction(cc.sequence(actionMove,func)); 
						break;
					//Up
					case 8:
						playerSprite.setFlippedX(false);
						// Move the sprite
					    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerPosition.x, 
					    	600-50*playerPosition.y));
					    var func=new cc.CallFunc(function(){
							console.log("Termina Movimiento arriba");
							//playerSprite.stopAction(actionMove);
							playerSprite.stopAction(up);
							playerSprite.setTexture(g_ressources[13]);
							//Verificamos si ha ganado
						    if(maxCrates === counterCrates){
						    	console.log("Win");
						    	var funcT=new cc.CallFunc(function(){
									console.log("Termina Win Anim");
									//playerSprite.stopAction(actionMove);
									//playerSprite.stopAction(animWin);
									//playerSprite.setTexture(g_ressources[11]);
									layer.moveNextLevel();
								},playerSprite);
						    	playerSprite.runAction(cc.sequence(animWin,funcT));
						    }

						    console.log('level',level);
							console.log('crates',cratesArray);
							console.log(level[playerPosition.y][playerPosition.x]);
						    //Player over crate drop
						    if(level[playerPosition.y][playerPosition.x] === 6){
						    	console.log("Crates Changed");
								var placeCrateSuccess = cratesArray[playerPosition.y][playerPosition.x];
								console.log('Crates',placeCrateSuccess);
						    	placeCrateSuccess.setTexture(g_ressources[17]);
						    }else{
							    //Not over crates drop
								for(i=0;i<11;i++){
									for(j=0;j<15;j++){
										switch(level[i][j]){
											case 2:
												var placeCrate = cratesArray[i][j];
												placeCrate.setTexture(g_ressources[16]);
												//placeCrate.setPosition(165+25*j,186-25*i);
												break;
										}
									}
								}
						    }
						},playerSprite);
						playerSprite.runAction(up);
						playerSprite.runAction(cc.sequence(actionMove,func)); 
						break;				
				}
				//Termina Switch
				console.log('Move',value);

				if(value === 12 ){
			    	//Quitar Sprites de crates
			    	var coin = coinsArray[y_1][x_1];
			    	coin.stopAllActions();
			    	// Move the sprite
				    var actionMove = cc.MoveTo.create(0.5, cc.p(coin.x, coin.y + 50));
				    var funcT=new cc.CallFunc(function(){
						console.log("Termina Coin Anim");
						//playerSprite.stopAction(actionMove);
						coin.stopAllActions();
				    	layer.removeChild(coin);
				    	coinsArray[y_1][x_1] = null;
				    	console.log("Mas dos");
						layer.addScore(100);
				    	level[y_1][x_1] = 0;
				    	console.log("Level Changed",level);
				    	console.log("Coin",coinsArray);
					},coin);
			    	coin.runAction(actionMove);
			    	coin.runAction(cc.sequence(takedCoin,funcT));
			    }
				break;	
			case 10:
				level[playerPosition.y][playerPosition.x]-=4;
				playerPosition.x+=deltaX;
				playerPosition.y+=deltaY;
				level[playerPosition.y][playerPosition.x]+=4;
				switch(playerDirection){
					//Down 
					case 2:
						playerSprite.setFlippedX(false);
						// Move the sprite
					    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerPosition.x, 
					    	600-50*playerPosition.y));
					    var func=new cc.CallFunc(function(){
							console.log("Termina Movimiento abajo");
							//playerSprite.stopAction(actionMove);
							playerSprite.stopAction(down);
							playerSprite.setTexture(g_ressources[11]);
							//Verificamos si ha ganado
						    if(maxCrates === counterCrates){
						    	console.log("Win");
						    	playerSprite.stopAllActions();
						    	var funcT=new cc.CallFunc(function(){
									console.log("Termina Win Anim");
									//playerSprite.stopAction(actionMove);
									//playerSprite.stopAction(animWin);
									//playerSprite.setTexture(g_ressources[11]);
									layer.moveNextLevel();
								},playerSprite);
						    	playerSprite.runAction(cc.sequence(animWin,funcT));
						    }	
						    console.log('level',level);
							console.log('crates',cratesArray);
							console.log(level[playerPosition.y][playerPosition.x]);
						    //Player over crate drop
						    if(level[playerPosition.y][playerPosition.x] === 6){
						    	console.log("Crates Changed");
								var placeCrateSuccess = cratesArray[playerPosition.y][playerPosition.x];
								console.log('Crates',placeCrateSuccess);
						    	placeCrateSuccess.setTexture(g_ressources[17]);
						    }else{
							    //Not over crates drop
								for(i=0;i<11;i++){
									for(j=0;j<15;j++){
										switch(level[i][j]){
											case 2:
												var placeCrate = cratesArray[i][j];
												placeCrate.setTexture(g_ressources[16]);
												//placeCrate.setPosition(165+25*j,186-25*i);
												break;
										}
									}
								}
						    }
						},playerSprite);
						playerSprite.runAction(down);
						playerSprite.runAction(cc.sequence(actionMove,func)); 
						break;
					//Left
					case 4:
						// Move the sprite
					    playerSprite.setFlippedX(false);
					    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerPosition.x, 
					    	600-50*playerPosition.y));
					    var func=new cc.CallFunc(function(){
							console.log("Termina Movimiento izquierda");
							//playerSprite.stopAction(actionMove);
							playerSprite.stopAction(left);
							playerSprite.setTexture(g_ressources[12]);
							//Verificamos si ha ganado
						    if(maxCrates === counterCrates){
						    	console.log("Win");
						    	var funcT=new cc.CallFunc(function(){
									console.log("Termina Win Anim");
									//playerSprite.stopAction(actionMove);
									//playerSprite.stopAction(animWin);
									//playerSprite.setTexture(g_ressources[11]);
									layer.moveNextLevel();
								},playerSprite);
						    	playerSprite.runAction(cc.sequence(animWin,funcT));
						    }

						    console.log('level',level);
							console.log('crates',cratesArray);
							console.log(level[playerPosition.y][playerPosition.x]);
						    //Player over crate drop
						    if(level[playerPosition.y][playerPosition.x] === 6){
						    	console.log("Crates Changed");
								var placeCrateSuccess = cratesArray[playerPosition.y][playerPosition.x];
								console.log('Crates',placeCrateSuccess);
						    	placeCrateSuccess.setTexture(g_ressources[17]);
						    }else{
							    //Not over crates drop
								for(i=0;i<11;i++){
									for(j=0;j<15;j++){
										switch(level[i][j]){
											case 2:
												var placeCrate = cratesArray[i][j];
												placeCrate.setTexture(g_ressources[16]);
												//placeCrate.setPosition(165+25*j,186-25*i);
												break;
										}
									}
								}
						    }
						},playerSprite);
						playerSprite.runAction(left);
						playerSprite.runAction(cc.sequence(actionMove,func)); 
						break;
					//Right
					case 6:
						playerSprite.setFlippedX(true);
						// Move the sprite
					    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerPosition.x, 
					    	600-50*playerPosition.y));
					    var func=new cc.CallFunc(function(){
							console.log("Termina Movimiento derecha");
							//playerSprite.stopAction(actionMove);
							playerSprite.stopAction(left);
							playerSprite.setTexture(g_ressources[12]);
							//Verificamos si ha ganado
						    if(maxCrates === counterCrates){
						    	console.log("Win");
						    	var funcT=new cc.CallFunc(function(){
									console.log("Termina Win Anim");
									//playerSprite.stopAction(actionMove);
									//playerSprite.stopAction(animWin);
									//playerSprite.setTexture(g_ressources[11]);
									layer.moveNextLevel();
								},playerSprite);
						    	playerSprite.runAction(cc.sequence(animWin,funcT));
						    }

						    console.log('level',level);
							console.log('crates',cratesArray);
							console.log(level[playerPosition.y][playerPosition.x]);
						    //Player over crate drop
						    if(level[playerPosition.y][playerPosition.x] === 6){
						    	console.log("Crates Changed");
								var placeCrateSuccess = cratesArray[playerPosition.y][playerPosition.x];
								console.log('Crates',placeCrateSuccess);
						    	placeCrateSuccess.setTexture(g_ressources[17]);
						    }else{
							    //Not over crates drop
								for(i=0;i<11;i++){
									for(j=0;j<15;j++){
										switch(level[i][j]){
											case 2:
												var placeCrate = cratesArray[i][j];
												placeCrate.setTexture(g_ressources[16]);
												//placeCrate.setPosition(165+25*j,186-25*i);
												break;
										}
									}
								}
						    }
						},playerSprite);
						playerSprite.runAction(left);
						playerSprite.runAction(cc.sequence(actionMove,func)); 
						break;
					//Up
					case 8:
						playerSprite.setFlippedX(false);
						// Move the sprite
					    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerPosition.x, 
					    	600-50*playerPosition.y));
					    var func=new cc.CallFunc(function(){
							console.log("Termina Movimiento arriba");
							//playerSprite.stopAction(actionMove);
							playerSprite.stopAction(up);
							playerSprite.setTexture(g_ressources[13]);
							//Verificamos si ha ganado
						    if(maxCrates === counterCrates){
						    	console.log("Win");
						    	var funcT=new cc.CallFunc(function(){
									console.log("Termina Win Anim");
									//playerSprite.stopAction(actionMove);
									//playerSprite.stopAction(animWin);
									//playerSprite.setTexture(g_ressources[11]);
									layer.moveNextLevel();
								},playerSprite);
						    	playerSprite.runAction(cc.sequence(animWin,funcT));
						    }

						    console.log('level',level);
							console.log('crates',cratesArray);
							console.log(level[playerPosition.y][playerPosition.x]);
						    //Player over crate drop
						    if(level[playerPosition.y][playerPosition.x] === 6){
						    	console.log("Crates Changed");
								var placeCrateSuccess = cratesArray[playerPosition.y][playerPosition.x];
								console.log('Crates',placeCrateSuccess);
						    	placeCrateSuccess.setTexture(g_ressources[17]);
						    }else{
							    //Not over crates drop
								for(i=0;i<11;i++){
									for(j=0;j<15;j++){
										switch(level[i][j]){
											case 2:
												var placeCrate = cratesArray[i][j];
												placeCrate.setTexture(g_ressources[16]);
												//placeCrate.setPosition(165+25*j,186-25*i);
												break;
										}
									}
								}
						    }
						},playerSprite);
						playerSprite.runAction(up);
						playerSprite.runAction(cc.sequence(actionMove,func)); 
						break;
				
				}
				break;
			case 11:
				//crate + coin
				console.log('crate plus coin :: ',level);
				if(level[playerPosition.y+deltaY*2][playerPosition.x+deltaX*2]==0
				|| level[playerPosition.y+deltaY*2][playerPosition.x+deltaX*2]==2
				|| level[playerPosition.y+deltaY*2][playerPosition.x+deltaX*2]==8){
					level[playerPosition.y][playerPosition.x]-=4;
					playerPosition.x+=deltaX;
					playerPosition.y+=deltaY;
					level[playerPosition.y][playerPosition.x]+=4;

					switch(playerDirection){
						//Down 
						case 2:
							playerSprite.setFlippedX(false);
							// Move the sprite
						    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerPosition.x, 
						    	600-50*playerPosition.y));
						    var func=new cc.CallFunc(function(){
								console.log("Termina Movimiento abajo");
								//playerSprite.stopAction(actionMove);
								playerSprite.stopAction(down);
								playerSprite.setTexture(g_ressources[11]);
								//Verificamos si ha ganado
							    if(maxCrates === counterCrates){
							    	console.log("Win Down");
							    	playerSprite.stopAllActions();
							    	var funcT=new cc.CallFunc(function(){
										console.log("Termina Win Anim");
										//playerSprite.stopAction(actionMove);
										layer.moveNextLevel();
									},playerSprite);
							    	playerSprite.runAction(cc.sequence(animWin,funcT));
							    	//playerSprite.runAction(animWin);
							    }
							    //Player over crate drop
							    if(level[playerPosition.y][playerPosition.x] === 6){
							    	console.log("Crates Changed");
									var placeCrateSuccess = cratesArray[playerPosition.y][playerPosition.x];
									console.log('Crates',placeCrateSuccess);
							    	placeCrateSuccess.setTexture(g_ressources[17]);
							    }else{
								    //Not over crates drop
									for(i=0;i<11;i++){
										for(j=0;j<15;j++){
											switch(level[i][j]){
												case 2:
													var placeCrate = cratesArray[i][j];
													placeCrate.setTexture(g_ressources[16]);
													//placeCrate.setPosition(165+25*j,186-25*i);
													break;
											}
										}
									}
							    }
							},playerSprite);
							playerSprite.runAction(down);
							playerSprite.runAction(cc.sequence(actionMove,func)); 
							break;
						//Left
						case 4:
							// Move the sprite
						    playerSprite.setFlippedX(false);
						    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerPosition.x, 
						    	600-50*playerPosition.y));
						    var func=new cc.CallFunc(function(){
								console.log("Termina Movimiento izquierda");
								//playerSprite.stopAction(actionMove);
								playerSprite.stopAction(left);
								playerSprite.setTexture(g_ressources[12]);
								//Verificamos si ha ganado
							    if(maxCrates === counterCrates){
							    	console.log("Win");
							    	var funcT=new cc.CallFunc(function(){
										console.log("Termina Win Anim");
										//playerSprite.stopAction(actionMove);
										//playerSprite.stopAction(animWin);
										//playerSprite.setTexture(g_ressources[11]);
										layer.moveNextLevel();
									},playerSprite);
							    	playerSprite.runAction(cc.sequence(animWin,funcT));
							    }
							    //Player over crate drop
							    if(level[playerPosition.y][playerPosition.x] === 6){
							    	console.log("Crates Changed");
									var placeCrateSuccess = cratesArray[playerPosition.y][playerPosition.x];
									console.log('Crates',placeCrateSuccess);
							    	placeCrateSuccess.setTexture(g_ressources[17]);
							    }else{
								    //Not over crates drop
									for(i=0;i<11;i++){
										for(j=0;j<15;j++){
											switch(level[i][j]){
												case 2:
													var placeCrate = cratesArray[i][j];
													placeCrate.setTexture(g_ressources[16]);
													//placeCrate.setPosition(165+25*j,186-25*i);
													break;
											}
										}
									}
							    }
							},playerSprite);
							playerSprite.runAction(left);
							playerSprite.runAction(cc.sequence(actionMove,func)); 
							break;
						//Right
						case 6:
							playerSprite.setFlippedX(true);
							// Move the sprite
						    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerPosition.x, 
						    	600-50*playerPosition.y));
						    var func=new cc.CallFunc(function(){
								console.log("Termina Movimiento derecha");
								//playerSprite.stopAction(actionMove);
								playerSprite.stopAction(left);
								playerSprite.setTexture(g_ressources[12]);
								//Verificamos si ha ganado
							    if(maxCrates === counterCrates){
							    	console.log("Win");
							    	var funcT=new cc.CallFunc(function(){
										console.log("Termina Win Anim");
										//playerSprite.stopAction(actionMove);
										//playerSprite.stopAction(animWin);
										//playerSprite.setTexture(g_ressources[11]);
										layer.moveNextLevel();
									},playerSprite);
							    	playerSprite.runAction(cc.sequence(animWin,funcT));
							    }
							    //Player over crate drop
							    if(level[playerPosition.y][playerPosition.x] === 6){
							    	console.log("Crates Changed");
									var placeCrateSuccess = cratesArray[playerPosition.y][playerPosition.x];
									console.log('Crates',placeCrateSuccess);
							    	placeCrateSuccess.setTexture(g_ressources[17]);
							    }else{
								    //Not over crates drop
									for(i=0;i<11;i++){
										for(j=0;j<15;j++){
											switch(level[i][j]){
												case 2:
													var placeCrate = cratesArray[i][j];
													placeCrate.setTexture(g_ressources[16]);
													//placeCrate.setPosition(165+25*j,186-25*i);
													break;
											}
										}
									}
							    }
							},playerSprite);
							playerSprite.runAction(left);
							playerSprite.runAction(cc.sequence(actionMove,func)); 
							break;
						//Up
						case 8:
							playerSprite.setFlippedX(false);
							// Move the sprite
						    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerPosition.x, 
						    	600-50*playerPosition.y));
						    var func=new cc.CallFunc(function(){
								console.log("Termina Movimiento arriba");
								//playerSprite.stopAction(actionMove);
								playerSprite.stopAction(up);
								playerSprite.setTexture(g_ressources[13]);
								//Verificamos si ha ganado
							    if(maxCrates === counterCrates){
							    	console.log("Win");
							    	var funcT=new cc.CallFunc(function(){
										console.log("Termina Win Anim");
										//playerSprite.stopAction(actionMove);
										//playerSprite.stopAction(animWin);
										//playerSprite.setTexture(g_ressources[11]);
										layer.moveNextLevel();
									},playerSprite);
							    	playerSprite.runAction(cc.sequence(animWin,funcT));
							    }
							    //Player over crate drop
							    if(level[playerPosition.y][playerPosition.x] === 6){
							    	console.log("Crates Changed");
									var placeCrateSuccess = cratesArray[playerPosition.y][playerPosition.x];
									console.log('Crates',placeCrateSuccess);
							    	placeCrateSuccess.setTexture(g_ressources[17]);
							    }else{
								    //Not over crates drop
									for(i=0;i<11;i++){
										for(j=0;j<15;j++){
											switch(level[i][j]){
												case 2:
													var placeCrate = cratesArray[i][j];
													placeCrate.setTexture(g_ressources[16]);
													//placeCrate.setPosition(165+25*j,186-25*i);
													break;
											}
										}
									}
							    }
							},playerSprite);
							playerSprite.runAction(up);
							playerSprite.runAction(cc.sequence(actionMove,func)); 
							break;
					}

					//Termina Switch
					//Si delante de crate hay un switch, incrementar 3
					level[playerPosition.y+deltaY][playerPosition.x+deltaX]+=3;
					level[playerPosition.y][playerPosition.x]-=3;

					var movingCrate = cratesArray[playerPosition.y]
					[playerPosition.x];
					/*movingCrate.setPosition(movingCrate.getPosition().
					x+25*deltaX,movingCrate.getPosition().y-25*deltaY);*/
					
				    var actionMoveCrate = cc.MoveTo.create(0.5, cc.p(movingCrate.getPosition().x+50*deltaX
				    	,movingCrate.getPosition().y-50*deltaY));
				    var funcCrate=new cc.CallFunc(function(){
						console.log("Termina Movimiento");
						playerSprite.stopAction(actionMoveCrate);
					},movingCrate);
				    movingCrate.runAction(cc.sequence(actionMoveCrate,funcCrate));
				    //Crate over crate Drop
				    if(level[playerPosition.y+deltaY][playerPosition.x+deltaX] === 5 ){
				    	//Ha colocado en el lugar correcto el objeto
				    	var placeCrateSuccess = cratesArray[playerPosition.y+deltaY][playerPosition.x+deltaX];
				    	placeCrateSuccess.setTexture(g_ressources[17]);
				    	counterCrates++;
				    	layer.addScore(250*counterCrates);
				    }

				    /*else if(level[movingCrate.getPosition().x+50*deltaX][movingCrate.getPosition().y-50*deltaY] === 11 
				    	|| level[movingCrate.getPosition().x+50*deltaX][movingCrate.getPosition().y-50*deltaY] === 13){
				    	console.log('crate over coin');
				    }
				    /*else{
				    	var placeCrateSuccess = level[playerPosition.y+deltaY][playerPosition.x+deltaX];
				    	placeCrateSuccess.setTexture(g_ressources[16]);
				    }*/

					/*movingCrate.setPosition(movingCrate.getPosition().
					x+50*deltaX,movingCrate.getPosition().y-50*deltaY);*/

					cratesArray[playerPosition.y+deltaY][playerPosition.
					x+deltaX]=movingCrate;
					cratesArray[playerPosition.y][playerPosition.x]=null;

				}

				console.log('level :: ',level);

				let values = level[playerPosition.y][playerPosition.x];
				let x_1_1 = playerPosition.x;
				let y_1_1 = playerPosition.y;
				if(values === 12 ){
			    	//Quitar Sprites de crates
			    	var coin = coinsArray[y_1_1][x_1_1];
			    	coin.stopAllActions();
			    	// Move the sprite
				    var actionMove = cc.MoveTo.create(0.5, cc.p(coin.x, coin.y + 50));
				    var funcT=new cc.CallFunc(function(){
						console.log("Termina Coin Anim");
						//playerSprite.stopAction(actionMove);
						coin.stopAllActions();
				    	layer.removeChild(coin);
				    	coinsArray[y_1_1][x_1_1] = null;
				    	console.log("Mas dos");
						layer.addScore(100);
				    	level[y_1_1][x_1_1] = 0;
				    	console.log("Level Changed",level);
				    	console.log("Coin",coinsArray);
					},coin);
			    	coin.runAction(actionMove);
			    	coin.runAction(cc.sequence(takedCoin,funcT));
			    }

				break;
		}
	}
}

/*function moveSecondPlayer(deltaX,deltaY){
	if(playerSpriteTwo !== null && chronoOver === 0){
		switch(level[playerTwoPosition.y+deltaY][playerTwoPosition.x+deltaX]){
		case 0:
		case 2:
			level[playerTwoPosition.y][playerTwoPosition.x]-=4;
			playerTwoPosition.x+=deltaX;
			playerTwoPosition.y+=deltaY;
			level[playerTwoPosition.y][playerTwoPosition.x]+=4;
			switch(playerDirection){
				//Down 
				case 2:
					playerSpriteTwo.setFlippedX(false);
					playerSpriteTwo.stopAllActions();
					// Move the sprite
				    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerTwoPosition.x, 
				    	600-50*playerTwoPosition.y));
				    var func=new cc.CallFunc(function(){
						console.log("Termina Movimiento abajo");
						//playerSprite.stopAction(actionMove);
						playerSpriteTwo.stopAllActions();
						playerSpriteTwo.runAction(stand);
						//Verificamos si ha ganado
					    if(maxCrates === counterCrates){
					    	console.log("Win");
					    	playerSpriteTwo.stopAllActions();
					    	var funcT=new cc.CallFunc(function(){
								console.log("Termina Win Anim");
								//playerSprite.stopAction(actionMove);
								//playerSpriteTwo.stopAllActions();
								//playerSpriteTwo.runAction(Stand);
							},playerSpriteTwo);
					    	playerSpriteTwo.runAction(cc.sequence(animWinTwo,funcT));
					    }	
					    console.log('level',level);
						console.log('crates',cratesArray);
						console.log(level[playerTwoPosition.y][playerTwoPosition.x]);
					    //Player over crate drop
					    if(level[playerTwoPosition.y][playerTwoPosition.x] === 6){
					    	console.log("Crates Changed");
							var placeCrateSuccess = cratesArray[playerTwoPosition.y][playerTwoPosition.x];
							console.log('Crates',placeCrateSuccess);
					    	placeCrateSuccess.setTexture(g_ressources[17]);
					    }else{
						    //Not over crates drop
							for(i=0;i<11;i++){
								for(j=0;j<15;j++){
									switch(level[i][j]){
										case 2:
											var placeCrate = cratesArray[i][j];
											placeCrate.setTexture(g_ressources[16]);
											//placeCrate.setPosition(165+25*j,186-25*i);
											break;
									}
								}
							}
					    }
					},playerSpriteTwo);
					playerSpriteTwo.runAction(sdown);
					playerSpriteTwo.runAction(cc.sequence(actionMove,func)); 
					break;
				//Left
				case 4:
					// Move the sprite
				    playerSpriteTwo.setFlippedX(true);
				    playerSpriteTwo.stopAllActions();
				    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerTwoPosition.x, 
				    	600-50*playerTwoPosition.y));
				    var func=new cc.CallFunc(function(){
						console.log("Termina Movimiento izquierda");
						//playerSpriteTwo.stopAction(actionMove);
						playerSpriteTwo.stopAllActions();
						playerSpriteTwo.runAction(stand_left);
						//Verificamos si ha ganado
					    if(maxCrates === counterCrates){
					    	console.log("Win");
					    	playerSpriteTwo.stopAllActions();
					    	var funcT=new cc.CallFunc(function(){
								console.log("Termina Win Anim");
								//playerSpriteTwo.stopAction(actionMove);
								//playerSpriteTwo.stopAllActions();
								//playerSpriteTwo.runAction(stand);
							},playerSpriteTwo);
					    	playerSpriteTwo.runAction(cc.sequence(animWinTwo,funcT));
					    }

					    console.log('level',level);
						console.log('crates',cratesArray);
						console.log(level[playerTwoPosition.y][playerTwoPosition.x]);
					    //Player over crate drop
					    if(level[playerTwoPosition.y][playerTwoPosition.x] === 6){
					    	console.log("Crates Changed");
							var placeCrateSuccess = cratesArray[playerTwoPosition.y][playerTwoPosition.x];
							console.log('Crates',placeCrateSuccess);
					    	placeCrateSuccess.setTexture(g_ressources[17]);
					    }else{
						    //Not over crates drop
							for(i=0;i<11;i++){
								for(j=0;j<15;j++){
									switch(level[i][j]){
										case 2:
											var placeCrate = cratesArray[i][j];
											placeCrate.setTexture(g_ressources[16]);
											//placeCrate.setPosition(165+25*j,186-25*i);
											break;
									}
								}
							}
					    }
					},playerSpriteTwo);
					playerSpriteTwo.runAction(sleft);
					playerSpriteTwo.runAction(cc.sequence(actionMove,func)); 
					break;
				//Right
				case 6:
					playerSpriteTwo.setFlippedX(false);
					playerSpriteTwo.stopAllActions();
					// Move the sprite
				    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerTwoPosition.x, 
				    	600-50*playerTwoPosition.y));
				    var func=new cc.CallFunc(function(){
						console.log("Termina Movimiento derecha");
						//playerSpriteTwo.stopAction(actionMove);
						playerSpriteTwo.stopAllActions();
						playerSpriteTwo.runAction(stand_left);
						//Verificamos si ha ganado
					    if(maxCrates === counterCrates){
					    	console.log("Win");
					    	playerSpriteTwo.stopAllActions();
					    	var funcT=new cc.CallFunc(function(){
								console.log("Termina Win Anim");
								//playerSpriteTwo.stopAction(actionMove);
								//playerSpriteTwo.stopAllActions();
								//playerSpriteTwo.runAction(stand);
							},playerSpriteTwo);
					    	playerSpriteTwo.runAction(cc.sequence(animWinTwo,funcT));
					    }

					    console.log('level',level);
						console.log('crates',cratesArray);
						console.log(level[playerTwoPosition.y][playerTwoPosition.x]);
					    //Player over crate drop
					    if(level[playerTwoPosition.y][playerTwoPosition.x] === 6){
					    	console.log("Crates Changed");
							var placeCrateSuccess = cratesArray[playerTwoPosition.y][playerTwoPosition.x];
							console.log('Crates',placeCrateSuccess);
					    	placeCrateSuccess.setTexture(g_ressources[17]);
					    }else{
						    //Not over crates drop
							for(i=0;i<11;i++){
								for(j=0;j<15;j++){
									switch(level[i][j]){
										case 2:
											var placeCrate = cratesArray[i][j];
											placeCrate.setTexture(g_ressources[16]);
											//placeCrate.setPosition(165+25*j,186-25*i);
											break;
									}
								}
							}
					    }
					},playerSpriteTwo);
					playerSpriteTwo.runAction(sleft);
					playerSpriteTwo.runAction(cc.sequence(actionMove,func)); 
					break;
				//Up
				case 8:
					playerSpriteTwo.setFlippedX(false);
					playerSpriteTwo.stopAllActions();
					// Move the sprite
				    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerTwoPosition.x, 
				    	600-50*playerTwoPosition.y));
				    var func=new cc.CallFunc(function(){
						console.log("Termina Movimiento arriba");
						//playerSpriteTwo.stopAction(actionMove);
						playerSpriteTwo.stopAllActions();
						playerSpriteTwo.setTexture(g_ressources[28]);
						//Verificamos si ha ganado
					    if(maxCrates === counterCrates){
					    	console.log("Win");
					    	playerSpriteTwo.stopAllActions();
					    	var funcT=new cc.CallFunc(function(){
								console.log("Termina Win Anim");
								//playerSpriteTwo.stopAction(actionMove);
								//playerSpriteTwo.stopAllActions();
								//playerSpriteTwo.runAction(stand);
							},playerSpriteTwo);
					    	playerSpriteTwo.runAction(cc.sequence(animWinTwo,funcT));
					    }

					    console.log('level',level);
						console.log('crates',cratesArray);
						console.log(level[playerTwoPosition.y][playerTwoPosition.x]);
					    //Player over crate drop
					    if(level[playerTwoPosition.y][playerTwoPosition.x] === 6){
					    	console.log("Crates Changed");
							var placeCrateSuccess = cratesArray[playerTwoPosition.y][playerTwoPosition.x];
							console.log('Crates',placeCrateSuccess);
					    	placeCrateSuccess.setTexture(g_ressources[17]);
					    }else{
						    //Not over crates drop
							for(i=0;i<11;i++){
								for(j=0;j<15;j++){
									switch(level[i][j]){
										case 2:
											var placeCrate = cratesArray[i][j];
											placeCrate.setTexture(g_ressources[16]);
											//placeCrate.setPosition(165+25*j,186-25*i);
											break;
									}
								}
							}
					    }
					},playerSpriteTwo);
					playerSpriteTwo.runAction(sup);
					playerSpriteTwo.runAction(cc.sequence(actionMove,func)); 
					break;
				
			}
			break;
		case 3:
		case 5:
			if(level[playerTwoPosition.y+deltaY*2][playerTwoPosition.x+deltaX*2]==0
			|| level[playerTwoPosition.y+deltaY*2][playerTwoPosition.x+deltaX*2]==2){
				level[playerTwoPosition.y][playerTwoPosition.x]-=4;
				playerTwoPosition.x+=deltaX;
				playerTwoPosition.y+=deltaY;
				level[playerTwoPosition.y][playerTwoPosition.x]+=1;
			    switch(playerDirection){
				//Down 
				case 2:
					playerSpriteTwo.setFlippedX(false);
					playerSpriteTwo.stopAllActions();
					// Move the sprite
				    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerTwoPosition.x, 
				    	600-50*playerTwoPosition.y));
				    var func=new cc.CallFunc(function(){
						console.log("Termina Movimiento abajo");
						//playerSprite.stopAction(actionMove);
						playerSpriteTwo.stopAllActions();
						playerSpriteTwo.runAction(stand);
						//Verificamos si ha ganado
					    if(maxCrates === counterCrates){
					    	console.log("Win");
					    	playerSpriteTwo.stopAllActions();
					    	var funcT=new cc.CallFunc(function(){
								console.log("Termina Win Anim");
								//playerSprite.stopAction(actionMove);
								//playerSpriteTwo.stopAllActions();
								//playerSpriteTwo.runAction(Stand);
							},playerSpriteTwo);
					    	playerSpriteTwo.runAction(cc.sequence(animWinTwo,funcT));
					    }	
					    console.log('level',level);
						console.log('crates',cratesArray);
						console.log(level[playerTwoPosition.y][playerTwoPosition.x]);
					    //Player over crate drop
					    if(level[playerTwoPosition.y][playerTwoPosition.x] === 6){
					    	console.log("Crates Changed");
							var placeCrateSuccess = cratesArray[playerTwoPosition.y][playerTwoPosition.x];
							console.log('Crates',placeCrateSuccess);
					    	placeCrateSuccess.setTexture(g_ressources[17]);
					    }else{
						    //Not over crates drop
							for(i=0;i<11;i++){
								for(j=0;j<15;j++){
									switch(level[i][j]){
										case 2:
											var placeCrate = cratesArray[i][j];
											placeCrate.setTexture(g_ressources[16]);
											//placeCrate.setPosition(165+25*j,186-25*i);
											break;
									}
								}
							}
					    }
					},playerSpriteTwo);
					playerSpriteTwo.runAction(sdown);
					playerSpriteTwo.runAction(cc.sequence(actionMove,func)); 
					break;
				//Left
				case 4:
					// Move the sprite
				    playerSpriteTwo.setFlippedX(true);
				    playerSpriteTwo.stopAllActions();
				    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerTwoPosition.x, 
				    	600-50*playerTwoPosition.y));
				    var func=new cc.CallFunc(function(){
						console.log("Termina Movimiento izquierda");
						//playerSpriteTwo.stopAction(actionMove);
						playerSpriteTwo.stopAllActions();
						playerSpriteTwo.runAction(stand_left);
						//Verificamos si ha ganado
					    if(maxCrates === counterCrates){
					    	console.log("Win");
					    	playerSpriteTwo.stopAllActions();
					    	var funcT=new cc.CallFunc(function(){
								console.log("Termina Win Anim");
								//playerSpriteTwo.stopAction(actionMove);
								//playerSpriteTwo.stopAllActions();
								//playerSpriteTwo.runAction(stand);
							},playerSpriteTwo);
					    	playerSpriteTwo.runAction(cc.sequence(animWinTwo,funcT));
					    }

					    console.log('level',level);
						console.log('crates',cratesArray);
						console.log(level[playerTwoPosition.y][playerTwoPosition.x]);
					    //Player over crate drop
					    if(level[playerTwoPosition.y][playerTwoPosition.x] === 6){
					    	console.log("Crates Changed");
							var placeCrateSuccess = cratesArray[playerTwoPosition.y][playerTwoPosition.x];
							console.log('Crates',placeCrateSuccess);
					    	placeCrateSuccess.setTexture(g_ressources[17]);
					    }else{
						    //Not over crates drop
							for(i=0;i<11;i++){
								for(j=0;j<15;j++){
									switch(level[i][j]){
										case 2:
											var placeCrate = cratesArray[i][j];
											placeCrate.setTexture(g_ressources[16]);
											//placeCrate.setPosition(165+25*j,186-25*i);
											break;
									}
								}
							}
					    }
					},playerSpriteTwo);
					playerSpriteTwo.runAction(sleft);
					playerSpriteTwo.runAction(cc.sequence(actionMove,func)); 
					break;
				//Right
				case 6:
					playerSpriteTwo.setFlippedX(false);
					playerSpriteTwo.stopAllActions();
					// Move the sprite
				    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerTwoPosition.x, 
				    	600-50*playerTwoPosition.y));
				    var func=new cc.CallFunc(function(){
						console.log("Termina Movimiento derecha");
						//playerSpriteTwo.stopAction(actionMove);
						playerSpriteTwo.stopAllActions();
						playerSpriteTwo.runAction(stand_left);
						//Verificamos si ha ganado
					    if(maxCrates === counterCrates){
					    	console.log("Win");
					    	playerSpriteTwo.stopAllActions();
					    	var funcT=new cc.CallFunc(function(){
								console.log("Termina Win Anim");
								//playerSpriteTwo.stopAction(actionMove);
								//playerSpriteTwo.stopAllActions();
								//playerSpriteTwo.runAction(stand);
							},playerSpriteTwo);
					    	playerSpriteTwo.runAction(cc.sequence(animWinTwo,funcT));
					    }

					    console.log('level',level);
						console.log('crates',cratesArray);
						console.log(level[playerTwoPosition.y][playerTwoPosition.x]);
					    //Player over crate drop
					    if(level[playerTwoPosition.y][playerTwoPosition.x] === 6){
					    	console.log("Crates Changed");
							var placeCrateSuccess = cratesArray[playerTwoPosition.y][playerTwoPosition.x];
							console.log('Crates',placeCrateSuccess);
					    	placeCrateSuccess.setTexture(g_ressources[17]);
					    }else{
						    //Not over crates drop
							for(i=0;i<11;i++){
								for(j=0;j<15;j++){
									switch(level[i][j]){
										case 2:
											var placeCrate = cratesArray[i][j];
											placeCrate.setTexture(g_ressources[16]);
											//placeCrate.setPosition(165+25*j,186-25*i);
											break;
									}
								}
							}
					    }
					},playerSpriteTwo);
					playerSpriteTwo.runAction(sleft);
					playerSpriteTwo.runAction(cc.sequence(actionMove,func)); 
					break;
				//Up
				case 8:
					playerSpriteTwo.setFlippedX(false);
					playerSpriteTwo.stopAllActions();
					// Move the sprite
				    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerTwoPosition.x, 
				    	600-50*playerTwoPosition.y));
				    var func=new cc.CallFunc(function(){
						console.log("Termina Movimiento arriba");
						//playerSpriteTwo.stopAction(actionMove);
						playerSpriteTwo.stopAllActions();
						playerSpriteTwo.setTexture(g_ressources[28]);
						//Verificamos si ha ganado
					    if(maxCrates === counterCrates){
					    	console.log("Win");
					    	playerSpriteTwo.stopAllActions();
					    	var funcT=new cc.CallFunc(function(){
								console.log("Termina Win Anim");
								//playerSpriteTwo.stopAction(actionMove);
								//playerSpriteTwo.stopAllActions();
								//playerSpriteTwo.runAction(stand);
							},playerSpriteTwo);
					    	playerSpriteTwo.runAction(cc.sequence(animWinTwo,funcT));
					    }

					    console.log('level',level);
						console.log('crates',cratesArray);
						console.log(level[playerTwoPosition.y][playerTwoPosition.x]);
					    //Player over crate drop
					    if(level[playerTwoPosition.y][playerTwoPosition.x] === 6){
					    	console.log("Crates Changed");
							var placeCrateSuccess = cratesArray[playerTwoPosition.y][playerTwoPosition.x];
							console.log('Crates',placeCrateSuccess);
					    	placeCrateSuccess.setTexture(g_ressources[17]);
					    }else{
						    //Not over crates drop
							for(i=0;i<11;i++){
								for(j=0;j<15;j++){
									switch(level[i][j]){
										case 2:
											var placeCrate = cratesArray[i][j];
											placeCrate.setTexture(g_ressources[16]);
											//placeCrate.setPosition(165+25*j,186-25*i);
											break;
									}
								}
							}
					    }
					},playerSpriteTwo);
					playerSpriteTwo.runAction(sup);
					playerSpriteTwo.runAction(cc.sequence(actionMove,func)); 
					break;
				
			}

				level[playerTwoPosition.y+deltaY][playerTwoPosition.x+deltaX]+=3;
				var movingCrate = cratesArray[playerTwoPosition.y]
				[playerTwoPosition.x];

				
			    var actionMoveCrate = cc.MoveTo.create(0.5, cc.p(movingCrate.getPosition().x+50*deltaX
			    	,movingCrate.getPosition().y-50*deltaY));
			    var funcCrate=new cc.CallFunc(function(){
					console.log("Termina Movimiento");
					playerSpriteTwo.stopAction(actionMoveCrate);
				},movingCrate);
			    movingCrate.runAction(cc.sequence(actionMoveCrate,funcCrate));
			    //Crate over crate Drop
			    if(level[playerTwoPosition.y+deltaY][playerTwoPosition.x+deltaX] === 5 ){
			    	//Ha colocado en el lugar correcto el objeto
			    	var placeCrateSuccess = cratesArray[playerTwoPosition.y+deltaY][playerTwoPosition.x+deltaX];
			    	placeCrateSuccess.setTexture(g_ressources[17]);
			    	counterCrates++;
			    	layer.addScore(250*counterCrates);
			    }

				cratesArray[playerTwoPosition.y+deltaY][playerTwoPosition.
				x+deltaX]=movingCrate;
				cratesArray[playerTwoPosition.y][playerTwoPosition.x]=null;
			}
		break;
		case 8:
			level[playerTwoPosition.y][playerTwoPosition.x]-=4;
			playerTwoPosition.x+=deltaX;
			playerTwoPosition.y+=deltaY;
			level[playerTwoPosition.y][playerTwoPosition.x]+=4;
			console.log('Level Player',level[playerTwoPosition.y][playerTwoPosition.x]);
			switch(playerDirection){
				//Down 
				case 2:
					playerSpriteTwo.setFlippedX(false);
					playerSpriteTwo.stopAllActions();
					// Move the sprite
				    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerTwoPosition.x, 
				    	600-50*playerTwoPosition.y));
				    var func=new cc.CallFunc(function(){
						console.log("Termina Movimiento abajo");
						//playerSprite.stopAction(actionMove);
						playerSpriteTwo.stopAllActions();
						playerSpriteTwo.runAction(stand);
						//Verificamos si ha ganado
					    if(maxCrates === counterCrates){
					    	console.log("Win");
					    	playerSpriteTwo.stopAllActions();
					    	var funcT=new cc.CallFunc(function(){
								console.log("Termina Win Anim");
								//playerSprite.stopAction(actionMove);
								//playerSpriteTwo.stopAllActions();
								//playerSpriteTwo.runAction(Stand);
							},playerSpriteTwo);
					    	playerSpriteTwo.runAction(cc.sequence(animWinTwo,funcT));
					    }	
					    console.log('level',level);
						console.log('crates',cratesArray);
						console.log(level[playerTwoPosition.y][playerTwoPosition.x]);
					    //Player over crate drop
					    if(level[playerTwoPosition.y][playerTwoPosition.x] === 6){
					    	console.log("Crates Changed");
							var placeCrateSuccess = cratesArray[playerTwoPosition.y][playerTwoPosition.x];
							console.log('Crates',placeCrateSuccess);
					    	placeCrateSuccess.setTexture(g_ressources[17]);
					    }else{
						    //Not over crates drop
							for(i=0;i<11;i++){
								for(j=0;j<15;j++){
									switch(level[i][j]){
										case 2:
											var placeCrate = cratesArray[i][j];
											placeCrate.setTexture(g_ressources[16]);
											//placeCrate.setPosition(165+25*j,186-25*i);
											break;
									}
								}
							}
					    }
					},playerSpriteTwo);
					playerSpriteTwo.runAction(sdown);
					playerSpriteTwo.runAction(cc.sequence(actionMove,func)); 
					break;
				//Left
				case 4:
					// Move the sprite
				    playerSpriteTwo.setFlippedX(true);
				    playerSpriteTwo.stopAllActions();
				    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerTwoPosition.x, 
				    	600-50*playerTwoPosition.y));
				    var func=new cc.CallFunc(function(){
						console.log("Termina Movimiento izquierda");
						//playerSpriteTwo.stopAction(actionMove);
						playerSpriteTwo.stopAllActions();
						playerSpriteTwo.runAction(stand_left);
						//Verificamos si ha ganado
					    if(maxCrates === counterCrates){
					    	console.log("Win");
					    	playerSpriteTwo.stopAllActions();
					    	var funcT=new cc.CallFunc(function(){
								console.log("Termina Win Anim");
								//playerSpriteTwo.stopAction(actionMove);
								//playerSpriteTwo.stopAllActions();
								//playerSpriteTwo.runAction(stand);
							},playerSpriteTwo);
					    	playerSpriteTwo.runAction(cc.sequence(animWinTwo,funcT));
					    }

					    console.log('level',level);
						console.log('crates',cratesArray);
						console.log(level[playerTwoPosition.y][playerTwoPosition.x]);
					    //Player over crate drop
					    if(level[playerTwoPosition.y][playerTwoPosition.x] === 6){
					    	console.log("Crates Changed");
							var placeCrateSuccess = cratesArray[playerTwoPosition.y][playerTwoPosition.x];
							console.log('Crates',placeCrateSuccess);
					    	placeCrateSuccess.setTexture(g_ressources[17]);
					    }else{
						    //Not over crates drop
							for(i=0;i<11;i++){
								for(j=0;j<15;j++){
									switch(level[i][j]){
										case 2:
											var placeCrate = cratesArray[i][j];
											placeCrate.setTexture(g_ressources[16]);
											//placeCrate.setPosition(165+25*j,186-25*i);
											break;
									}
								}
							}
					    }
					},playerSpriteTwo);
					playerSpriteTwo.runAction(sleft);
					playerSpriteTwo.runAction(cc.sequence(actionMove,func)); 
					break;
				//Right
				case 6:
					playerSpriteTwo.setFlippedX(false);
					playerSpriteTwo.stopAllActions();
					// Move the sprite
				    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerTwoPosition.x, 
				    	600-50*playerTwoPosition.y));
				    var func=new cc.CallFunc(function(){
						console.log("Termina Movimiento derecha");
						//playerSpriteTwo.stopAction(actionMove);
						playerSpriteTwo.stopAllActions();
						playerSpriteTwo.runAction(stand_left);
						//Verificamos si ha ganado
					    if(maxCrates === counterCrates){
					    	console.log("Win");
					    	playerSpriteTwo.stopAllActions();
					    	var funcT=new cc.CallFunc(function(){
								console.log("Termina Win Anim");
								//playerSpriteTwo.stopAction(actionMove);
								//playerSpriteTwo.stopAllActions();
								//playerSpriteTwo.runAction(stand);
							},playerSpriteTwo);
					    	playerSpriteTwo.runAction(cc.sequence(animWinTwo,funcT));
					    }

					    console.log('level',level);
						console.log('crates',cratesArray);
						console.log(level[playerTwoPosition.y][playerTwoPosition.x]);
					    //Player over crate drop
					    if(level[playerTwoPosition.y][playerTwoPosition.x] === 6){
					    	console.log("Crates Changed");
							var placeCrateSuccess = cratesArray[playerTwoPosition.y][playerTwoPosition.x];
							console.log('Crates',placeCrateSuccess);
					    	placeCrateSuccess.setTexture(g_ressources[17]);
					    }else{
						    //Not over crates drop
							for(i=0;i<11;i++){
								for(j=0;j<15;j++){
									switch(level[i][j]){
										case 2:
											var placeCrate = cratesArray[i][j];
											placeCrate.setTexture(g_ressources[16]);
											//placeCrate.setPosition(165+25*j,186-25*i);
											break;
									}
								}
							}
					    }
					},playerSpriteTwo);
					playerSpriteTwo.runAction(sleft);
					playerSpriteTwo.runAction(cc.sequence(actionMove,func)); 
					break;
				//Up
				case 8:
					playerSpriteTwo.setFlippedX(false);
					playerSpriteTwo.stopAllActions();
					// Move the sprite
				    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerTwoPosition.x, 
				    	600-50*playerTwoPosition.y));
				    var func=new cc.CallFunc(function(){
						console.log("Termina Movimiento arriba");
						//playerSpriteTwo.stopAction(actionMove);
						playerSpriteTwo.stopAllActions();
						playerSpriteTwo.setTexture(g_ressources[28]);
						//Verificamos si ha ganado
					    if(maxCrates === counterCrates){
					    	console.log("Win");
					    	playerSpriteTwo.stopAllActions();
					    	var funcT=new cc.CallFunc(function(){
								console.log("Termina Win Anim");
								//playerSpriteTwo.stopAction(actionMove);
								//playerSpriteTwo.stopAllActions();
								//playerSpriteTwo.runAction(stand);
							},playerSpriteTwo);
					    	playerSpriteTwo.runAction(cc.sequence(animWinTwo,funcT));
					    }

					    console.log('level',level);
						console.log('crates',cratesArray);
						console.log(level[playerTwoPosition.y][playerTwoPosition.x]);
					    //Player over crate drop
					    if(level[playerTwoPosition.y][playerTwoPosition.x] === 6){
					    	console.log("Crates Changed");
							var placeCrateSuccess = cratesArray[playerTwoPosition.y][playerTwoPosition.x];
							console.log('Crates',placeCrateSuccess);
					    	placeCrateSuccess.setTexture(g_ressources[17]);
					    }else{
						    //Not over crates drop
							for(i=0;i<11;i++){
								for(j=0;j<15;j++){
									switch(level[i][j]){
										case 2:
											var placeCrate = cratesArray[i][j];
											placeCrate.setTexture(g_ressources[16]);
											//placeCrate.setPosition(165+25*j,186-25*i);
											break;
									}
								}
							}
					    }
					},playerSpriteTwo);
					playerSpriteTwo.runAction(sup);
					playerSpriteTwo.runAction(cc.sequence(actionMove,func)); 
					break;
				
			}

			if(level[playerTwoPosition.y][playerTwoPosition.x] === 12 ){
		    	//Quitar Sprites de crates
		    	var coin = cratesArray[playerTwoPosition.y][playerTwoPosition.x];
		    	coin.stopAllActions();
		    	// Move the sprite
			    var actionMove = cc.MoveTo.create(0.5, cc.p(coin.x, coin.y + 50));
			    var funcT=new cc.CallFunc(function(){
					console.log("Termina Coin Anim");
					//playerSprite.stopAction(actionMove);
					coin.stopAllActions();
			    	layer.removeChild(coin);
			    	cratesArray[playerTwoPosition.y][playerTwoPosition.x] = null;
			    	console.log("Mas dos");
					layer.addScore(100);
			    	level[playerTwoPosition.y][playerTwoPosition.x] = 4;
			    	console.log("Level Changed",level);
			    	console.log("Crates Coin",cratesArray);
				},coin);
		    	coin.runAction(actionMove);
		    	coin.runAction(cc.sequence(takedCoin,funcT));
		    }
		break;
		case 10:
			level[playerTwoPosition.y][playerTwoPosition.x]-=4;
			playerTwoPosition.x+=deltaX;
			playerTwoPosition.y+=deltaY;
			level[playerTwoPosition.y][playerTwoPosition.x]+=4;
			switch(playerDirection){
				//Down 
				case 2:
					playerSpriteTwo.setFlippedX(false);
					playerSpriteTwo.stopAllActions();
					// Move the sprite
				    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerTwoPosition.x, 
				    	600-50*playerTwoPosition.y));
				    var func=new cc.CallFunc(function(){
						console.log("Termina Movimiento abajo");
						//playerSprite.stopAction(actionMove);
						playerSpriteTwo.stopAllActions();
						playerSpriteTwo.runAction(stand);
						//Verificamos si ha ganado
					    if(maxCrates === counterCrates){
					    	console.log("Win");
					    	playerSpriteTwo.stopAllActions();
					    	var funcT=new cc.CallFunc(function(){
								console.log("Termina Win Anim");
								//playerSprite.stopAction(actionMove);
								//playerSpriteTwo.stopAllActions();
								//playerSpriteTwo.runAction(Stand);
							},playerSpriteTwo);
					    	playerSpriteTwo.runAction(cc.sequence(animWinTwo,funcT));
					    }	
					    console.log('level',level);
						console.log('crates',cratesArray);
						console.log(level[playerTwoPosition.y][playerTwoPosition.x]);
					    //Player over crate drop
					    if(level[playerTwoPosition.y][playerTwoPosition.x] === 6){
					    	console.log("Crates Changed");
							var placeCrateSuccess = cratesArray[playerTwoPosition.y][playerTwoPosition.x];
							console.log('Crates',placeCrateSuccess);
					    	placeCrateSuccess.setTexture(g_ressources[17]);
					    }else{
						    //Not over crates drop
							for(i=0;i<11;i++){
								for(j=0;j<15;j++){
									switch(level[i][j]){
										case 2:
											var placeCrate = cratesArray[i][j];
											placeCrate.setTexture(g_ressources[16]);
											//placeCrate.setPosition(165+25*j,186-25*i);
											break;
									}
								}
							}
					    }
					},playerSpriteTwo);
					playerSpriteTwo.runAction(sdown);
					playerSpriteTwo.runAction(cc.sequence(actionMove,func)); 
					break;
				//Left
				case 4:
					// Move the sprite
				    playerSpriteTwo.setFlippedX(true);
				    playerSpriteTwo.stopAllActions();
				    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerTwoPosition.x, 
				    	600-50*playerTwoPosition.y));
				    var func=new cc.CallFunc(function(){
						console.log("Termina Movimiento izquierda");
						//playerSpriteTwo.stopAction(actionMove);
						playerSpriteTwo.stopAllActions();
						playerSpriteTwo.runAction(stand_left);
						//Verificamos si ha ganado
					    if(maxCrates === counterCrates){
					    	console.log("Win");
					    	playerSpriteTwo.stopAllActions();
					    	var funcT=new cc.CallFunc(function(){
								console.log("Termina Win Anim");
								//playerSpriteTwo.stopAction(actionMove);
								//playerSpriteTwo.stopAllActions();
								//playerSpriteTwo.runAction(stand);
							},playerSpriteTwo);
					    	playerSpriteTwo.runAction(cc.sequence(animWinTwo,funcT));
					    }

					    console.log('level',level);
						console.log('crates',cratesArray);
						console.log(level[playerTwoPosition.y][playerTwoPosition.x]);
					    //Player over crate drop
					    if(level[playerTwoPosition.y][playerTwoPosition.x] === 6){
					    	console.log("Crates Changed");
							var placeCrateSuccess = cratesArray[playerTwoPosition.y][playerTwoPosition.x];
							console.log('Crates',placeCrateSuccess);
					    	placeCrateSuccess.setTexture(g_ressources[17]);
					    }else{
						    //Not over crates drop
							for(i=0;i<11;i++){
								for(j=0;j<15;j++){
									switch(level[i][j]){
										case 2:
											var placeCrate = cratesArray[i][j];
											placeCrate.setTexture(g_ressources[16]);
											//placeCrate.setPosition(165+25*j,186-25*i);
											break;
									}
								}
							}
					    }
					},playerSpriteTwo);
					playerSpriteTwo.runAction(sleft);
					playerSpriteTwo.runAction(cc.sequence(actionMove,func)); 
					break;
				//Right
				case 6:
					playerSpriteTwo.setFlippedX(false);
					playerSpriteTwo.stopAllActions();
					// Move the sprite
				    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerTwoPosition.x, 
				    	600-50*playerTwoPosition.y));
				    var func=new cc.CallFunc(function(){
						console.log("Termina Movimiento derecha");
						//playerSpriteTwo.stopAction(actionMove);
						playerSpriteTwo.stopAllActions();
						playerSpriteTwo.runAction(stand_left);
						//Verificamos si ha ganado
					    if(maxCrates === counterCrates){
					    	console.log("Win");
					    	playerSpriteTwo.stopAllActions();
					    	var funcT=new cc.CallFunc(function(){
								console.log("Termina Win Anim");
								//playerSpriteTwo.stopAction(actionMove);
								//playerSpriteTwo.stopAllActions();
								//playerSpriteTwo.runAction(stand);
							},playerSpriteTwo);
					    	playerSpriteTwo.runAction(cc.sequence(animWinTwo,funcT));
					    }

					    console.log('level',level);
						console.log('crates',cratesArray);
						console.log(level[playerTwoPosition.y][playerTwoPosition.x]);
					    //Player over crate drop
					    if(level[playerTwoPosition.y][playerTwoPosition.x] === 6){
					    	console.log("Crates Changed");
							var placeCrateSuccess = cratesArray[playerTwoPosition.y][playerTwoPosition.x];
							console.log('Crates',placeCrateSuccess);
					    	placeCrateSuccess.setTexture(g_ressources[17]);
					    }else{
						    //Not over crates drop
							for(i=0;i<11;i++){
								for(j=0;j<15;j++){
									switch(level[i][j]){
										case 2:
											var placeCrate = cratesArray[i][j];
											placeCrate.setTexture(g_ressources[16]);
											//placeCrate.setPosition(165+25*j,186-25*i);
											break;
									}
								}
							}
					    }
					},playerSpriteTwo);
					playerSpriteTwo.runAction(sleft);
					playerSpriteTwo.runAction(cc.sequence(actionMove,func)); 
					break;
				//Up
				case 8:
					playerSpriteTwo.setFlippedX(false);
					playerSpriteTwo.stopAllActions();
					// Move the sprite
				    var actionMove = cc.MoveTo.create(0.5, cc.p(50+50*playerTwoPosition.x, 
				    	600-50*playerTwoPosition.y));
				    var func=new cc.CallFunc(function(){
						console.log("Termina Movimiento arriba");
						//playerSpriteTwo.stopAction(actionMove);
						playerSpriteTwo.stopAllActions();
						playerSpriteTwo.setTexture(g_ressources[28]);
						//Verificamos si ha ganado
					    if(maxCrates === counterCrates){
					    	console.log("Win");
					    	playerSpriteTwo.stopAllActions();
					    	var funcT=new cc.CallFunc(function(){
								console.log("Termina Win Anim");
								//playerSpriteTwo.stopAction(actionMove);
								//playerSpriteTwo.stopAllActions();
								//playerSpriteTwo.runAction(stand);
							},playerSpriteTwo);
					    	playerSpriteTwo.runAction(cc.sequence(animWinTwo,funcT));
					    }

					    console.log('level',level);
						console.log('crates',cratesArray);
						console.log(level[playerTwoPosition.y][playerTwoPosition.x]);
					    //Player over crate drop
					    if(level[playerTwoPosition.y][playerTwoPosition.x] === 6){
					    	console.log("Crates Changed");
							var placeCrateSuccess = cratesArray[playerTwoPosition.y][playerTwoPosition.x];
							console.log('Crates',placeCrateSuccess);
					    	placeCrateSuccess.setTexture(g_ressources[17]);
					    }else{
						    //Not over crates drop
							for(i=0;i<11;i++){
								for(j=0;j<15;j++){
									switch(level[i][j]){
										case 2:
											var placeCrate = cratesArray[i][j];
											placeCrate.setTexture(g_ressources[16]);
											//placeCrate.setPosition(165+25*j,186-25*i);
											break;
									}
								}
							}
					    }
					},playerSpriteTwo);
					playerSpriteTwo.runAction(sup);
					playerSpriteTwo.runAction(cc.sequence(actionMove,func)); 
					break;
				
			}
			break;
	}
	}
}*/

var PlayScene = cc.Scene.extend({
 	onEnter:function () {
 		this._super();
 		reinit();
 		layer = new PlayLayer();
 		this.addChild(layer);
 	}
 });

function reinit(){
	level = [
		[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
		[1,1,0,0,0,0,1,1,1,1,1,1,1,1,1],
		[1,1,3,0,2,0,8,0,1,1,1,1,1,1,1],
		[1,8,0,4,8,0,0,8,1,1,1,1,1,1,1],
		[1,0,3,1,2,0,8,0,1,1,1,1,1,1,1],
		[1,8,0,1,1,1,1,8,1,1,1,1,1,1,1],
		[1,1,0,1,1,1,1,0,1,1,1,1,1,1,1],
		[1,0,8,0,8,0,0,8,1,1,1,1,1,1,1],
		[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
		[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
		[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
		[0,0,0,1,1,1,1,1,1,0,0,0,0,0,0]
		];
		cratesArray = [];
        playerPosition;
        playerSprite;
		// 2 down
		// 4 left 
		// 5 stand
		// 6 right
		// 8 up
		// 45 stand - left
		// 56 stand - right
		playerDirection = 5;

	    playerTwoPosition;
		 playerSpriteTwo;

		 startTouch;
		 endTouch;
		 swipeTolerance = 10;
		 left = null;
		 right = null;
		 up = null;
		 down = null;
		//Chrono Trigger
		 sleft = null;
		 sright = null;
		 sup = null;
		 sdown = null;
		 stand = null;
		 stand_up = null;
		 stand_left = null;
		 chrono_win = null;
		//Chrono Trigger
		 animateCrate = null;
		 animateCrate2 = null;
		 counterCrates = 0;
		 maxCrates = 0;
		 animWin = null;
		 animWinTwo = null;
		 counterPlayers = 0;
		 animateCoins = null;
		 takedCoin = null;

		//Rock Down Mario RPG
		 animateRockDown = null;

		//Mario over
		 animMarioOver = null;
		 animMOverUp = null;
		 marioOver = 0;

		//Chrono Over
		 animChronoOver = null;
		 chronoOver = 0;
}